var express = require('express'),
    cookieParser = require('cookie-parser'),
    path = require('path'),
    bodyParser = require('body-parser'),
    errorhandler = require('errorhandler'),
    http = require('http'),
    compression = require('compression'),
    multer = require('multer'),
    path = require('path'),
    request = require('request'),
    fs = require('fs'),
    app = express(),
    passport = require('passport'),
    GoogleStrategy = require('passport-google-oauth2').Strategy;;

var GOOGLE_CLIENT_ID      = "883374306567-9qtg29lrbdg6io0gch4s8s8bcj3uf7dh.apps.googleusercontent.com",
    GOOGLE_CLIENT_SECRET  = "cgPCFSbEEa5JsfLZnwyF1DSL";

app.set('view', __dirname + '/views');
compression = require('compression');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ limit: '500mb' }));
app.use(errorhandler());

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        if (req.url.indexOf('profileUpload') > -1) {
            cb(null, __dirname + '/uploads/profiles/');
        } else if (req.url.indexOf('user/image') > -1){
            cb(null, __dirname + '/uploads/userLogos/');
        } else if (req.url.indexOf('uploadResearchDocuments') > -1){
            cb(null, __dirname + '/uploads/researchDocuments/');
        } else if (req.url.indexOf('uploadProposal') > -1) {
            cb(null, __dirname + '/uploads/proposals/');
        } else if (req.url.indexOf('uploadResource') > -1 || req.url.indexOf('requestsResponse') > -1) {
            cb(null, __dirname + '/uploads/resources/');
        } else if (req.url.indexOf('/ui/project/uploads/') > -1) {
            let type = req.url.replace("/ui/project/uploads/", "");
            cb(null, __dirname + '/uploads/'+type+'/');
        } else if (req.url.indexOf('research') > -1) {
            cb(null, __dirname + '/uploads/research/'); 
        } else if (req.url.indexOf('uploadArticle') > -1) {
            cb(null, __dirname + '/uploads/article/'); 
        } else {
            cb(null, __dirname + '/uploads/');
        }
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '_' + file.originalname);
    }
}),
    upload = multer({ storage: storage });

app.use(upload.single('upload'));

app.use('/', express.static(path.join(__dirname, '/')));

app.get('/admin',function(req,res){
    res.sendFile(__dirname+'/index.html');
});
app.get('/changepassword::id', function(req, res) {
    res.sendFile(__dirname+'/index.html');
});
app.get('/activateduser', function(req, res) {
    res.sendFile(__dirname+'/index.html');
});
app.get('/errorpage', function(req, res) {
    res.sendFile(__dirname+'/index.html');
});
app.get('/linkexpired', function(req, res) {
    res.sendFile(__dirname+'/index.html');
});

// GET /auth/google
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  The first step in Google authentication will involve
//   redirecting the user to google.com.  After authorization, Google
//   will redirect the user back to this application at /auth/google/callback
app.get('/auth/google', passport.authenticate('google', { scope: [
       'email', 'profile'] 
}));

// GET /auth/google/callback
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  If authentication fails, the user will be redirected back to the
//   login page.  Otherwise, the primary route function function will be called,
//   which, in this example, will redirect the user to the home page.
app.get( '/auth/google/callback', 
    passport.authenticate( 'google', { 
        successRedirect: '/',
        failureRedirect: '/login'
    }
));

var remoteServer = {
    host: 'localhost',
    port:'3002'
}

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(obj, done) {
  done(null, obj);
});

// Use the GoogleStrategy within Passport.
//   Strategies in Passport require a `verify` function, which accept
//   credentials (in this case, an accessToken, refreshToken, and Google
//   profile), and invoke a callback with a user object.
passport.use(new GoogleStrategy({
    clientID:     GOOGLE_CLIENT_ID,
    clientSecret: GOOGLE_CLIENT_SECRET,
    //NOTE :
    //Carefull ! and avoid usage of Private IP, otherwise you will get the device_id device_name issue for Private IP during authentication
    //The workaround is to set up thru the google cloud console a fully qualified domain name such as http://mydomain:3000/ 
    //then edit your /etc/hosts local file to point on your private IP. 
    //Also both sign-in button + callbackURL has to be share the same url, otherwise two cookies will be created and lead to lost your session
    //if you use it.
    callbackURL: "http://svm.edvenswa.com:3003/auth/google/callback",
    passReqToCallback   : true
  },
  function(request, accessToken, refreshToken, profile, done) {
    // asynchronous verification, for effect...
    process.nextTick(function () {
      
      // To keep the example simple, the user's Google profile is returned to
      // represent the logged-in user.  In a typical application, you would want
      // to associate the Google account with a user record in your database,
      // and return that user instead.
      return done(null, profile);
    });
  }
));


app.use(function (req, res, next) {

    if (req.headers.token) {
        token = req.headers.token;
    } else {
        token = '';
    }

    if (req.url.indexOf('ui') > -1 || req.url.indexOf('app') > -1 || req.url.indexOf('api') > -1) {
        if (req.file) {
            var body = {
                "advanceDetails": JSON.parse(req.body.advanceDetails),
                "file": req.file
            };
            var data = JSON.stringify(body);
            var pOptions = {
                host: remoteServer.host,
                port: remoteServer.port,
                path: req.url,
                method: req.method,
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': Buffer.byteLength(data),
                    'token': token
                }
            };

            if (pOptions.path.indexOf('auth') == -1) {
                pOptions.headers.token = req.headers.token;
            }

            if (req.headers && req.headers.authorization) {
                pOptions.headers.Authorization = req.headers.authorization;
            }
            sendRequest(pOptions, data, function (response) {
                res.json(response);
            });
        } else if (req.method == 'POST') {
            var data = JSON.stringify(req.body);

            var pOptions = {
                host: remoteServer.host,
                port: remoteServer.port,
                path: req.url,
                method: req.method,
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': Buffer.byteLength(data),
                    'token': token
                }
            };

            if (pOptions.path.indexOf('auth') == -1) {
                pOptions.headers.token = token;
            }

            if (req.headers && req.headers.authorization) {
                pOptions.headers.Authorization = req.headers.authorization;
            }

            sendRequest(pOptions, data, function (response) {
                res.json(response);
            });
        } else if (req.method == 'PUT') {

            var data = JSON.stringify(req.body);

            var pOptions = {
                host: remoteServer.host,
                port: remoteServer.port,
                path: req.url,
                method: req.method,
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': Buffer.byteLength(data),
                    'token': token
                }
            };

            if (req.headers && req.headers.authorization) {
                pOptions.headers.Authorization = req.headers.authorization;
            }

            sendRequest(pOptions, data, function (response) {
                res.json(response);
            });
        } else if (req.method == 'DELETE') {
            var data = JSON.stringify(req.body);
            var dOptions = {
                host: remoteServer.host,
                port: remoteServer.port,
                path: req.url,
                method: req.method,
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': Buffer.byteLength(data),
                    'token': token
                }
            };
            console.log('header', dOptions.headers);
            if (req.headers && req.headers.authorization) {
                dOptions.headers.Authorization = req.headers.authorization;
            }

            sendRequest(dOptions, data, function (response) {
                res.json(response);
            });
        } else if (req.method == 'GET') {
            var gOptions = {
                host: remoteServer.host,
                port: remoteServer.port,
                path: req.url,
                method: req.method,
                headers: {
                    'Content-Type': 'application/json',
                    'token': token
                }
            };

            if (req.headers && req.headers.authorization) {
                gOptions.headers.Authorization = req.headers.authorization;
            }

            sendGetRequest(gOptions, http, function (response) {
                res.json(response);
            });
        }
    } else {
        next();
    }

});

function sendRequest(options, data, callback) {
    var req = http.request(options, function (res) {
        res.setEncoding('utf8');
        var response = '';
        res.on('data', function (chunk) {
            response += chunk;
        });
        res.on('error', function (error) {
            callback({ status: 'Error', error: error });
        });
        res.on('end', function () {
            try {
                callback(JSON.parse(response));
            } catch (e) {
                callback({ status: 'Error', error: response });
            }
        });
    });
    req.end(data);
}

function sendGetRequest(options, httpType, callback) {
    var req = httpType.get(options, function (res) {
        res.setEncoding('utf8');
        var response = '';
        res.on('data', function (chunk) {
            response += chunk;
        });
        res.on('end', function () {
            if (response.length) {
                try {
                    callback(JSON.parse(response));
                } catch (e) {
                    var resObj = { status: 'Failure', errors: [response] }
                    callback(resObj);
                }
            } else {
                callback({});
            }
        });
    });
    req.on('error', function (e) {
        console.log('problem with request: ' + e.message);
    });
    req.end();
}

process.on('uncaughtException', function (err) {
    console.log(err);
})

//Start Server
app.listen(3003, function () {
    console.log('SVM UI Server started @Port : 3003');
});
