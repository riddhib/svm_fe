angular.module('SVM')
    .filter('arrayToString', function () {
        return function (array) {
            if (array.length > 3) {
                return 'Multiple';
            } else {
                return array.join(", ");
            }
        }
    })
    .filter('convertToDate', function () {
        return function (milliseconds) {
            var date = new Date(milliseconds);
            var year = date.getFullYear();
            var month = ('0' + (date.getMonth() + 1)).slice(-2);
            var day = ('0' + date.getDate()).slice(-2);
            return month + '/' + day + '/' + year;
        }
    })
    .filter('solutionType', function() {
        return function(value) {
            if (value == 'halfBaked') {
                return 'Half Baked';
            }else if (value == 'fullyBaked') {
                return 'Fully Baked';
            }
            else if(value == 'commercialized') {
                return 'Commercialized';
            }
        }
    })
    .filter('projectStatus', function() {
        return function(value) {
            if (value == 'inProgress') {
                return 'In-progress';
            }else if (value == 'new') {
                return 'New';
            }
            else if(value == 'completed') {
                return 'Completed';
            }
        }
    })
    .filter('searchUser', function () {
        return function (arr, searchString) {
            if (!searchString) {
                return arr;
            }
            var result = [];
            searchString = searchString.toLowerCase();
            angular.forEach(arr, function (item) {
                if (item.firstName.toLowerCase().indexOf(searchString) !== -1 || item.lastName.toLowerCase().indexOf(searchString) !== -1 ||
                    item.email.toLowerCase().indexOf(searchString) !== -1 ||  item.type.toLowerCase().indexOf(searchString) !== -1 || (item.areaOfInterest.toString()).toLowerCase().indexOf(searchString) !== -1  ) {
                    result.push(item);
                }
            });
            return result;
        };
    })
    .filter('searchChallenge', function () {
        return function (arr, searchString) {
            if (!searchString) {
                return arr;
            }
            var result = [];
            searchString = searchString.toLowerCase();
            angular.forEach(arr, function (item) {
                if (item.title.toLowerCase().indexOf(searchString) !== -1 || item.description.toLowerCase().indexOf(searchString) !== -1 ||
                    item.location.toLowerCase().indexOf(searchString) !== -1 ||  item.sector.toLowerCase().indexOf(searchString) !== -1 || (item.keyword.toString()).toLowerCase().indexOf(searchString) !== -1  ) {
                    result.push(item);
                }
            });
            return result;
        };
    })
    .filter('searchText', function () {
        return function (arr, searchString) {
            if (!searchString) {
                return arr;
            }
            var result = [];
            searchString = searchString.toLowerCase();
            angular.forEach(arr, function (item) {
                if (item.title.toLowerCase().indexOf(searchString) !== -1 || item.description.toLowerCase().indexOf(searchString) !== -1 ||
                    item.location.toLowerCase().indexOf(searchString) !== -1 ||  item.sector.toLowerCase().indexOf(searchString) !== -1 || (item.keyword.toString()).toLowerCase().indexOf(searchString) !== -1  ) {
                    result.push(item);
                }
            });
            return result;
        };
    })
    .filter('searchSectorName', function () {
        return function (arr, searchString) {
            if (!searchString) {
                return arr;
            }
            var result = [];
            searchString = searchString.toLowerCase();
            angular.forEach(arr, function (item) {
                if (item.name.toLowerCase().indexOf(searchString) !== -1 ) {
                    result.push(item);
                }
            });
            return result;
        };
    })
    .filter('searchSolution', function(){
        return function(arr, searchString){
            if(!searchString){
                return arr;
            }
            var result = [];
            searchString = searchString.toLowerCase();
            angular.forEach(arr, function(item){
                if(item.name.toLowerCase().indexOf(searchString) !== -1 || item.descriptionOfSolution.toLowerCase().indexOf(searchString) !== -1 || item.location.toLowerCase().indexOf(searchString) !== -1 || item.type.toLowerCase().indexOf(searchString) !== -1 || item.affiliated.toLowerCase().indexOf(searchString) !== -1){
                    result.push(item);
                }
            });
            return result;
        };
    })
    .filter('searchResearch', function(){
        return function(arr, searchString){
            if(!searchString){
                return arr;
            }
            var result = [];
            searchString = searchString.toLowerCase();
            angular.forEach(arr, function(item){
                if(item.researchName.toLowerCase().indexOf(searchString) !== -1 || item.description.toLowerCase().indexOf(searchString) !== -1 || (item.sector.toString()).toLowerCase().indexOf(searchString) !== -1 || (item.keyword.toString()).toLowerCase().indexOf(searchString) !== -1 ){
                    result.push(item);
                }
            });
            return result;
        };
    })
    .filter('searchProjectFolder', function(){
        return function(arr, searchString){
            if(!searchString){
                return arr;
            }
            var result = [];
            searchString = searchString.toLowerCase();
            angular.forEach(arr, function(item){
                if(item.name.toLowerCase().indexOf(searchString) !== -1 ||  item.category.toLowerCase().indexOf(searchString) !== -1){
                    result.push(item);
                }
            });
            return result;
        };
    })
    .filter('searchCollaborate', function(){
        return function(arr, searchString){
            if(!searchString){
                return arr;
            }
            var result = [];
            searchString = searchString.toLowerCase();
            angular.forEach(arr, function(item){
                if(item.name.toLowerCase().indexOf(searchString) !== -1  ||  item.location.toLowerCase().indexOf(searchString) !== -1 || item.type.toLowerCase().indexOf(searchString) !== -1 ){
                    result.push(item);
                }
            });
            return result;
        };
    })
    .filter('searchProject', function(){
        return function(arr, searchString){
            if(!searchString){
                return arr;
            }
            var result = [];
            searchString = searchString.toLowerCase();
            angular.forEach(arr, function(item){
                if(item.name.toLowerCase().indexOf(searchString) !== -1  ||  item.status.toLowerCase().indexOf(searchString) !== -1
                    || item.categories.toLowerCase().indexOf(searchString) !== -1 || item.type.toLowerCase().indexOf(searchString) !== -1
                    || item.description.toLowerCase().indexOf(searchString) !== -1 || item.sector.toString().toLowerCase().indexOf(searchString) !== -1){
                    result.push(item);
                }
            });
            return result;
        };
    })
    .filter('convertToISTDate', function() {
        return function(milliseconds) {
            if(milliseconds) {
                // var date = new Date(milliseconds + new Date().getTimezoneOffset()*60000);
                var date = new Date(milliseconds);
                var year = date.getFullYear();
                var month = ('0' + (date.getMonth() + 1)).slice(-2);
                var day = ('0' + date.getDate()).slice(-2);
                return month + '/' + day + '/' + year;
            } else {
                return '';
            }
        }
    })
    .filter('convertToTimeHours', function () {
        return function (time) {
            if (time) {
                // var date = new Date(new Date(time).getTime() + new Date().getTimezoneOffset()*60000);
                var date = new Date(new Date(time).getTime());
                var year = date.getFullYear();
                var month = ('0' + (date.getMonth() + 1)).slice(-2);
                var day = ('0' + date.getDate()).slice(-2);
                var hours = ('0' + date.getHours()).slice(-2);
                var minutes = ('0' + date.getMinutes()).slice(-2);
                return (day + '/' + month + '/' + year + ' ' + hours + ':' + minutes);
            } else {
                return '';
            }
        }
    })
    .filter('convertToReportDate', function () {
        return function (time) {
            if (time) {
                // var date = new Date(new Date(time).getTime() + new Date().getTimezoneOffset()*60000);
                var date = new Date(new Date(time).getTime());
                var year = date.getFullYear();
                var month = ('0' + (date.getMonth() + 1)).slice(-2);
                var day = ('0' + date.getDate()).slice(-2);
                return (day + '/' + month + '/' + year);
            } else {
                return '';
            }
        }
    })
    .filter('convertToTime', function () {
        return function (milliseconds) {
            var date = new Date(milliseconds);
            var hours = date.getHours();
            var minutes = date.getMinutes();
            return hours.toString() + ':' + (minutes < 10 ? '0' : '') + minutes.toString();
        }
    })
    .filter('convertToUTCDate', function () {
        return function (milliseconds) {
            if (milliseconds || milliseconds != 0) {
                var UTCTime = new Date(milliseconds + new Date().getTimezoneOffset() * 60000);
                var date = new Date(UTCTime);
                var year = date.getFullYear();
                var month = ('0' + (date.getMonth() + 1)).slice(-2);
                var day = ('0' + date.getDate()).slice(-2);
                var hours = ('0' + date.getHours()).slice(-2);
                var minutes = ('0' + date.getMinutes()).slice(-2);
                return day + '/' + month + '/' + year + ' ' + hours + ':' + minutes;
            } else {
                return '';
            }
        }
    })
    .filter('capitalize', function () {
        return function (input) {
            return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
        }
    })
    .filter('searchCheck', function () {
        return function (arr, searchString) {
            if (!searchString) {
                return arr;
            }
            var result = [];
            searchString = searchString.toLowerCase();
            angular.forEach(arr, function (item) {
                if (item.checkCategoryName.toLowerCase().indexOf(searchString) !== -1) {
                    result.push(item);
                }
            });
            return result;
        };
    })
    .filter('searchTask', function () {
        return function (arr, searchString) {
            if (!searchString) {
                return arr;
            }
            var result = [];
            searchString = searchString.toLowerCase();
            angular.forEach(arr, function (item) {
                if (item.name.toLowerCase().indexOf(searchString) !== -1) {
                    result.push(item);
                }
            });
            return result;
        };
    })
    .filter('strReplace', function () {
        return function (input, from, to) {
            input = input || '';
            from = from || '';
            to = to || '';
            return input.replace(new RegExp(from, 'g'), to);
        };
    })

    .filter('removePara', function () {
        return function (stringWithHtml) {
            var strippedText = $('<div/>').html(stringWithHtml).text();
            return strippedText;
        }
    })
    .filter('dateMonthFilter', function () {
        return function (milliseconds) {
            var monthList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            var date = new Date(milliseconds);
            var month = monthList[date.getMonth() + 1];
            var day = ('0' + date.getDate()).slice(-2);
            return month + ' ' + day;
        };
    })
    .filter('discussionFollowingFilter', function (){
        return function (arr, id) {
            if(!arr.length){
                return arr;
            } else {
                var data = [];
                data = arr.map( arrayItem =>{
                   if(arrayItem.following.length) {
                        var item = arrayItem.following.filter(follower => {
                             return follower.userId == id;
                        }) || [];
                        if(item.length){
                            return arrayItem;
                        }
                   } 
                });
                return data;
            }
        }
    });

