angular.module('SVM').
    factory('ValidateService', ['$q', function ($q) {
        return {
            validateSignIn: function (signin) {
                var deferred = $q.defer();
                signin = signin || {};
                var validate = {};
                if (!signin.email) {
                    validate.general = { error: true, required: true };
                    validate.loginEmail = { error: true, require: true };
                } else if(signin.email){
                    re = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@[*[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+]*/;
                    if(!re.test(signin.email)){
                        validate.general = { error: true };
                        validate.loginEmail = {error: true, regexMatch: "Please enter a valid email address"}
                    }
                }
                if (!signin.password) {
                    validate.general = { error: true, required: true };
                    validate.loginPassword = { error: true, require: true };
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateSignUp: function (signup) {
                var deferred = $q.defer();
                signup = signup || {};
                var validate = {};
                if (!signup.firstName) {
                    validate.general = { error: true, required: true };
                    validate.firstName = { error: true, require: true };
                }
                if (!signup.lastName) {
                    validate.general = { error: true, required: true };
                    validate.lastName = { error: true, require: true };
                }
                if (!signup.email) {
                    validate.general = { error: true, required: true };
                    validate.signUpEmail = { error: true, require: true };
                } else if(signup.email){
                    re = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@[*[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+]*/;
                    if(!re.test(signup.email)){
                        validate.general = { error: true };
                        validate.signUpEmail = {error: true, regexMatch: "Please enter a valid email address"}
                    }
                }
                if (!signup.password) {
                    validate.general = { error: true, required: true };
                    validate.signUpPassword = { error: true, require: true };
                } else if(signup.password){
                    if(signup.password.length < 7) {
                        validate.general = { error: true };
                        validate.signUpPassword = {error: true, regexMatch: "Password must contain at least 7 characters!"};
                    }
                    re = /[0-9]/;
                    if(!re.test(signup.password)) {
                        validate.general = { error: true };
                        validate.signUpPassword = {error: true, regexMatch: "Password must contain at least one number (0-9)!"};
                    }
                    re = /[a-z]/;
                    if(!re.test(signup.password)) {
                        validate.general = { error: true };
                        validate.signUpPassword = {error: true, regexMatch: "Password must contain at least one lowercase letter (a-z)!"};
                    }
                    re = /[A-Z]/;
                    if(!re.test(signup.password)) {
                        validate.general = { error: true };
                        validate.signUpPassword = {error: true, regexMatch: "Password must contain at least one uppercase letter (A-Z)!"};
                    }
                    // re = /[!@#$%^&*]/;
                    // if(!re.test(signup.password)) {
                    //     validate.general = { error: true };
                    //     validate.signUpPassword = {error: true, regexMatch: "Password must contain at least one special character !, @, #, $, %, ^, &, * "};
                    // }
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateUserForgotPassword: function (forgotpassword) {
                var deferred = $q.defer();
                forgotpassword = forgotpassword || {};
                var validate = {};
                if (!forgotpassword.email) {
                    validate.general = { error: true, required: true };
                    validate.forgotPasswordEmail = { error: true, require: true };
                } else if(forgotpassword.email){
                    re = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@[*[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+]*/;
                    if(!re.test(forgotpassword.email)){
                        validate.general = { error: true };
                        validate.forgotPasswordEmail = {error: true, regexMatch: "Please enter a valid email address"}
                    }
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateUserResetPassword: function (resetpassword) {
                var deferred = $q.defer();
                resetpassword = resetpassword || {};
                var validate = {};
                if(resetpassword.password != undefined && resetpassword.password != "" && resetpassword.password == resetpassword.confirmPassword) {
                    if(resetpassword.password.length < 7) {
                        validate.general = { error: true };
                        validate.newPassword = {error: true, regexMatch: "Password must contain at least 7 characters!"};
                    }
                    re = /[0-9]/;
                    if(!re.test(resetpassword.password)) {
                        validate.general = { error: true };
                        validate.newPassword = {error: true, regexMatch: "Password must contain at least one number (0-9)!"};
                    }
                    re = /[a-z]/;
                    if(!re.test(resetpassword.password)) {
                        validate.general = { error: true };
                        validate.newPassword = {error: true, regexMatch: "Password must contain at least one lowercase letter (a-z)!"};
                    }
                    re = /[A-Z]/;
                    if(!re.test(resetpassword.password)) {
                        validate.general = { error: true };
                        validate.newPassword = {error: true, regexMatch: "Password must contain at least one uppercase letter (A-Z)!"};
                    }
                    // re = /[!@#$%^&*]/;
                    // if(!re.test(resetpassword.password)) {
                    //     validate.general = { error: true };
                    //     validate.newPassword = {error: true, regexMatch: "Password must contain at least one special character !, @, #, $, %, ^, &, * "};
                    // }
                } else {
                    if(resetpassword.password != resetpassword.confirmPassword){
                        validate.general = { error: true };
                        validate.newPassword = {error: true, regexMatch: "Please check that you've entered both New and Confirm passwords same!"};
                    } else {
                        validate.general = { error: true };
                        validate.newPassword = {error: true, regexMatch: "Please check that you've entered and confirmed your new password!"};
                    }
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateChallenge: function (challenge) {
                var deferred = $q.defer();
                challenge = challenge || {};
                var validate = {};
                if (!challenge.title) {
                    validate.general = { error: true, required: true };
                    validate.title = { error: true, require: true };
                }
                if (!challenge.description) {
                    validate.general = { error: true, required: true };
                    validate.description = { error: true, require: true };
                }
                if (!challenge.location) {
                    validate.general = { error: true, required: true };
                    validate.location = { error: true, require: true };
                }
                if (!challenge.sector) {
                    validate.general = { error: true, required: true };
                    validate.sector = { error: true, require: true };
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateEditChallenge: function (challenge) {
                var deferred = $q.defer();
                challenge = challenge || {};
                var validate = {};
                if (!challenge.title) {
                    validate.general = { error: true, required: true };
                    validate.title = { error: true, require: true };
                }
                if (!challenge.location) {
                    validate.general = { error: true, required: true };
                    validate.location = { error: true, require: true };
                }
                if (!challenge.sector) {
                    validate.general = { error: true, required: true };
                    validate.sector = { error: true, require: true };
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateUploadResource: function (resource) {
                var deferred = $q.defer();
                resource = resource || {};
                var validate = {};
                if (!resource.fileName1) {
                    validate.general = { error: true, required: true };
                    validate.fileName1 = { error: true, require: true };
                }
                if (!resource.file) {
                    validate.general = { error: true, required: true };
                    validate.file1 = { error: true, require: true };
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateWebArticles: function(article){
                var deferred = $q.defer();
                article = article[article.length-1] || {};
                var validate = {};
                if(!article.name){
                    validate.general = { error: true, required: true };
                    validate.name = { error: true, require: true };
                }
                if(!article.link){
                    validate.general = { error: true, required: true };
                    validate.link = { error: true, require: true };
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateUploadProposal: function (proposal) {
                var deferred = $q.defer();
                proposal = proposal || {};
                var validate = {};
                if (!proposal.fileName2) {
                    validate.general = { error: true, required: true };
                    validate.fileName2 = { error: true, require: true };
                }
                if (!proposal.file) {
                    validate.general = { error: true, required: true };
                    validate.file2 = { error: true, require: true };
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateProjectUploadProjects: function (projectResource) {
                var deferred = $q.defer();
                projectResource = projectResource || {};
                var validate = {};
                if (!projectResource.fileName1) {
                    validate.general = { error: true, required: true };
                    validate.fileName = { error: true, require: true };
                }
                if (!projectResource.file) {
                    validate.general = { error: true, required: true };
                    validate.file = { error: true, require: true };
                }
                if (!projectResource.description) {
                    validate.general = { error: true, required: true };
                    validate.description = { error: true, require: true };
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateSolution: function (solution) {
                var deferred = $q.defer();
                solution = solution || {};
                var validate = {};
                if (!solution.name) {
                    validate.general = { error: true, required: true };
                    validate.name = { error: true, require: true };
                }
                if (!solution.descriptionOfSolution) {
                    validate.general = { error: true, required: true };
                    validate.descriptionOfSolution = { error: true, require: true };
                }
                if (!solution.affiliated) {
                    validate.general = { error: true, required: true };
                    validate.affiliated = { error: true, require: true };
                }
                if (!solution.type) {
                    validate.general = { error: true, required: true };
                    validate.type = { error: true, require: true };
                }
                if (!solution.location) {
                    validate.general = { error: true, required: true };
                    validate.location = { error: true, require: true };
                }
                if (!solution.sector || solution.sector.length == 0) {
                    validate.general = { error: true, required: true };
                    validate.sector = { error: true, require: true };
                }
                var validImages = ['jpg', 'png', 'jpeg'];
                if(solution.logoName){
                    var fileNameExt = solution.logoName.substr(solution.logoName.lastIndexOf('.') + 1);
                    if ($.inArray(fileNameExt, validImages) == -1) {
                        validate.general = { error: true, required: true };
                        validate.logo = { error: true, require: true };
                    }
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateEditSolution: function (solution) {
                var deferred = $q.defer();
                solution = solution || {};
                var validate = {};
                if (!solution.name) {
                    validate.general = { error: true, required: true };
                    validate.name = { error: true, require: true };
                }
                if (!solution.descriptionOfSolution) {
                    validate.general = { error: true, required: true };
                    validate.descriptionOfSolution = { error: true, require: true };
                }
                if (!solution.affiliated) {
                    validate.general = { error: true, required: true };
                    validate.affiliated = { error: true, require: true };
                }
                if (!solution.type) {
                    validate.general = { error: true, required: true };
                    validate.type = { error: true, require: true };
                }
                if (!solution.location) {
                    validate.general = { error: true, required: true };
                    validate.location = { error: true, require: true };
                }
                if (!solution.sector || solution.sector.length == 0) {
                    validate.general = { error: true, required: true };
                    validate.sector = { error: true, require: true };
                }
                var validImages = ['jpg', 'png', 'jpeg'];
                if(solution.logoName){
                    var fileNameExt = solution.logoName.substr(solution.logoName.lastIndexOf('.') + 1);
                    if ($.inArray(fileNameExt, validImages) == -1) {
                        validate.general = { error: true, required: true };
                        validate.logo = { error: true, require: true };
                    }
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateProject: function (project) {
                var deferred = $q.defer();
                project = project || {};
                var validate = {};
                if (!project.name) {
                    validate.general = { error: true, required: true };
                    validate.name = { error: true, require: true };
                }
                if (!project.description) {
                    validate.general = { error: true, required: true };
                    validate.description = { error: true, require: true };
                }
                if (!project.categories) {
                    validate.general = { error: true, required: true };
                    validate.type = { error: true, require: true };
                }
                if (!project.type) {
                    validate.general = { error: true, required: true };
                    validate.projectType = { error: true, require: true };
                }
                if (!project.sector || project.sector.length == 0) {
                    validate.general = { error: true, required: true };
                    validate.sector = { error: true, require: true };
                }
                if (!project.status) {
                    validate.general = { error: true, required: true };
                    validate.status = { error: true, require: true };
                }
                if (!project.startDate) {
                    validate.general = { error: true, required: true };
                    validate.startDate = { error: true, require: true };
                }
                if (!project.endDate) {
                    validate.general = { error: true, required: true };
                    validate.endDate = { error: true, require: true };
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateProjectUpdate: function (project) {
                var deferred = $q.defer();
                project = project || {};
                var validate = {};
                if (!project.name) {
                    validate.general = { error: true, required: true };
                    validate.name = { error: true, require: true };
                }
                if (!project.sector || !project.sector.length) {
                    validate.general = { error: true, required: true };
                    validate.sector = { error: true, require: true };
                }
                if (!project.description) {
                    validate.general = { error: true, required: true };
                    validate.description = { error: true, require: true };
                }
                if (!project.status) {
                    validate.general = { error: true, required: true };
                    validate.status = { error: true, require: true };
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateProjectFolder: function (project) {
                var deferred = $q.defer();
                project = project || {};
                var validate = {};
                if (!project.name) {
                    validate.general = { error: true, required: true };
                    validate.name = { error: true, require: true };
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateEditProjectFolder: function (project) {
                var deferred = $q.defer();
                project = project || {};
                var validate = {};
                if (!project.name) {
                    validate.general = { error: true, required: true };
                    validate.name = { error: true, require: true };
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateFolderUpdate: function(folderData){
                var deferred = $q.defer();
                folderData = folderData || {};
                var validate = {};
                if (!folderData.userIds || folderData.userIds.length < 1) {
                    validate.general = { error: true, required: true };
                    validate.users = { error: true, require: true };
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateReqRes: function (req) {
                var deferred = $q.defer();
                req = req || {};
                var validate = {};
                if (!req.message) {
                    validate.general = { error: true, required: true };
                    validate.message = { error: true, require: true };
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateDiscussion: function (discussion) {
                var deferred = $q.defer();
                discussion = discussion || {};
                var validate = {};
                if (!discussion.discussionName) {
                    validate.general = { error: true, required: true };
                    validate.discussionName = { error: true, require: true };
                }
                if (!discussion.body) {
                    validate.general = { error: true, required: true };
                    validate.body = { error: true, require: true };
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateCollaborate: function (collaborate) {
                var deferred = $q.defer();
                collaborate = collaborate || {};
                var validate = {};
                if (!collaborate.name) {
                    validate.general = { error: true, required: true };
                    validate.name = { error: true, require: true };
                }
                if (!collaborate.type) {
                    validate.general = { error: true, required: true };
                    validate.type = { error: true, require: true };
                }
                if (!collaborate.type || collaborate.type.length == 0) {
                    validate.general = { error: true, required: true };
                    validate.location = { error: true, require: true };
                }
                if (!collaborate.sector || collaborate.sector.length == 0) {
                    validate.general = { error: true, required: true };
                    validate.sector = { error: true, require: true };
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateCollaborateUpdate: function (collaborate) {
                var deferred = $q.defer();
                collaborate = collaborate || {};
                var validate = {};
                if (!collaborate.name) {
                    validate.general = { error: true, required: true };
                    validate.name = { error: true, require: true };
                }
                if (!collaborate.type) {
                    validate.general = { error: true, required: true };
                    validate.type = { error: true, require: true };
                }
                if (!collaborate.location) {
                    validate.general = { error: true, required: true };
                    validate.location = { error: true, require: true };
                }
                if (!collaborate.sector || collaborate.sector.length == 0) {
                    validate.general = { error: true, required: true };
                    validate.sector = { error: true, require: true };
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateResearch: function (research) {
                var deferred = $q.defer();
                research = research || {};
                var validate = {};
                if (!research.researchName) {
                    validate.general = { error: true, required: true };
                    validate.researchName = { error: true, require: true };
                }
                if (!research.type) {
                    validate.general = { error: true, required: true };
                    validate.type = { error: true, require: true };
                }
                if (!research.sector || research.sector.length == 0) {
                    validate.general = { error: true, required: true };
                    validate.sector = { error: true, require: true };
                }
                if (!research.file) {
                    validate.general = { error: true, required: true };
                    validate.file = { error: true, require: true };
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateResearchProjectUpdate: function(researchData){
                var deferred = $q.defer();
                researchData = researchData || {};
                var validate = {};
                if (!researchData.projectIds || researchData.projectIds.length < 1) {
                    validate.general = { error: true, required: true };
                    validate.projects = { error: true, require: true };
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateEditUser: function (user) {
                var deferred = $q.defer();
                user = user || {};
                var validate = {};
                if (!user.firstName) {
                    validate.general = { error: true, required: true };
                    validate.firstName = { error: true, require: true };
                }
                if (!user.lastName) {
                    validate.general = { error: true, required: true };
                    validate.lastName = { error: true, require: true };
                }
                if (!user.type) {
                    validate.general = { error: true, required: true };
                    validate.type = { error: true, require: true };
                }
                if (!user.areaOfInterest) {
                    validate.general = { error: true, required: true };
                    validate.areaOfInterest = { error: true, require: true };
                }
                var validImages = ['jpg', 'png', 'jpeg'];
                if(user.logoName){
                    var fileNameExt = user.logoName.substr(user.logoName.lastIndexOf('.') + 1);
                    if ($.inArray(fileNameExt, validImages) == -1) {
                        validate.general = { error: true, required: true };
                        validate.logo = { error: true, require: true };
                    }
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateUserPersonalDetails: function (user) {
                var deferred = $q.defer();
                user = user || {};
                var validate = {};
                
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
            validateChangePassword: function (user) {
                var deferred = $q.defer();
                user = user || {};
                var validate = {};
                if (!user.password) {
                    validate.general = { error: true, required: true };
                    validate.password = { error: true, require: true };
                }
                if(user.newPassword != undefined && user.newPassword != "" && user.newPassword == user.confirmPassword) {
                    if(user.newPassword.length < 7) {
                        validate.general = { error: true };
                        validate.newPassword = {error: true, regexMatch: "Password must contain at least 7 characters!"};
                    }
                    re = /[0-9]/;
                    if(!re.test(user.newPassword)) {
                        validate.general = { error: true };
                        validate.newPassword = {error: true, regexMatch: "Password must contain at least one number (0-9)!"};
                    }
                    re = /[a-z]/;
                    if(!re.test(user.newPassword)) {
                        validate.general = { error: true };
                        validate.newPassword = {error: true, regexMatch: "Password must contain at least one lowercase letter (a-z)!"};
                    }
                    re = /[A-Z]/;
                    if(!re.test(user.newPassword)) {
                        validate.general = { error: true };
                        validate.newPassword = {error: true, regexMatch: "Password must contain at least one uppercase letter (A-Z)!"};
                    }
                    re = /[!@#$%^&*]/;
                    if(!re.test(user.newPassword)) {
                        validate.general = { error: true };
                        validate.newPassword = {error: true, regexMatch: "Password must contain at least one special character !, @, #, $, %, ^, &, * "};
                    }
                } else {
                    if(user.newPassword != user.confirmPassword){
                        validate.general = { error: true };
                        validate.newPassword = {error: true, regexMatch: "Please check that you've entered both New and Confirm passwords same!"};
                    } else {
                        validate.general = { error: true };
                        validate.newPassword = {error: true, regexMatch: "Please check that you've entered and confirmed your new password!"};
                    }
                }
                if (!Object.keys(validate).length) {
                    deferred.resolve();
                } else {
                    validate.error = true;
                    deferred.reject(validate);
                }
                return deferred.promise;
            },
        }
    }]);