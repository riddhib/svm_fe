angular.module('SVM')
    .factory('ResearchService', ['$http', '$q', 'ValidateService', function ($http, $q, ValidateService) {
        return {            
            getResearchDetail: function(research) {
                var defered = $q.defer();
                $http.get('/ui/research/'+ research.researchId, {
                    headers: {'token': research.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            }, 
            addResearch: function(data) {
                var defered = $q.defer();
                ValidateService.validateResearch(data)
                    .then(function () {
        	            var fd = new FormData();
        	            fd.append("upload", data.file);
                        var details = JSON.stringify({"description": data.description,"sector": data.sector,"keyword": data.keyword,"researchUrl": data.researchUrl,"author": data.author, "fileName": data.researchName, "type": data.type, "researchName": data.researchName,"userId": data.userId, "uploadBy": data.uploadBy});
                        fd.append("advanceDetails", details);
        	            $http.post('/ui/research', fd, {
        	            	transformRequest: angular.identity,
                            headers: { 'Content-Type': undefined, "token": data.token }
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
        	            return defered.promise;
                    }, function(validate){
                        defered.reject(validate);
                    });
                return defered.promise;
            }, 
            updateResearch: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/research', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            updateAcademicResearchFiles: function(obj) {
                var defered = $q.defer();                
                $http.put('/ui/researchDelete', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;                    
            },
            getReaseachList: function(token) {
                var defered = $q.defer();
                $http.get('/ui/query/research', {
                    headers: {'token': token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            },
            addProjects: function(obj) {
                var defered = $q.defer();
                ValidateService.validateResearchProjectUpdate(obj)
                    .then(function () {
                        $http.put('/ui/research/addProjects',obj, {
                            headers: {'token': obj.token}
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(error);
                        });
                        return defered.promise;
                    }, function (validate) {
                        defered.reject(validate);
                    });
                return defered.promise;
            },
            setSolutionId: function(id){
                solutionId = id;
            },
            getSolutionId: function(){
                return solutionId;
            },  
            researchFollowing: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/following/', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },  
            setResearchId: function(id){
                researchId = id;
            },
            getResearchId: function(){
                return researchId;
            },
            setResearchName: function(name){
                researchName = name;
            },
            getResearchName: function(){
                return researchName;
            },
            setPath: function(x){
                path = x;
            },
            getPath: function(){
                return path;
            }
        }
    }]);