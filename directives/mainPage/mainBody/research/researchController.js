angular.module('SVM')
    .directive('research', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/research/research.html',
            controller: 'researchController'
        }
    })
    .controller('researchController', ['$scope', 'MainService', 'ResearchService','$timeout', function($scope, MainService, ResearchService, $timeout) {
        setTimeout(function(){
          $('.select2').select2({
            minimumResultsForSearch: Infinity
          });
          $('.select2-show-search').select2({
            minimumResultsForSearch: ''
          });
        }, 100);
        $scope.solutionDetail = {};
        $scope.researchList = [];
        $scope.requiredResearchData = [];
        $scope.researchCategories = [];
        $scope.followDetails = {};
        $scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
        $scope.token = localStorage.getItem("token");
        $scope.changeLoaderStatus = MainService.setLoaderStatus;
        $scope.changeLoaderStatus(true);
        $scope.resCardViewValue = false;
        $scope.closeDialog = function(){
          $scope.researchData = {};
          $scope.validate = {};
          jQuery("#modaldemo6").modal("hide");
        } 
        function init() {
            ResearchService.getReaseachList($scope.token).then(function(response) {
                response.result.forEach(function(research){
                    $scope.following = research.following.filter(follow=> follow.userId == $scope.userDetails.userId);
                    if($scope.following.length > 0) {
                        research.isFollowing = 1;
                    } else {
                        research.isFollowing = 0;
                    }
                    $scope.requiredResearchData.push(research);
                  });
                $scope.researchList = $scope.requiredResearchData;
                $scope.researchCategories = response;
                $scope.requiredResearchData = [];
                $scope.changeLoaderStatus(false);
            }, function(error) {

            })
        }

        $scope.saveResearch = function() {          
            if ($scope.researchData) {
                $scope.researchData.file = $scope.document;
                $scope.researchData.userId =$scope.userDetails.userId;
                $scope.researchData.uploadBy = $scope.userDetails.userName;
                $scope.researchData.token = $scope.token;
                if($scope.researchData.researchUrl) {
                  let researchUrl = $scope.researchData.researchUrl.replace(/^\/\/|^.*?:(\/\/)?/, '');
                  $scope.researchData.researchUrl = researchUrl;
                }
              }
           
            ResearchService.addResearch($scope.researchData).then(function(response) {
              $scope.changeLoaderStatus(true);
              init();
              $scope.closeDialog();
              $scope.validate = {};
              $scope.name = '';
              $scope.changeLoaderStatus(false);  
            }, function(validate) {
              $scope.validate = validate;
            });
        }
        init();
        $scope.follow = function(researchId, isFollowing){
            $scope.followDetails.collectionName = 'research';
            $scope.followDetails.collectionId = researchId;
            $scope.followDetails.userId =  $scope.userDetails.userId;
            $scope.followDetails.userName =  $scope.userDetails.userName;
            $scope.followDetails.token = $scope.token;
            if(isFollowing) {
              $scope.followDetails.follow = false;
            } else {
              $scope.followDetails.follow = true;
            }
            ResearchService.researchFollowing($scope.followDetails).then(function(response) {  
              if(response.status == 'Success') {
                $scope.researchList.forEach(function(research){
                  if(research.researchId == $scope.followDetails.collectionId ){
                    research.isFollowing =  $scope.followDetails.follow ;
                  }
                });
              
              } else {
              }
              }, function(error) {
      
            });
      
        }

        $scope.clear = function(){
          $scope.researchData = {};
          $scope.validate = {};
        }  

        $scope.resChangeView = function () {
          if($scope.resCardViewValue){
            $scope.resCardViewValue = false;
          } else {
            $scope.resCardViewValue = true;
          }
        }

        $scope.loadSelect = function(){
          setTimeout(function(){
            $('#sector').select2({
              dropdownParent: $('#modaldemo6'),
              width : '100%'
            });
            $('#type').select2({
              dropdownParent: $('#modaldemo6'),
              width : '100%'
            });
          }, 100)
        }
        $scope.searchData = function () {
            $scope.searchString = '';
            $timeout(function () {
              if ($scope.sector.length == 0 || $scope.sector == "All") {
                $scope.researchList = $scope.researchCategories.result;
              } 
              else {
                var matchedResearch = [];
                  $scope.researchCategories.result.forEach(function (research) {
                    if (research.sector.includes( $scope.sector) && !matchedResearch.find(x => x.researchId === research.researchId)) {
                      matchedResearch.push(research);
                    }
                  });
                $scope.researchList = matchedResearch;
                matchedResearch = [];
              }
            }, 100);
        }
        $scope.fileNameChanged = function (file) {
            var selectedFile = file.files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
                $scope.$apply(function () {
                    $scope.src = e.target.result;
                    $scope.name = selectedFile.name;
                });
            };
            reader.readAsDataURL(selectedFile);
            $scope.$apply(function () {
                if (selectedFile != undefined) {
                    $scope.document = selectedFile;
                }
            });
          }
          $scope.upload = {};
          $scope.research = function() {
              if ($scope.upload) {
                  $scope.upload.file = $scope.document;
                  $scope.upload.name = $scope.name;
                  $scope.upload.userId =$scope.userDetails.userId;
                  $scope.upload.uploadBy = $scope.userDetails.userName;
                  $scope.upload.token = $scope.token;
              }
              ChallengeService.uploadResources($scope.upload).then(function (response) {
                  if (response.status == 'Success') {                      
                    // $scope.validate = {};
                    $scope.closeDialog();
                    init();
                  }
              }, function (validate) {
                  $scope.validate = validate;
              });
          
          }
          $scope.researchProjects = function(researchId,researchName) {
            ResearchService.setResearchId(researchId);
            ResearchService.setResearchName(researchName);
            MainService.setActiveView('researchProjectsView');
          }
          $scope.projectList =function() {
            MainService.setActiveView('projectListView');
          }
    }])