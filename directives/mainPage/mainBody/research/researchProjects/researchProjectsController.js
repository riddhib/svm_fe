angular.module('SVM')
    .directive('researchProjects', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/research/researchProjects/researchProjects.html',
            controller: 'researchProjectsController'
        }
    })
    .controller('researchProjectsController', ['$scope', 'MainService', 'ResearchService', 'ProjectService', '$timeout', function($scope, MainService, ResearchService, ProjectService, $timeout) {
        $scope.researchId = ResearchService.getResearchId();
        $scope.researchName = ResearchService.getResearchName();
        $scope.token = localStorage.getItem("token");
        $scope.projectsList = {};
        var table1 = null;

        function init() {
            ProjectService.getProjectList($scope.token).then(function(response) {
                $scope.projectsList = response.result;
                ResearchService.getResearchDetail({"researchId": $scope.researchId, "token": $scope.token}).then(function(response){
                    let researchProjects = response.result[0].projects;
                    $scope.projectsList = $scope.projectsList.map(project => {
                        project.isSelected = researchProjects.length ? (researchProjects.some(researchProject => researchProject.projectId == project.projectId) || false) : false;
                        return project;
                    });
                    $scope.projectsList = $scope.projectsList.filter(prj=> prj.type == 'Research');
                });
            }, function(error) {
                console.log("Error", error);
            });
        }
        init();
        $scope.addProjects = function() {
            setLoading();
            $scope.successMessage = null;
            $scope.errorMessage = null;
            ResearchService.addProjects({"projectIds": $scope.projectIds, "researchId": $scope.researchId, "token": $scope.token}).then(function(response) {
                if (response.status == "Success") {
                    $scope.successMessage = "Projects added successfully";
                    clearLoading();
                    setTimeout(function() {
                        $('#successMessage').fadeOut('fast');
                    }, 2500);
                } else if (response.error) {
                    $scope.errorMessage = response.error;
                    clearLoading();
                    $scope.upload = {};
                    setTimeout(function() {
                      $('#errorMessage').fadeOut('fast');
                    }, 3000);
                }
            })
        }

        $scope.researchView = function(view){
            MainService.setActiveView(view);
        }

         $scope.deselectProjects = function(){
            $scope.projectIds = [];
            $('[name=deselectProject]:checked').each(function () {
                $scope.projectIds.push($(this).val());
            });
        }
        $scope.selectProjects = function(){
            $scope.projectIds = [];
            $('[name=selectProject]:checked').each(function () {
                $scope.projectIds.push($(this).val());
            });
        }

        var setLoading = function () {
            var search = $('#addProjects');
            if (!search.data('normal-text')) {
              search.data('normal-text', search.html());
            }
            search.html(search.data('loading-text'));
        };
        
        var clearLoading = function () {
            var search = $('#addProjects');
            search.html(search.data('normal-text'));
        };  
    }])