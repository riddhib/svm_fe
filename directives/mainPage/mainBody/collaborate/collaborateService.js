angular.module('SVM')
    .factory('CollaborateService', ['$http', '$q', 'ValidateService', function ($http, $q, ValidateService) {
        var path = null;
        return {
            getCollaborateDetail: function(collaborate) {
                var defered = $q.defer();
                $http.get('/ui/collaborate/'+ collaborate.collaborateId, {
                    headers: {'token': collaborate.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            }, 
            addCollaborate: function(obj) {
                var defered = $q.defer();
                ValidateService.validateCollaborate(obj)
                    .then(function () {
                        $http.post('/ui/collaborate', obj, {
                            headers: {'token': obj.token}
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function (validate) {
                        defered.reject(validate);
                    });
                return defered.promise;
            }, 
            updateCollaborate: function(obj) {
                var defered = $q.defer();
                ValidateService.validateCollaborateUpdate(obj)
                    .then(function () {
                        $http.put('/ui/collaborate', obj, {
                            headers: {'token': obj.token}
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function (validate) {
                        defered.reject(validate);
                    });
                return defered.promise;
            },
            collaborateUpdateAbout: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/collaborate', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            collaborateUpdateInterest: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/collaborate', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            collaborateUpdatePreviousEngagement: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/collaborate', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            getCollaborateList: function(token) {
                var defered = $q.defer();
                $http.get('/ui/query/collaborate', {
                    headers: {'token': token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            },
            setCollaborateId: function(id){
                collaborateId = id;
            },
            getCollaborateId: function(){
                return collaborateId;
            },
            collaborateFollowing: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/following/', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            collaborateRating: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/userRating/', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            setPath: function(x){
                path = x;
            },
            getPath: function(){
                return path;
            }     
        }
    }]);