angular.module('SVM')
    .directive('editCollaborate', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/collaborate/editCollaborate/editCollaborate.html',
            controller: 'editCollaborateController'
        }
    })
    .controller('editCollaborateController', ['$scope', 'MainService','CollaborateService', '$sce', function($scope,MainService, CollaborateService, $sce) {
       $scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
       $scope.token = localStorage.getItem("token");
       $scope.aboutValue = true;
       $scope.interestValue = false;
       $scope.prevSvValue = false;
       $scope.ratingDetails = {};
       $scope.followDetails ={};
       $scope.collaborateId = CollaborateService.getCollaborateId();
       $scope.changeLoaderStatus = MainService.setLoaderStatus;
       $scope.changeLoaderStatus(true);
       $scope.path = CollaborateService.getPath();
        function init() {
          CollaborateService.getCollaborateDetail({'collaborateId': $scope.collaborateId, 'token': $scope.token}).then(function(response) { 
            if (response.status == 'Success') {
                $scope.singleCollaborateData=response.result[0];
                $scope.followData = response.result[0].following.filter(uid => uid.userId == $scope.userDetails.userId);
                $scope.number = 5;
                $scope.changeLoaderStatus(false);
            }
          }, function(error) {

          });
        }
        init();
       $scope.about = function(){
            $scope.aboutValue = true;
            $scope.interestValue = false;
            $scope.prevSvValue = false;
       }
        $scope.interest = function(){
                $scope.aboutValue = false;
                $scope.interestValue = true;
                $scope.prevSvValue = false;
        }
        $scope.prevSV = function(){
                $scope.aboutValue = false;
                $scope.interestValue = false;
                $scope.prevSvValue = true;
        }
        $scope.changeCollaborateView = function(view){
            MainService.setActiveView(view);
        }
        $scope.collaborateView = function() {
          MainService.setActiveView('collaborateView');
        }
        $scope.getNumber = function(num) {
          return new Array(num);   
        }
        $scope.toTrustedHTML = function( html ){
          return $sce.trustAsHtml( html );
        }
        $scope.rateCollaborator =  function(collaborateId, ratingNo) {
          $scope.ratingDetails.collectionName = 'collaborate';
          $scope.ratingDetails.collectionId = collaborateId;
          $scope.ratingDetails.ratingById =  $scope.userDetails.userId;
          $scope.ratingDetails.rating = ratingNo;
          $scope.ratingDetails.ratingBy =  $scope.userDetails.userName;
          $scope.ratingDetails.token = $scope.token;
          CollaborateService.collaborateRating($scope.ratingDetails).then(function(response) {  
            if(response.status == 'Success') {
              CollaborateService.getCollaborateDetail({'collaborateId': collaborateId, 'token': $scope.token}).then(function(response) { 
                  if (response.status == 'Success') {
                      $scope.singleCollaborateData=response.result[0];
                      $scope.number = 5;
                      $scope.changeLoaderStatus(false);
                  }
              }, function(error) {

              });
            } else {
            }
            }, function(error) {

          });

        }
        $scope.follow = function(collaborateId, isFollowing){
          $scope.followDetails.collectionName = 'collaborate';
          $scope.followDetails.collectionId = collaborateId;
          $scope.followDetails.userId =  $scope.userDetails.userId;
          $scope.followDetails.userName =  $scope.userDetails.userName; 
          $scope.followDetails.token = $scope.token;
          if(isFollowing == 1) {
            $scope.followDetails.follow = true;
          } else {
            $scope.followDetails.follow = false;
          }
          CollaborateService.collaborateFollowing($scope.followDetails).then(function(response) {  
            if(response.status == 'Success') {
             init();
            
            } else {
            }
            }, function(error) {
    
          });
    
        }


      
    }])