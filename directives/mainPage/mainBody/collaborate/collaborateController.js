angular.module('SVM')
    .directive('collaborate', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/collaborate/collaborate.html',
            controller: 'collaborateController'
        }
    })
    .controller('collaborateController', ['$scope', 'MainService','CollaborateService','$timeout', function($scope,MainService, CollaborateService, $timeout) {
        setTimeout(function(){
          $('.select2').select2({
            minimumResultsForSearch: Infinity
          });
          $('.select2-show-search').select2({
            minimumResultsForSearch: ''
          });
        }, 100);
        $scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
        $scope.token = localStorage.getItem("token");
        $scope.followDetails = {};
        $scope.collaborateDetail = {};
        $scope.requiredCollaborateData = [];
        $scope.collaborationpersona = [];
        $scope.ratingDetails = {};
        $scope.sectorView = true;
        $scope.collCardViewValue = false;
        $scope.changeLoaderStatus = MainService.setLoaderStatus;
        $scope.sectorNames = [
          {
            name:'All'
          },
          {
            name:'Agriculture'
          },
          {
            name:'Healthcare'
          },
          {
            name:'Transportation'
          },
          {
            name:'Water & Sanitation'
          },
          {
            name:'Connectivity'
          },
          {
            name:'Education'
          },
          {
            name:'Energy'
          },
          {
            name:'Livelihood'
          }
        ];
        $scope.changeCollaborateView = function(view){
            MainService.setActiveView(view);
        }
        $scope.editCollaborate = function(id){
        CollaborateService.setCollaborateId(id);
        MainService.setActiveView('editCollaborateView');
        }
        function init() {
            CollaborateService.getCollaborateList($scope.token).then(function(response) {
                response.result.forEach(function(collaborate){
                  $scope.following = collaborate.following.filter(follow=> follow.userId == $scope.userDetails.userId);
                  if($scope.following.length > 0) {
                    collaborate.isFollowing = 1;
                  } else {
                    collaborate.isFollowing = 0;
                  }
                  $scope.requiredCollaborateData.push(collaborate);
                });
                $scope.collaborationList = $scope.requiredCollaborateData;
                $scope.collaborateCategories = response;
                $scope.requiredCollaborateData = [];
                $scope.number = 5;
            }, function(error) {

            })
        }
        $scope.follow = function(collaborateId, isFollowing){
          $scope.followDetails.collectionName = 'collaborate';
          $scope.followDetails.collectionId = collaborateId;
          $scope.followDetails.userId =  $scope.userDetails.userId;
          $scope.followDetails.userName =  $scope.userDetails.userName; 
          $scope.followDetails.token = $scope.token;
          if(isFollowing) {
            $scope.followDetails.follow = false;
          } else {
            $scope.followDetails.follow = true;
          }
          CollaborateService.collaborateFollowing($scope.followDetails).then(function(response) {  
            if(response.status == 'Success') {
              $scope.collaborationList.forEach(function(collaborate){
                if(collaborate.collaborateId == $scope.followDetails.collectionId ){
                  collaborate.isFollowing =  $scope.followDetails.follow ;
                }
              });
            
            } else {
            }
            }, function(error) {
    
          });
    
        }
        $scope.rateCollaborator =  function(collaborateId, ratingNo) {
          $scope.ratingDetails.collectionName = 'collaborate';
          $scope.ratingDetails.collectionId = collaborateId;
          $scope.ratingDetails.ratingById =  $scope.userDetails.userId;
          $scope.ratingDetails.rating = ratingNo;
          $scope.ratingDetails.ratingBy =  $scope.userDetails.userName;
          $scope.ratingDetails.token = $scope.token;
          CollaborateService.collaborateRating($scope.ratingDetails).then(function(response) {  
            if(response.status == 'Success') {
              $scope.collaborationList.forEach(function(collaborate){
                if(collaborate.collaborateId == $scope.ratingDetails.collectionId ){
                  collaborate.avgRating =  response.result.avgRating ;
                  collaborate.totalRating =  response.result.totalRating ;
                }
              });
            } else {
            }
            }, function(error) {

          });

        }
        $scope.getNumber = function(num) {
          return new Array(num);   
        }
        $scope.imageUpload = function(event){
          var files = event.target.files; //FileList object
          for (var i = 0; i < files.length; i++) {
            var file = files[i];
            $scope.image1 = file.name;
            var reader = new FileReader();
            reader.onload = $scope.imageIsLoaded; 
            reader.readAsDataURL(file);
          }
        }
   
        $scope.imageIsLoaded = function(e){        
            $scope.$apply(function() {
                $scope.collaborateDetail.logo = e.target.result;
            });
        }

        $scope.clear = function(){
          $scope.collaborateDetail = {};
          $scope.validate = {};
          $("#sector").select2("val", "");
        }  

        $scope.closeDialog = function(){
          $scope.collaborateDetail = {};
          $scope.validate = {};
          jQuery("#modaldemo8").modal("hide");
        } 
        $scope.collChangeView = function () {
          if($scope.collCardViewValue){
            $scope.collCardViewValue = false;
          } else {
            $scope.collCardViewValue = true;
          }
        }
       $scope.searchData = function (sector) {
        $scope.changeLoaderStatus(true);
        CollaborateService.getCollaborateList($scope.token).then(function(response) {
            response.result.forEach(function(collaborate){
              $scope.following = collaborate.following.filter(follow=> follow.userId == $scope.userDetails.userId);
              if($scope.following.length > 0) {
                collaborate.isFollowing = 1;
              } else {
                collaborate.isFollowing = 0;
              }
              $scope.requiredCollaborateData.push(collaborate);
            });
            $scope.collaborationList = $scope.requiredCollaborateData;
            $scope.collaborateCategories = response;
            $scope.requiredCollaborateData = [];
            $scope.number = 5;
            $scope.sector = sector;
            $scope.sectorView = false;
            $scope.searchString = '';
            $timeout(function () {
              $('.select2-show-search').select2({
                minimumResultsForSearch: ''
              });
              if (sector.length == 0 || sector == "All") {
                $scope.collaborationList = $scope.collaborateCategories.result;
                $scope.collaborationpersona = $scope.collaborationList;
              } 
              else {
                var matchedCollaborate = [];
                  $scope.collaborateCategories.result.forEach(function (collaborate) {
                    if (collaborate.sector.includes(sector) && !matchedCollaborate.find(x => x.collaborateId === collaborate.collaborateId)) {
                      matchedCollaborate.push(collaborate);
                    }
                  });
                $scope.collaborationList = matchedCollaborate;
                $scope.collaborationpersona = matchedCollaborate;
                matchedCollaborate = [];
              }
            }, 100);
            $scope.changeLoaderStatus(false);
        }, function(error) {

        })
      }
      $scope.searchByPersona = function (persona) {
        $scope.searchString = '';
        $timeout(function () {
          if (persona == "All") {
            $scope.collaborationList = $scope.collaborationpersona;
          } else {
            var matchedCollaboration = [];
              $scope.collaborationpersona.forEach(function (collaborate) {
                if (collaborate.type == persona && !matchedCollaboration.find(x => x.collaborateId === collaborate.collaborateId)) {
                  matchedCollaboration.push(collaborate);
                }
              });
            $scope.collaborationList = matchedCollaboration;
            matchedCollaboration = [];
          }
        }, 100);
      }
      $scope.loadSelect = function(){
        setTimeout(function(){
          $('#type').select2({
            dropdownParent: $('#modaldemo8'),
            width : '100%'
          });
          $('#sector').select2({
            dropdownParent: $('#modaldemo8'),
            width : '100%'
          });
        }, 100)
      }
        $scope.saveCollaborate = function() {
          $scope.changeLoaderStatus(true);
          if($scope.image1) {
            $scope.collaborateDetail.logoName = $scope.image1;
          }
          if($scope.image2) {
            $scope.collaborateDetail.logoName = $scope.image2;
          }
          $scope.collaborateDetail.token = $scope.token;
          CollaborateService.addCollaborate($scope.collaborateDetail).then(function(response) {
            if (response.status == 'Success') {
              $scope.collaborateDetail = {};
              $scope.validate = {};
              jQuery("#modaldemo8").modal("hide");
              init();
              $scope.changeLoaderStatus(false);
            }
          }, function(validate) {
            $scope.validate = validate;
            $scope.changeLoaderStatus(false);
          })
        }
        $scope.collaborateView = function() {
            $scope.sector = null;
            $scope.sectorView = true;
        }
        init();
    }])