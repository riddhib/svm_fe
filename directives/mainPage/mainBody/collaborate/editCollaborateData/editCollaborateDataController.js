angular.module('SVM')
    .directive('editCollaborateData', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/collaborate/editCollaborateData/editCollaborateData.html',
            controller: 'editCollaborateDataController'
        }
    })
    .controller('editCollaborateDataController', ['$scope', 'MainService','CollaborateService',function($scope,MainService, CollaborateService) {
        setTimeout(function(){
          $('.select2').select2({
            minimumResultsForSearch: Infinity
          });
          $('.select2-show-search').select2({
            minimumResultsForSearch: ''
          });
          $('#about').summernote({
              placeholder: 'About..',
              height: 150,
              tooltip: false
            });
            $('#prevEngagement').summernote({
              placeholder: 'Previous Engeagements..',
              height: 150,
              tooltip: false
            });
            $('#interest').summernote({
              placeholder: 'Interests..',
              height: 150,
              tooltip: false
            });
        }, 200);
        $scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
        $scope.token = localStorage.getItem("token");
        $scope.collaborateUpdateData = {};
        $scope.singleCollaborateData = {};
        $scope.collaborateUpdateAbout = {};
        $scope.collaborateUpdateInterest = {};
        $scope.collaborateUpdatePreviousEngagement ={};
        $scope.collaborateId = CollaborateService.getCollaborateId();
        function init(){
            CollaborateService.getCollaborateDetail({'collaborateId': $scope.collaborateId, 'token': $scope.token}).then(function(response) { 
                if (response.status == 'Success') {
                    $scope.singleCollaborateData=response.result[0];
                    var markupStr1 = $scope.singleCollaborateData.about;
                    var markupStr2 = $scope.singleCollaborateData.interests;
                    var markupStr3 = $scope.singleCollaborateData.previousSV;
                    $('#about').summernote('code', markupStr1);
                    $('#prevEngagement').summernote('code', markupStr3);
                    $('#interest').summernote('code', markupStr2);
                }
            }, function(error) {
     
            });
        }
        init();
        $scope.collaboratePrimaryDetails = function() {
            if($scope.image1) {
                $scope.collaborateUpdateData.logoName = $scope.image1;
            }
            if($scope.image2) {
            $scope.collaborateUpdateData.logoName = $scope.image2;
            }
            setLoading("primaryDetails");
            $scope.successMessage1 = null;
            $scope.errorMessage1 = null;
            $scope.collaborateUpdateData.collaborateId = $scope.collaborateId;
            $scope.collaborateUpdateData.name = $scope.singleCollaborateData.name;
            $scope.collaborateUpdateData.sector = $scope.singleCollaborateData.sector;
            $scope.collaborateUpdateData.location = $scope.singleCollaborateData.location;
            $scope.collaborateUpdateData.keyword =$scope.singleCollaborateData.keyword;
            $scope.collaborateUpdateData.type =$scope.singleCollaborateData.type;
            if($scope.singleCollaborateData.website) {
                $scope.collaborateUpdateData.website = $scope.singleCollaborateData.website.replace(/^\/\/|^.*?:(\/\/)?/, '');
            }
            $scope.collaborateUpdateData.logo = $scope.singleCollaborateData.logo; 
            $scope.collaborateUpdateData.token = $scope.token;
            CollaborateService.updateCollaborate($scope.collaborateUpdateData).then(function(response) {  
                if(response.status == 'Success') {
                    $scope.validate = {};
                    CollaborateService.getCollaborateDetail({'collaborateId': $scope.collaborateId, 'token': $scope.token}).then(function(response) { 
                        if (response.status == 'Success') {
                            $scope.singleCollaborateData=response.result[0];
                            var markupStr1 = $scope.singleCollaborateData.about;
                            var markupStr2 = $scope.singleCollaborateData.interests;
                            var markupStr3 = $scope.singleCollaborateData.previousSV;
                            $('#about').summernote('code', markupStr1);
                            $('#prevEngagement').summernote('code', markupStr3);
                            $('#interest').summernote('code', markupStr2);
                        }
                    }, function(error) {
             
                    }); 
                    $scope.successMessage1 = "Collaborate details updated successfully";
                    clearLoading("primaryDetails");
                    setTimeout(function() {
                        $('#successMessage1').fadeOut('fast');
                    }, 2500);
                } else if(response.error){
                    $scope.validate = {};
                    $scope.errorMessage1 = response.error;
                    clearLoading("primaryDetails");
                    setTimeout(function() {
                      $('#errorMessage1').fadeOut('fast');
                    }, 2500);
                }
            }, function(validate) {
                clearLoading("primaryDetails");
                $scope.validate = validate;
            });
        }
        $scope.imageUpload = function(event){
            var files = event.target.files; //FileList object
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                var reader = new FileReader();
                $scope.image1 = file.name;
                reader.onload = $scope.imageIsLoaded; 
                reader.readAsDataURL(file);
            }
       }
   
       $scope.imageIsLoaded = function(e){        
            $scope.$apply(function() {
                $scope.singleCollaborateData.logo = e.target.result;
            });
       }
        $scope.about = function() {
            setLoading("about");
            $scope.successMessage2 = null;
            $scope.errorMessage2 = null;
            $scope.collaborateUpdateAbout.collaborateId = $scope.collaborateId;
            $scope.collaborateUpdateAbout.about =$('#about').summernote('code');
            $scope.collaborateUpdateAbout.token = $scope.token;
            CollaborateService.collaborateUpdateAbout($scope.collaborateUpdateAbout).then(function(response) {  
                if(response.status == 'Success') {
                    $scope.validate = {};
                    CollaborateService.getCollaborateDetail({'collaborateId': $scope.collaborateId, 'token': $scope.token}).then(function(response) { 
                        if (response.status == 'Success') {
                            $scope.singleCollaborateData=response.result[0];
                            var markupStr1 = $scope.singleCollaborateData.about;
                            var markupStr2 = $scope.singleCollaborateData.interests;
                            var markupStr3 = $scope.singleCollaborateData.previousSV;
                            $('#about').summernote('code', markupStr1);
                            $('#prevEngagement').summernote('code', markupStr3);
                            $('#interest').summernote('code', markupStr2);
                        }
                    }, function(error) {
             
                    });  
                    $scope.successMessage2 = "Collaborate details updated successfully";
                    clearLoading("about");
                    setTimeout(function() {
                        $('#successMessage2').fadeOut('fast');
                    }, 2500);
                } else if(response.error){
                    $scope.validate = {};
                    $scope.errorMessage2 = response.error;
                    clearLoading("about");
                    setTimeout(function() {
                      $('#errorMessage2').fadeOut('fast');
                    }, 2500);
                }
            }, function(error) {
            });
        }
        $scope.interest = function(){
            setLoading("interests");
            $scope.successMessage3 = null;
            $scope.errorMessage3 = null;
            $scope.collaborateUpdateInterest.collaborateId = $scope.collaborateId;
            $scope.collaborateUpdateInterest.interests = $('#interest').summernote('code');
            $scope.collaborateUpdateInterest.token = $scope.token;
            CollaborateService.collaborateUpdateInterest($scope.collaborateUpdateInterest).then(function(response) {  
                if(response.status == 'Success') {
                    $scope.validate = {};
                    CollaborateService.getCollaborateDetail({'collaborateId': $scope.collaborateId, 'token': $scope.token}).then(function(response) { 
                        if (response.status == 'Success') {
                            $scope.singleCollaborateData=response.result[0];
                            var markupStr1 = $scope.singleCollaborateData.about;
                            var markupStr2 = $scope.singleCollaborateData.interests;
                            var markupStr3 = $scope.singleCollaborateData.previousSV;
                            $('#about').summernote('code', markupStr1);
                            $('#prevEngagement').summernote('code', markupStr3);
                            $('#interest').summernote('code', markupStr2);
                        }
                    }, function(error) {
             
                    });  
                    $scope.successMessage3 = "Collaborate details updated successfully";
                    clearLoading("interests");
                    setTimeout(function() {
                        $('#successMessage3').fadeOut('fast');
                    }, 2500);
                } else if(response.error){
                    $scope.validate = {};
                    $scope.errorMessage3 = response.error;
                    clearLoading("interests");
                    setTimeout(function() {
                      $('#errorMessage3').fadeOut('fast');
                    }, 2500);
                }
            }, function(error) {
            });
        }
        $scope.prevEngagement = function(){
            setLoading("prevEngagement");
            $scope.successMessage4 = null;
            $scope.errorMessage4 = null;
            $scope.collaborateUpdatePreviousEngagement.collaborateId = $scope.collaborateId;
            $scope.collaborateUpdatePreviousEngagement.previousSV = $('#prevEngagement').summernote('code');
            $scope.collaborateUpdatePreviousEngagement.token = $scope.token;
            CollaborateService.collaborateUpdatePreviousEngagement($scope.collaborateUpdatePreviousEngagement).then(function(response) {  
                if(response.status == 'Success') {
                    $scope.validate = {};
                    CollaborateService.getCollaborateDetail({'collaborateId': $scope.collaborateId, 'token': $scope.token}).then(function(response) { 
                        if (response.status == 'Success') {
                            $scope.singleCollaborateData=response.result[0];
                            var markupStr1 = $scope.singleCollaborateData.about;
                            var markupStr2 = $scope.singleCollaborateData.interests;
                            var markupStr3 = $scope.singleCollaborateData.previousSV;
                            $('#about').summernote('code', markupStr1);
                            $('#prevEngagement').summernote('code', markupStr3);
                            $('#interest').summernote('code', markupStr2);
                        }
                    }, function(error) {
             
                    }); 
                    $scope.successMessage4 = "Collaborate details updated successfully";
                    clearLoading("prevEngagement");
                    setTimeout(function() {
                        $('#successMessage4').fadeOut('fast');
                    }, 2500);
                } else if(response.error){
                    $scope.validate = {};
                    $scope.errorMessage4 = response.error;
                    clearLoading("prevEngagement");
                    setTimeout(function() {
                      $('#errorMessage4').fadeOut('fast');
                    }, 2500);
                }
            }, function(error) {
            });
        }
        $scope.collaborateView = function(view) {
            MainService.setActiveView(view);
        }
        var setLoading = function (id) {
            if(id=="primaryDetails"){
                var search = $('#primaryDetails');
            } else if(id=="about"){
                var search = $('#about');
            } else if(id=="interests"){
                var search = $('#interests');
            } else if(id=="prevEngagement"){
                var search = $('#prevEngagement');
            } 
            if (!search.data('normal-text')) {
              search.data('normal-text', search.html());
            }
            search.html(search.data('loading-text'));
        };
        var clearLoading = function (id) {
            if(id=="primaryDetails"){
                var search = $('#primaryDetails');
            } else if(id=="about"){
                var search = $('#about');
            } else if(id=="interests"){
                var search = $('#interests');
            } else if(id=="prevEngagement"){
                var search = $('#prevEngagement');
            } 
            search.html(search.data('normal-text'));
        };
      
    }])