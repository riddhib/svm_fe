angular.module('SVM')
  .directive('villageBulletinData', function () {
    return {
      restrict: 'E',
      scope: false,
      templateUrl: './directives/mainPage/mainBody/villageBulletin/villageBulletinData/villageBulletinData.html',
      controller: 'VillageBulletinDataController'
    }
  })
  .controller('VillageBulletinDataController', ['$scope', 'MainService', 'LoginService', 'ChallengeService', 'UserService','$timeout','DiscussionService', function ($scope, MainService, LoginService, UserService, $timeout,) {
    setTimeout(function(){
      $('.select2-show-search').select2({
        minimumResultsForSearch: ''
      });
      // Toggles
      $('.toggle').toggles({
        on: true,
        height: 26
      });
    }, 100);
    $scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
    $scope.changeLoaderStatus = MainService.setLoaderStatus;
    $scope.cardViewValue = false;
    $scope.information = function () {
      MainService.setActiveView('villageBulletinInformationView');
    }

    $scope.changeView = function () {
      $scope.cardViewValue = $scope.cardView;
    }
    $scope.changePage = function(view) {
        MainService.setActiveView(view);
    }
  }])
