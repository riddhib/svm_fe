angular.module('SVM')
  .directive('villageBulletinInformation', function () {
    return {
      restrict: 'E',
      scope: false,
      templateUrl: './directives/mainPage/mainBody/villageBulletin/villageBulletinInformation/villageBulletinInformation.html',
      controller: 'villageBulletinInformationController'
    }
  })
  .controller('villageBulletinInformationController', ['$scope', 'MainService', 'LoginService', 'ChallengeService', 'UserService','$timeout','DiscussionService', function ($scope, MainService, LoginService, UserService, $timeout,) {
      
    setTimeout(function(){
        $('.select2-show-search').select2({
            minimumResultsForSearch: ''
        });

        // Summernote editor
        $('#opportunity').summernote({
            placeholder: 'General Details..',
            height: 150,
            tooltip: false
        });
    }, 100);
    $scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
    $scope.changeLoaderStatus = MainService.setLoaderStatus;
    $scope.genDetailsValue = true;
    $scope.reqValue = false;
    $scope.respnseValue = false;
    $scope.applyValue = false;   
    $scope.genDetailsShow = function() {
        $scope.genDetailsValue = true;
        $scope.reqValue = false;
        $scope.respnseValue = false;
        $scope.applyValue = false;   
    }
    $scope.painPointsShow = function() {
        $scope.genDetailsValue = false;
        $scope.reqValue = true;
        $scope.respnseValue = false;
        $scope.applyValue = false;  
       
    }
    $scope.locationProfileShow = function() {
        $scope.genDetailsValue = false;
        $scope.reqValue = false;
        $scope.respnseValue = true;
        $scope.applyValue = false;  
    }
      $scope.impactShow = function() {
        $scope.genDetailsValue = false;
        $scope.reqValue = false;
        $scope.respnseValue = false;
        $scope.applyValue = true;  
    }
    $scope.changePage = function(view) {
        MainService.setActiveView(view);
    }
  }])

