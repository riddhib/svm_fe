angular.module('SVM')
    .directive('villageBulletin', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/villageBulletin/villageBulletin.html',
            controller: 'villageBulletinController'
        }
    })
    .controller('villageBulletinController', ['$scope', 'MainService', 'VillageBulletinService','$timeout', function($scope, MainService, VillageBulletinService, $timeout) {
    	$scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
    	$scope.sectorNames = [
    	  {
            name:'All'
          },
          {
            name:'Agriculture'
          },
          {
            name:'Healthcare'
          },
          {
            name:'Transportation'
          },
          {
            name:'Water & Sanitation'
          },
          {
            name:'Connectivity'
          },
          {
            name:'Education'
          },
          {
            name:'Energy'
          },
          {
            name:'Livelihood'
          }
        ];
        $scope.sectorView = true;
        $scope.changeLoaderStatus = MainService.setLoaderStatus;
        $scope.searchData = function (sector) {
          $scope.sector = sector;
          MainService.setActiveView('villageBulletinDataView');
          //$scope.sectorView = false;        
        }
        $scope.villageBulletinHome = function() {
          $scope.sector = null;
          $scope.sectorView =  true;
        }
    }]);