angular.module('SVM')
    .factory('DiscussionService', ['$http', '$q', 'ValidateService',  function ($http, $q, ValidateService) {
        var pathDetails = null;
        var path=null;
        return {
            addReply: function(data){
                var defered = $q.defer();
                $http.put('/ui/replyToDiscussion', data, {
                    headers: {'token': data.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            createThread: function (objectData){
                var defered = $q.defer();
                $http.post('/ui/discussionThread', objectData, {
                    headers: {'token': objectData.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            getDiscussion: function(discussion) {
                var defered = $q.defer();
                $http.get('/ui/discussion/'+ discussion.discussionId, {
                    headers: {'token': discussion.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            }, 
            addDiscussions: function(obj) {
                var defered = $q.defer();
                ValidateService.validateDiscussion(obj)
                    .then(function () {
                        $http.post('/ui/discussion', obj, {
                            headers: {'token': obj.token}
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function (validate) {
                        defered.reject(validate);
                    });
                return defered.promise;
            }, 
            discussionFollowing: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/following/', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            getDiscussionList: function(token) {
                var defered = $q.defer();
                $http.get('/ui/query/discussion', {
                    headers: {'token': token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            },
            getCategoryDiscussionList: function(user) {
                var defered = $q.defer();
                $http.get('/ui/category/discussionList/'+user.userId, {
                    headers: {'token': user.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            },
            getDiscussionThread: function(discussionThread){
                var defered = $q.defer();
                $http.get('/ui/discussionThread/'+discussionThread.discussionThreadId, {
                    headers: {'token': discussionThread.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            },
            getSampleDiscussionThread: function (){
                var defered = $q.defer();
                $http.get('/ui/sampleDiscussionThread/', {
                    headers: {'token': token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            },
            getSubCategoryList: function(category){
                var defered = $q.defer();
                $http.get('/ui/getSubCategories/'+category.category, {
                    headers: {'token': category.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            },
            updatedDiscussions: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/discussion/', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            vote: function(obj){
                var defered = $q.defer();
                $http.put('/ui/voteComments', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            setPath: function(pathInfo){
                pathDetails = pathInfo;
            },
            getPath: function(){
                return pathDetails;
            },
            setDiscussionProfilePath: function(profilePath) {
                path= profilePath;
            },
            getDiscussionProfilePath: function() {
                return path;
            }

        }
    }]);