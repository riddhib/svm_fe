angular.module('SVM')
    .directive('discussions', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/discussions/discussions.html',
            controller: 'discussionsController'
        }
    })
    .controller('discussionsController', ['$scope', 'MainService', 'LoginService', 'DiscussionService','UserService', 
    '$timeout', '$sce', '$filter', function ($scope, MainService, LoginService, DiscussionService,UserService, $timeout, $sce, $filter) {
        $scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
        $scope.token = localStorage.getItem("token");
        $scope.tabs = ['Challenges', 'Solutions', 'Research', 'Projects', 'General'];
        var discussionData, intialData = {};
        $scope.discussionProfile = {};
        $scope.changeLoaderStatus = MainService.setLoaderStatus;
        $scope.path= DiscussionService.getPath();
        DiscussionService.setPath('');
        var view = 'discussion';
        $scope.userDiscussions = {};
        var discussions = MainService.getDiscussionThread();
        $scope.getDiscussionList = function () {
            $scope.changeLoaderStatus(true);
            DiscussionService.getCategoryDiscussionList({"userId": $scope.userDetails.userId, "token": $scope.token}).then(function (response) {
                if (response.status == 'Success') {
                    $scope.discussionData = response.result;
                    $scope.changeLoaderStatus(false);
                    discussionData = angular.copy($scope.discussionData);
                }
            });
        }
        $scope.getThread = function (discussion) {
            $scope.changeLoaderStatus(true);
            if (discussion.discussionThreadId > 0 ) {
                DiscussionService.getDiscussionThread({"discussionThreadId": discussion.discussionThreadId, "token": $scope.token}).then(function (response) {
                    let newThreadMessages = [];
                    response.result[0].message.forEach(msg=>{
                        let newDate = moment(msg.createdDate).format("MM/DD/YYYY HH:mm:ss");
                        msg.createdDate = newDate;
                        newThreadMessages.push(msg);
                    });
                    $scope.thread = response.result[0];
                    $scope.thread.message = newThreadMessages;
                    $scope.thread.body = discussion.body;
                    // MainService.setDiscussionThread();
                    $scope.changeLoaderStatus(false);
                }, function (error) {
                });
            } else {
                $scope.thread = {};
                $scope.changeLoaderStatus(false);
            }
        }
        $scope.toTrustedHTML = function (html) {
            return $sce.trustAsHtml(html);
        }
        $scope.changeView = function (discussion, view) {
            if (view == 'discussion') {
                $scope.getDiscussionList();
            } else {
                $scope.getThread(discussion);
            }
            $scope.viewDetails = {
                view: view,
                data: discussion
            };
        }
        if (Object.keys(discussions).length){
            view = 'discussionThread';
            DiscussionService.getDiscussion({"discussionId": discussions.discussionId, "token": $scope.token}).then(function (response) {
                intialData = response.result[0];
                $scope.changeView(intialData, view)
            }, function (error) {
                console.log(error);
            });
        } else {
            $scope.changeView(intialData, view);
        }

        $scope.categoryChanged = function (category) {
            if (category == 'General') {
                $scope.subCategory = [{
                    id: 1,
                    name: 'General'
                }]
            } else {
                DiscussionService.getSubCategoryList({"category": category, "token": $scope.token}).then(function (response) {
                    if (response.status == 'Success') {
                        var subCategory = response.result;
                        $scope.subCategory = []
                        if (category == 'Projects') {
                            $scope.subCategory = subCategory.map(function (category) {
                                return {
                                    id: category.projectId,
                                    name: category.name
                                }
                            });
                        } else if (category == 'Challenges') {
                            $scope.subCategory = subCategory.map(function (category) {
                                return {
                                    id: category.challengeId,
                                    name: category.title
                                }
                            });
                        } else if (category == 'Researches') {
                            $scope.subCategory = subCategory.map(function (category) {
                                return {
                                    id: category.researchId,
                                    name: category.researchName
                                }
                            });
                        }

                    } else {
                    }
                }, function (error) {

                });
            }
            setTimeout(function () {
                $('#project').select2({
                    dropdownParent: $('#modaldemo6')
                });
            }, 100)

        }

        $scope.filterChanged = function (filterString){
            $scope.discussionData = angular.copy(discussionData);
            if(filterString == 'All'){
                $scope.discussionData = $filter('filter') ($scope.discussionData, '');
            } else if (filterString == 'Following' ) {
                $scope.discussionData = $filter ('discussionFollowingFilter')($scope.discussionData, $scope.userDetails.userId)
            } else {
                $scope.discussionData = $filter ('filter')($scope.discussionData, filterString, 'category');
            }
        }

        $scope.addDiscussion = function (discussion) {
            $scope.discussion = {};
            if (discussion) $scope.discussion = discussion;
            $scope.discussion.createdBy = $scope.userDetails.userId;
            $scope.discussion.token = $scope.token;
            $scope.discussion.category = "General";
            DiscussionService.addDiscussions($scope.discussion).then(function (response) {
                $scope.changeLoaderStatus(true);
                if (response.status == 'Success') {
                    $scope.getDiscussionList();
                    $('#modaldemo6').modal('hide');
                }
                $scope.changeLoaderStatus(false);
            }, function (validate) {
                $scope.validate = validate;
            })
        }

        $scope.follow = function (discussion) {
            $scope.followDetails = {}
            $scope.followDetails.collectionName = 'discussion';
            $scope.followDetails.collectionId = discussion.discussionId;
            $scope.followDetails.userId = $scope.userDetails.userId;
            $scope.followDetails.userName = $scope.userDetails.userName;
            $scope.followDetails.token = $scope.token;
            if (discussion.isFollowing) {
                $scope.followDetails.follow = false;
            } else {
                $scope.followDetails.follow = true;
            }
            if(!$scope.followDetails.follow) {
                var index =  discussion.following.indexOf( follow => {
                    return follow.userId = $scope.userDetails.userId;
                });
                discussion.following.splice(index, 1);
            } else {
                discussion.following.push({
                    userId: $scope.userDetails.userId,
                    userName: $scope.followDetails.userName
                });
            }
            discussion.isFollowing = !discussion.isFollowing;

            DiscussionService.discussionFollowing($scope.followDetails).then(function (response) {
                if (response.status == 'Success') {
                    discussionData = angular.copy(response.result);
                } else {
                }
            }, function (error) {

            });

        }
        
        $scope.addComment = function () {
            var message = {
                "message": editor.comment.getContent(),
                "title": editor.title.getContent()
            }
            var editorLength = {
                commentLength : (editor.comment.elements[0].innerText.replace(/[^A-Z0-9]/ig, "")).length,
                titleLength: (editor.title.elements[0].innerText.replace(/[^A-Z0-9]/ig, "")).length
            }
            if( editorLength.commentLength && editorLength.titleLength){
                var data = {
                    "createdBy": $scope.userDetails.userId,
                    "category": $scope.viewDetails.data.category,
                    "parentId": $scope.viewDetails.data.parentId,
                    "discussionId": $scope.viewDetails.data.discussionId,
                    "message": message.message,
                    "title": message.title,
                    "token": $scope.token
                }
                if ($scope.viewDetails.data.discussionThreadId) {
                    data.discussionThreadId = $scope.viewDetails.data.discussionThreadId;
                }
                DiscussionService.createThread(data).then(function (response) {
                    if (response.status == 'Success') {
                        editor.comment.setContent('');
                        editor.title.setContent('');
                        $scope.viewDetails.data.discussionThreadId = response.result[0].discussionThreadId;
                        $scope.getThread(response.result[0]);
                    }
                }, function (error) {

                });
            }

        }

        $scope.isActivated = function(votes){
            return (votes.filter(vote => {
                return vote == $scope.userDetails.userId;
            }) || []).length;
        }

        $scope.voted = function(message, type){
            var voteObject = {
                "discussionThreadId": $scope.thread.discussionThreadId,
                "messageId": message.messageId
            }
            voteObject[type] = [$scope.userDetails.userId];
            voteObject.token = $scope.token;
            DiscussionService.vote(voteObject).then(function(response){
                if (response.status == 'Success') {
                    message.reply = '';
                    $scope.getThread($scope.thread);
                }
            }, function(error){

            });
        }

        $scope.showReplyBox = function(message, replyDetail){
            $scope.replyDetail = false;
            $scope.thread.message.forEach( threadMessage => {
                threadMessage.showReplyBox = false;
            });
            message.showReplyBox = true;
            if(replyDetail){
                $scope.replyDetail = angular.copy(replyDetail);
            }
        }
        
        $scope.addReply = function(message){
            message.reply = 
            $scope.showReplyBox(message);
        }

        $scope.replyTo = function (currentThread, message) {
            var repliedTo = message.postedBy.userId, reply =  message.reply;
            if($scope.replyDetail){
                repliedTo = $scope.replyDetail.userId;
                reply = message.reply;
            }
            var replyObject = {
                "discussionThreadId": currentThread.discussionThreadId,
                "repliedBy": $scope.userDetails.userId,
                "repliedTo": repliedTo,
                "messageId": message.messageId,
                "message": reply,
                "token": $scope.token
            }
            DiscussionService.addReply(replyObject).then(function (response) {
                if (response.status == 'Success') {
                    message.reply = '';
                    $scope.getThread($scope.thread);
                }
            }, function (error) {

            });
        }
        $scope.viewProfile =function(id,discussionName,discussionId) {
            $scope.discussionProfile.discussionName = discussionName;
            $scope.discussionProfile.discussionId = discussionId;
            DiscussionService.setDiscussionProfilePath($scope.discussionProfile);
            UserService.setUserId(id);
            MainService.setActiveView('userProfileView');
        } 
        $scope.clear = function(){
            $scope.userDiscussions = {};
            $scope.validate = {};
        }

        $scope.closeDialog = function(){
            $scope.userDiscussions = {};
            $scope.validate = {};
            jQuery("#modaldemo6").modal("hide");
        }

        $scope.getUserChat = function(view, user) {
            $scope.$parent.changeView(view, user);
        }
        $scope.setView = function(view) {
            MainService.setActiveView(view);
        }
    }]);