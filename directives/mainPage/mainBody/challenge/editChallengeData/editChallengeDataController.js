angular.module('SVM')
    .directive('editChallengeData', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/challenge/editChallengeData/editChallengeData.html',
            controller: 'editChallengeDataController'
        }
    })
    .controller('editChallengeDataController', ['$scope', 'MainService','LoginService', 'ChallengeService', function($scope,MainService,LoginService, ChallengeService) {
        setTimeout(function(){
            $('.select2-show-search').select2({
                minimumResultsForSearch: ''
            });
            // Inline editor
            var editor = new MediumEditor('.editable');

            // Summernote editor
            $('#generalDetails').summernote({
            placeholder: 'Challenge Summary..',
            height: 150,
            tooltip: false
            });
            $('#impact').summernote({
            placeholder: 'Impact..',
            height: 150,
            tooltip: false
            });
            $('#painPoints').summernote({
            placeholder: 'Challenge Details..',
            height: 150,
            tooltip: false
            });
        }, 200);
        $scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
        $scope.token = localStorage.getItem("token");
        $scope.newchallengeData={};
        $scope.challengeDetails = {};
        $scope.painPointsData = {};
        $scope.generalDetailsData = {};
        $scope.expressInterestData = {};
        $scope.deleteResource = {};
        $scope.impactData ={};
        $scope.challengeId = ChallengeService.getChallengeId();
        $scope.changeLoaderStatus = MainService.setLoaderStatus;
        function init() {
            ChallengeService.getchallengeData({'challengeId': $scope.challengeId, 'token': $scope.token}).then(function(response) { 
                if (response.status == 'Success') {
                    $scope.singleChallengeData=response.result.challenge;
                    var markupStr1 = $scope.singleChallengeData.generalDetails;
                    var markupStr2 = $scope.singleChallengeData.impact;
                    var markupStr3 = $scope.singleChallengeData.painPoints;
                    $scope.files = response.result.files;
                    $('#generalDetails').summernote('code', markupStr1);
                    $('#impact').summernote('code', markupStr2);
                    $('#painPoints').summernote('code', markupStr3);
                    $scope.changeLoaderStatus(false);
                }
            });
            ChallengeService.getResourceData(challengeId,'proposal', $scope.token).then(function(response) { 
                if (response.status == 'Success') {
                    $scope.getDocumentData=response.result;
                }
            });
        }
        init();
        $scope.updateChallengeDetails = function(){
            setLoading("primaryDetails");
            $scope.successMessage1 = null;
            $scope.errorMessage1 = null;
            $scope.challengeDetails.challengeId = $scope.challengeId;
            $scope.challengeDetails.location = $scope.singleChallengeData.location;
            $scope.challengeDetails.title = $scope.singleChallengeData.title;
            $scope.challengeDetails.keyword = $scope.singleChallengeData.keyword;
            $scope.challengeDetails.description = $scope.singleChallengeData.description;
            $scope.challengeDetails.sector = $scope.singleChallengeData.sector;
            $scope.challengeDetails.token = $scope.token;
            ChallengeService.updateChallenge($scope.challengeDetails).then(function(response) {  
                if(response.status == 'Success') {
                    $scope.validate = {};
                    $scope.challengeDetails = $scope.challengeDetails;
                    init();
                    $scope.successMessage1 = "Challenge details updated successfully";
                    clearLoading("primaryDetails");
                    setTimeout(function() {
                        $('#successMessage1').fadeOut('fast');
                    }, 2500);
                } else if(response.error){
                    $scope.validate = {};
                    $scope.errorMessage1 = response.error;
                        clearLoading("primaryDetails");
                        setTimeout(function() {
                      $('#errorMessage1').fadeOut('fast');
                    }, 2500);
                }
            }, function(validate) {
                clearLoading("primaryDetails");
                $scope.validate = validate;
            });
        }
        $scope.updateGeneralDetails = function(){
            setLoading("generalDetails");
            $scope.successMessage2 = null;
            $scope.errorMessage2 = null;
            $scope.generalDetailsData.challengeId = $scope.challengeId;
            $scope.generalDetailsData.generalDetails = $('#generalDetails').summernote('code');
            $scope.generalDetailsData.token = $scope.token;
            ChallengeService.updateGeneralDetails($scope.generalDetailsData).then(function(response) {  
                if(response.status == 'Success') {
                    $scope.validate = {};
                    $('#generalDetails').summernote('code', $scope.generalDetailsData.generalDetails);
                    $scope.successMessage2 = "Challenge details updated successfully";
                    clearLoading("generalDetails");
                    setTimeout(function() {
                        $('#successMessage2').fadeOut('fast');
                    }, 2500);
                } else if(response.error){
                    $scope.validate = {};
                    $scope.errorMessage2 = response.error;
                    clearLoading("generalDetails");
                    setTimeout(function() {
                      $('#errorMessage2').fadeOut('fast');
                    }, 3000);
                }
            }, function(validate) {
                clearLoading("generalDetails");
                $scope.validate = validate;
            });
        }
        $scope.updatePainPoints = function(){
            setLoading("painPoints");
            $scope.successMessage3 = null;
            $scope.errorMessage3 = null;
            $scope.painPointsData.challengeId = $scope.challengeId;
            $scope.painPointsData.painPoints = $('#painPoints').summernote('code');
            $scope.painPointsData.token = $scope.token;
            ChallengeService.updatePainPoints($scope.painPointsData).then(function(response) {  
                if(response.status == 'Success') {
                    $scope.validate = {};
                    $('#painPoints').summernote('code', $scope.painPointsData.painPoints);
                    $scope.successMessage3 = "Challenge details updated successfully";
                    clearLoading("painPoints");
                    setTimeout(function() {
                        $('#successMessage3').fadeOut('fast');
                    }, 2500);
                } else if(response.error){
                    $scope.validate = {};
                    $scope.errorMessage3 = response.error;
                    clearLoading("painPoints");
                    setTimeout(function() {
                      $('#errorMessage3').fadeOut('fast');
                    }, 3000);
                }
            }, function(validate) {
                clearLoading("painPoints");
                $scope.validate = validate;
            });
        }
        $scope.updateImpact = function(){
            setLoading("impact");
            $scope.successMessage4 = null;
            $scope.errorMessage4 = null;
            $scope.impactData.challengeId =  $scope.challengeId;
            $scope.impactData.impact = $('#impact').summernote('code');
            $scope.impactData.token = $scope.token;
            ChallengeService.updateImpact($scope.impactData).then(function(response) {  
                if(response.status == 'Success') {
                    $scope.validate = {};
                    $('#impact').summernote('code', $scope.impactData.impact);
                    $scope.successMessage4 = "Challenge details updated successfully";
                    clearLoading("impact");
                    setTimeout(function() {
                        $('#successMessage4').fadeOut('fast');
                    }, 2500);             
                } else if(response.error){
                    $scope.validate = {};
                    $scope.errorMessage4 = response.error;
                    clearLoading("impact");
                    setTimeout(function() {
                      $('#errorMessage4').fadeOut('fast');
                    }, 3000);
                }
            }, function(validate) {
                clearLoading("impact");
                $scope.validate = validate;
            });
        }

      $scope.signOut = function(){
        MainService.setActiveView('signUpView');
        localStorage.clear();
      }
      $scope.viewProfile = function(){
        MainService.setActiveView('userProfileView');
      }
      $scope.editProfile = function(){
        MainService.setActiveView('editProfileView');
      }
      $scope.fileNameChanged1 = function (file) {
        var selectedFile = file.files[0];
        var reader = new FileReader();
        reader.onload = function (e) {
            $scope.$apply(function () {
                $scope.src = e.target.result;
                $scope.name = selectedFile.name;
                $scope.selectedResource = selectedFile.name;
            });
        };
        reader.readAsDataURL(selectedFile);
        $scope.$apply(function () {
            if (selectedFile != undefined) {
                $scope.document = selectedFile;
            }
        });
      }
      $scope.fileNameChanged2 = function (file) {
        var selectedFile = file.files[0];
        var reader = new FileReader();
        reader.onload = function (e) {
            $scope.$apply(function () {
                $scope.src = e.target.result;
                $scope.name = selectedFile.name;
                $scope.selectedProposal = selectedFile.name;
            });
        };
        reader.readAsDataURL(selectedFile);
        $scope.$apply(function () {
            if (selectedFile != undefined) {
                $scope.document = selectedFile;
            }
        });
      }
      $scope.upload = {};
        $scope.resource = function() {
            setLoading("resource");
            $scope.successMessage5 = null;
            $scope.errorMessage5 = null;
            if ($scope.upload) {            
                $scope.upload.file = $scope.document;
                $scope.upload.userId =$scope.userDetails.userId;
                $scope.upload.description = $scope.upload.description1;
                $scope.upload.challengeId = $scope.challengeId;
                $scope.upload.uploadBy = $scope.userDetails.userName;
                $scope.upload.token = $scope.token;
            }
            ChallengeService.uploadResources($scope.upload).then(function (response) {
                if (response.status == 'Success') {
                    $scope.successMessage5 = "Resource Uploaded Successfully";
                    clearLoading("resource");
                    $scope.selectedResource = null;
                    $scope.upload = {};
                    setTimeout(function() {
                        $('#successMessage5').fadeOut('fast');
                    }, 2500);
                    init();
                } else if(response.error){
                    $scope.errorMessage5 = response.error;
                    clearLoading("resource");
                    $scope.upload = {};
                    setTimeout(function() {
                      $('#errorMessage5').fadeOut('fast');
                    }, 3000);
                }
                $scope.validate = {};
            }, function (validate) {
                clearLoading("resource");
                $scope.validate = validate;
            });
        
        } 
        $scope.uploadProposal = function() {
            setLoading("proposal");
            $scope.successMessage65 = null;
            $scope.errorMessage65 = null;
            if ( $scope.upload) {             
                $scope.upload.file = $scope.document;
                $scope.upload.fileName = $scope.upload.fileName;
                $scope.upload.description = $scope.upload.description2;
                $scope.upload.userId =$scope.userDetails.userId;
                $scope.upload.challengeId = $scope.challengeId;
                $scope.upload.uploadBy = $scope.userDetails.userName;
                $scope.upload.token = $scope.token;
            }
            ChallengeService.uploadProposal($scope.upload).then(function (response) {
                if (response.status == 'Success') {
                    $scope.successMessage6 = "Proposal Uploaded Successfully";
                    clearLoading("proposal");
                    $scope.selectedProposal = null;
                    $scope.upload = {};
                    setTimeout(function() {
                        $('#successMessage6').fadeOut('fast');
                    }, 2500);
                    init();
                } else if(response.error){
                    $scope.errorMessage6 = response.error;
                    clearLoading("proposal");
                    $scope.upload = {};
                    setTimeout(function() {
                      $('#errorMessage6').fadeOut('fast');
                    }, 3000);
                }
                $scope.validate = {};
            }, function (validate) {
                clearLoading("proposal");
                $scope.validate = validate;
            });
        }
        $scope.challengeViews = function(view) {
            MainService.setActiveView(view);
        }
        var setLoading = function (id) {
            if(id=="resource"){
                var search = $('#resource');
            } else if(id=="proposal"){
                var search = $('#proposal');
            } else if(id=="primaryDetails"){
                var search = $('#primaryDetails');
            } else if(id=="impact"){
                var search = $('#impact');
            } else if(id=="generalDetails"){
                var search = $('#generalDetails');
            } else if(id=="painPoints"){
                var search = $('#painPoints');
            }
            if (!search.data('normal-text')) {
              search.data('normal-text', search.html());
            }
            search.html(search.data('loading-text'));
        };
        var clearLoading = function (id) {
            if(id=="resource"){
                var search = $('#resource');
            } else if(id=="proposal"){
                var search = $('#proposal');
            } else if(id=="primaryDetails"){
                var search = $('#primaryDetails');
            } else if(id=="impact"){
                var search = $('#impact');
            } else if(id=="generalDetails"){
                var search = $('#generalDetails');
            } else if(id=="painPoints"){
                var search = $('#painPoints');
            }
            search.html(search.data('normal-text'));
            // init();
        }; 
        $scope.deleteResources = function(fileId) {
            $scope.deleteResource.challengeId = $scope.challengeId;
            $scope.deleteResource.resources = $scope.singleChallengeData.resources.filter(art=> art != fileId) ;
            $scope.deleteResource.token = $scope.token;
            ChallengeService.deleteFiles($scope.deleteResource).then(function(response) {  
                if(response.status == 'Success') {
                    $scope.validate = {};
                    init();     
                    $scope.deleteResource = {};         
                }
            }, function() {
                
            });
        }
        $scope.deleteExpressIntrest = function(fileId) {
            $scope.expressInterestData.challengeId = $scope.challengeId;
            $scope.expressInterestData.proposal = $scope.singleChallengeData.proposal.filter(art=> art != fileId) ;
            $scope.expressInterestData.token = $scope.token;
            ChallengeService.deleteFiles($scope.expressInterestData).then(function(response) {  
                if(response.status == 'Success') {
                    $scope.validate = {};
                    init();     
                    $scope.expressInterestData = {};         
                }
            }, function() {
                
            });
        }    
    }])