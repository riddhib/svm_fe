angular.module('SVM')
  .directive('challenge', function () {
    return {
      restrict: 'E',
      scope: false,
      templateUrl: './directives/mainPage/mainBody/challenge/challenge.html',
      controller: 'ChallengeController'
    }
  })
  .controller('ChallengeController', ['$scope', 'MainService', 'LoginService', 'ChallengeService', 'UserService','$timeout','DiscussionService', function ($scope, MainService, LoginService, ChallengeService, UserService, $timeout, DiscussionService) {
    setTimeout(function(){
      $('.select2-show-search').select2({
        minimumResultsForSearch: ''
      });
      // Toggles
      $('.toggle').toggles({
        on: true,
        height: 26
      });
    }, 100);
    $scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
    $scope.token = localStorage.getItem("token");
    $scope.newchallengeData = {};
    var discussionData ={};
    $scope.path ={};
    // $scope.keyword=[];
    $scope.challengeData = [];
    $scope.requiredChallengeData = [];
    $scope.followDetails = {};
    $scope.ratingDetails = {};
    $scope.changeLoaderStatus = MainService.setLoaderStatus;
    $scope.changeLoaderStatus(true);
    $scope.cardViewValue = false;
    function init() {
      ChallengeService.getchallenge($scope.token).then(function (response) {
        if (response.status == 'Success') {
          response.result.forEach(function(challenge){
            let round = Math.round(challenge.avgRating);
            challenge.avgRating = round;
            $scope.following = challenge.following.filter(follow=> follow.userId == $scope.userDetails.userId);
            if($scope.following.length > 0) {challenge.isFollowing = 1} else {challenge.isFollowing = 0};
            $scope.requiredChallengeData.push(challenge);
          });
          $scope.challengeData = $scope.requiredChallengeData;
          $scope.challengeList = response.result;
          $scope.challengeCategories = response;
          $scope.requiredChallengeData = [];
          $scope.number = 5;
          var matchedChecks = [];
          $scope.changeLoaderStatus(false);
        }
      });
    }

    init();

    $scope.searchData = function () {
      $scope.searchString = '';
      $timeout(function () {
        if ($scope.sector.length == 0 || $scope.sector == "All") {
          $scope.challengeData = $scope.challengeCategories.result;
        } 
        else {
          var matchedChallenge = [];
            $scope.challengeCategories.result.forEach(function (challenge) {
              if (challenge.sector.includes( $scope.sector) && !matchedChallenge.find(x => x.challengeId === challenge.challengeId)) {
                matchedChallenge.push(challenge);
              }
            });
          $scope.challengeData = matchedChallenge;
          matchedChallenge = [];
        }
      }, 100);
    }

    $scope.follow = function(challengeId, isFollowing){
      $scope.followDetails.collectionName = 'challenge';
      $scope.followDetails.collectionId = challengeId;
      $scope.followDetails.userId =  $scope.userDetails.userId;
      $scope.followDetails.userName =  $scope.userDetails.userName;
      $scope.followDetails.token = $scope.token;
      if(isFollowing) {
        $scope.followDetails.follow = false;
      } else {
        $scope.followDetails.follow = true;
      }
      ChallengeService.challengeFollowing($scope.followDetails).then(function(response) {  
        if(response.status == 'Success') {
          $scope.challengeData.forEach(function(challenge){
            if(challenge.challengeId == $scope.followDetails.collectionId ){
              challenge.isFollowing =  $scope.followDetails.follow ;
            }
          });
        
        } else {
        }
        }, function(error) {

      });

    }

    $scope.ratingChallenge =  function(challengeId, ratingNo) {
      $scope.ratingDetails.collectionName = 'challenge';
      $scope.ratingDetails.collectionId = challengeId;
      $scope.ratingDetails.ratingById =  $scope.userDetails.userId;
      $scope.ratingDetails.rating = ratingNo;
      $scope.ratingDetails.ratingBy =  $scope.userDetails.userName;
      $scope.ratingDetails.token = $scope.token;
      ChallengeService.challengeRating($scope.ratingDetails).then(function(response) {  
        if(response.status == 'Success') {
          $scope.challengeData.forEach(function(challenge){
            if(challenge.challengeId == $scope.ratingDetails.collectionId ){
              challenge.avgRating =  response.result.avgRating ;
              challenge.totalRating =  response.result.totalRating ;
            }
          });
        } else {
        }
        }, function(error) {

      });

    }
    
    $scope.signOut = function () {
      MainService.setActiveView('signUpView');
      localStorage.clear();
    }
    
    $scope.editchallenge = function (id) {
      MainService.setActiveView('editChallengeView');
      $scope.id = id;
      ChallengeService.setChallengeId($scope.id);
    }

    $scope.changeView = function () {
      $scope.cardViewValue = $scope.cardView;
    }

    $scope.discussion = function (x) {
      discussionData.body  = x.description;
      discussionData.discussionName = x.title;
      discussionData.discussionId  = x.discussions[0];
      $scope.path.pathName ='Challenges';
      $scope.path.challengeName = x.title;
      DiscussionService.setPath($scope.path);
      MainService.setDiscussionThread(discussionData);
      ChallengeService.setChallengeId(x.challengeId);
      MainService.setActiveView('discussionsView');
    }

    $scope.getNumber = function(num) {
      return new Array(num);   
    }

    $scope.clear = function(){
      $scope.challenge = {};
      $scope.validate = {};
    }  

    $scope.closeDialog = function(){
      $scope.challenge = {};
      $scope.validate = {};
      jQuery("#modaldemo6").modal("hide");
    }  

    $scope.addChallenge = function (challengeDetails) {
      $scope.changeLoaderStatus(true);
      $scope.newchallengeData = challengeDetails;
      if($scope.keyWord) $scope.newchallengeData.keyword = [$scope.keyWord];
      $scope.newchallengeData.token = $scope.token;
      ChallengeService.addChallenges($scope.newchallengeData).then(function (response) {
        if (response.status == 'Success') {
          init();
          $scope.challenge = {};
          $scope.validate = {};
          jQuery("#modaldemo6").modal("hide");
          $scope.changeLoaderStatus(false);
        }
      }, function (validate) {
        $scope.validate = validate;
        $scope.changeLoaderStatus(false);
      });
    }

    $scope.loadSelect = function(){
      setTimeout(function(){
        $('#challengeType').select2({
          dropdownParent: $('#modaldemo6'),
          width : '100%'
        });
      }, 100)
    }
  }])
