angular.module('SVM')
    .factory('ChallengeService', ['$http', '$q', 'ValidateService', function ($http, $q, ValidateService) {
        var path = null;
        return {
            setChallengeId: function(id){
                challengeId = id;
            },
            getChallengeId: function(){
                return challengeId;
            },      
            getchallenge: function(token) {
                var defered = $q.defer();
                $http.get('/ui/query/challenge/', {
                    headers: {'token': token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            },
            getchallengeData: function(challenge) {
                var defered = $q.defer();
                $http.get('/ui/editChallenge/'+ challenge.challengeId, {
                    headers: {'token': challenge.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            }, 
            addChallenges: function(obj) {
                var defered = $q.defer();
                ValidateService.validateChallenge(obj)
                    .then(function () {
                        $http.post('/ui/challenge', obj, {
                            headers: {'token': obj.token}
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function(validate){
                        defered.reject(validate);
                    });
                return defered.promise;
            }, 
            updateChallenge: function(obj) {
                var defered = $q.defer();
                ValidateService.validateEditChallenge(obj)
                    .then(function () {
                        $http.put('/ui/challenge/', obj, {
                            headers: {'token': obj.token}
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function(validate){
                        defered.reject(validate);
                    });
                return defered.promise;
            },
            updateGeneralDetails : function(obj) {
                var defered = $q.defer();
                $http.put('/ui/challenge/', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            updatePainPoints : function(obj) {
                var defered = $q.defer();
                $http.put('/ui/challenge/', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            updateImpact : function(obj) {
                var defered = $q.defer();
                $http.put('/ui/challenge/', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            challengeFollowing: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/following/', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            challengeRating: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/userRating/', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            uploadProposal: function (data) {
                var defered = $q.defer();
                ValidateService.validateUploadProposal(data)
                    .then(function () {
        	            var fd = new FormData();
        	            fd.append("upload", data.file);
                        var details = JSON.stringify({"description": data.description,"challengeId": data.challengeId,"fileName": data.fileName2,"userId": data.userId, "uploadBy": data.uploadBy});
                        fd.append("advanceDetails", details);
        	            $http.post('/ui/uploadProposal', fd, {
        	            	transformRequest: angular.identity,
                            headers: { 'Content-Type': undefined, 'token' : data.token }
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
        	            return defered.promise;
                    }, function(validate){
                        defered.reject(validate);
                    });
                return defered.promise;
            },
            deleteFiles: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/challenge', obj, {
                    headers: {token: obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            uploadResources: function (data) {
                var defered = $q.defer();
                ValidateService.validateUploadResource(data)
                    .then(function () {
                        var fd = new FormData();
                        fd.append("upload", data.file);
                        var details = JSON.stringify({"description": data.description,"challengeId": data.challengeId,"fileName": data.fileName1,"userId": data.userId, "uploadBy": data.uploadBy});
                        fd.append("advanceDetails", details);
                        $http.post('/ui/challenge/uploadResources', fd, {
                            transformRequest: angular.identity,
                            headers: { 'Content-Type': undefined, 'token' : data.token }
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function(validate){
                        defered.reject(validate);
                    });
                return defered.promise;
            },
            getResourceData: function(id,name, token) {
                var defered = $q.defer();
                $http.get('/ui/getDocuments/'+ id +'/'+name, {
                    headers: {'token': token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            },
            setPath: function(x){
                path = x;
            },
            getPath: function(){
                return path;
            }
        }
    }]);