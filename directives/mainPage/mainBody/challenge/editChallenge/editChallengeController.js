angular.module('SVM')
    .directive('editChallenge', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/challenge/editChallenge/editChallenge.html',
            controller: 'editChallengeController'
        }
    })
    .controller('editChallengeController', ['$scope', 'MainService','LoginService', 'ChallengeService', '$sce','DiscussionService', function($scope,MainService,LoginService, ChallengeService, $sce, DiscussionService) {
        $scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
        $scope.token = localStorage.getItem("token");
        var discussionData = {};
        $scope.newchallengeData={};
        $scope.followDetails ={};
        $scope.ratingDetails = {};
        $scope.discussionPath = {};
        $scope.genDetailsValue = true;
        $scope.painPointsValue = false;
        $scope.locationValue = false;
        $scope.impactValue = false;
        $scope.proposalValue = false;
        $scope.resourceValue = false;
        $scope.successMessage2 = null;
        $scope.errorMessage2 = null;
        $scope.successMessage1 = null;
        $scope.errorMessage1 = null;
        $scope.getDocumentData = [];
        $scope.changeLoaderStatus = MainService.setLoaderStatus;
        $scope.changeLoaderStatus(true);
        $scope.path = ChallengeService.getPath();
       
        $scope.challengeId = ChallengeService.getChallengeId();
        function init() {
          ChallengeService.getchallengeData({'challengeId': $scope.challengeId, 'token': $scope.token}).then(function(response) { 
              if (response.status == 'Success') {
                  $scope.singleChallengeData=response.result.challenge;
                  $scope.number = 5;
                  let round = Math.round($scope.singleChallengeData.avgRating);
                  $scope.following = $scope.singleChallengeData.following.filter(follow=> follow.userId == $scope.userDetails.userId);
                  if($scope.following.length > 0) {$scope.singleChallengeData.isFollowing = 1} else {$scope.singleChallengeData.isFollowing = 0};
                  $scope.singleChallengeData.avgRating = round;
                  $scope.changeLoaderStatus(false);
              }
          });
        }
        init();
        $scope.getNumber = function(num) {
          return new Array(num);   
        }

        $scope.toTrustedHTML = function( html ){
          return $sce.trustAsHtml( html );
        }
        
        $scope.genDetailsShow = function() {
          $scope.genDetailsValue = true;
          $scope.painPointsValue = false;
          $scope.locationValue = false;
          $scope.impactValue = false;
          $scope.proposalValue = false;
          $scope.resourceValue = false;
        }
        $scope.painPointsShow = function() {
          $scope.genDetailsValue = false;
          $scope.painPointsValue = true;
          $scope.locationValue = false;
          $scope.impactValue = false;
          $scope.proposalValue = false;
          $scope.resourceValue = false;
        }
        $scope.locationProfileShow = function() {
          $scope.genDetailsValue = false;
          $scope.painPointsValue = false;
          $scope.locationValue = true;
          $scope.impactValue = false;
          $scope.proposalValue = false;
          $scope.resourceValue = false;
        }
        $scope.impactShow = function() {
          $scope.genDetailsValue = false;
          $scope.painPointsValue = false;
          $scope.locationValue = false;
          $scope.impactValue = true;
          $scope.proposalValue = false;
          $scope.resourceValue = false;
        }
        $scope.resourceShow = function(){
          $scope.genDetailsValue = false;
          $scope.painPointsValue = false;
          $scope.locationValue = false;
          $scope.impactValue = false;
          $scope.proposalValue = false;
          $scope.resourceValue = true;
          $scope.getDocument($scope.challengeId,'resources');
        }
        $scope.proposalShow = function(){
          $scope.genDetailsValue = false;
          $scope.painPointsValue = false;
          $scope.locationValue = false;
          $scope.impactValue = false;
          $scope.proposalValue = true;
          $scope.resourceValue = false;
          $scope.getDocument($scope.challengeId,'proposal');
        }
        $scope.getDocument = function(challengeId,name){
          ChallengeService.getResourceData(challengeId,name, $scope.token).then(function(response) { 
            if (response.status == 'Success') {
                $scope.getDocumentData=response.result;
            }
          });
        }
        $scope.ratingChallenge =  function(challengeId, ratingNo) {
          $scope.ratingDetails.collectionName = 'challenge';
          $scope.ratingDetails.collectionId = challengeId;
          $scope.ratingDetails.ratingById =  $scope.userDetails.userId;
          $scope.ratingDetails.rating = ratingNo;
          $scope.ratingDetails.ratingBy =  $scope.userDetails.userName;
          $scope.ratingDetails.token = $scope.token;
          ChallengeService.challengeRating($scope.ratingDetails).then(function(response) {
            if(response.status == 'Success') {
              ChallengeService.getchallengeData({'challengeId': $scope.challengeId, 'token': $scope.token}).then(function(response) { 
                if (response.status == 'Success') {
                    $scope.singleChallengeData=response.result.challenge;
                    $scope.number = 5;
                    let round = Math.round($scope.singleChallengeData.avgRating);
                    $scope.singleChallengeData.avgRating = round;
                    $scope.changeLoaderStatus(false);
                }
              });
            } else {
            }
          }, function(error) {

          });

        }
        $scope.discussion = function (x) {
          discussionData.body  = x.description;
          discussionData.discussionName = x.title;
          discussionData.discussionId  = x.discussions[0];
          $scope.discussionPath.pathName ='Challenges';
          $scope.discussionPath.challengeName = x.title;
          DiscussionService.setPath($scope.discussionPath);
          MainService.setDiscussionThread(discussionData);
          MainService.setActiveView('discussionsView');
        }
        $scope.fileNameChanged1 = function (file) {
          var selectedFile = file.files[0];
          var reader = new FileReader();
          reader.onload = function (e) {
              $scope.$apply(function () {
                  $scope.src = e.target.result;
                  $scope.name = selectedFile.name;
                  $scope.selectedResource = selectedFile.name;
              });
          };
          reader.readAsDataURL(selectedFile);
          $scope.$apply(function () {
              if (selectedFile != undefined) {
                  $scope.document = selectedFile;
              }
          });
        }
        $scope.fileNameChanged2 = function (file) {
          var selectedFile = file.files[0];
          var reader = new FileReader();
          reader.onload = function (e) {
              $scope.$apply(function () {
                  $scope.src = e.target.result;
                  $scope.name = selectedFile.name;
                  $scope.selectedProposal = selectedFile.name;
              });
          };
          reader.readAsDataURL(selectedFile);
          $scope.$apply(function () {
              if (selectedFile != undefined) {
                  $scope.document = selectedFile;
              }
          });
        }
        $scope.upload = {};
        $scope.resource = function() {
            setLoading("resource");
            $scope.successMessage1 = null;
            $scope.errorMessage1 = null;        
            $scope.upload.file = $scope.document;
            $scope.upload.userId =$scope.userDetails.userId;
            $scope.upload.challengeId = $scope.challengeId;
            $scope.upload.uploadBy = $scope.userDetails.userName;
            $scope.upload.token = $scope.token;
            ChallengeService.uploadResources($scope.upload).then(function (response) {
                if (response.status == 'Success') {
                    $scope.successMessage1 = "Resource Uploaded Successfully";
                    $scope.upload = {};
                    $scope.selectedResource = '';
                    $scope.getDocument($scope.challengeId,'resources');
                    setTimeout(function() {
                      $('#successMessage1').fadeOut('fast');
                      clearLoading("resource");
                    }, 2500);
                } else if(response.error){
                    $scope.errorMessage1 = response.error;
                    $scope.upload = {};
                    setTimeout(function() {
                      $('#errorMessage1').fadeOut('fast');
                      clearLoading("resource");
                    }, 3000);
                }
                $scope.validate = {};
            }, function (validate) {
                clearLoading("resource");
                $scope.validate = validate;
                var firstValidatedElement = Object.keys($scope.validate)[0];
            });
        }

        $scope.uploadProposal = function() {
          $scope.successMessage2 = null;
          $scope.errorMessage2 = null;
          setLoading("proposal");
          $scope.upload.file = $scope.document;
          $scope.upload.fileName = $scope.upload.fileName;
          $scope.upload.userId =$scope.userDetails.userId;
          $scope.upload.challengeId = $scope.challengeId;
          $scope.upload.uploadBy = $scope.userDetails.userName;
          $scope.upload.token = $scope.token;
          ChallengeService.uploadProposal($scope.upload).then(function (response) {
              if (response.status == 'Success') {
                $scope.successMessage2 = "Proposal Uploaded Successfully";
                $scope.upload = {};
                $scope.selectedProposal = '';
                $scope.getDocument($scope.challengeId,'proposal');
                setTimeout(function() {
                  $('#successMessage2').fadeOut('fast');
                  clearLoading("proposal");
                }, 2500);
              } else if(response.error){
                $scope.errorMessage2 = response.error;
                $scope.upload = {};
                setTimeout(function() {
                  $('#errorMessage2').fadeOut('fast');
                  clearLoading("proposal");
                }, 3000);
              }
              $scope.validate = {};
          }, function (validate) {
              clearLoading("proposal");
              $scope.validate = validate;
              var firstValidatedElement = Object.keys($scope.validate)[0];
          });
        }

        $scope.follow = function(challengeId, isFollowing){
          $scope.followDetails.collectionName = 'challenge';
          $scope.followDetails.collectionId = challengeId;
          $scope.followDetails.userId =  $scope.userDetails.userId;
          $scope.followDetails.userName =  $scope.userDetails.userName;
          $scope.followDetails.token = $scope.token;
          if(isFollowing) {
            $scope.followDetails.follow = false;
          } else {
            $scope.followDetails.follow = true;
          }
          ChallengeService.challengeFollowing($scope.followDetails).then(function(response) {  
            if(response.status == 'Success') {
              init();            
            } else {
            }
            }, function(error) {

          });

        }

        $scope.signOut = function(){
          MainService.setActiveView('signUpView');
          localStorage.clear();
        }
        $scope.editchallengeData = function() {
          MainService.setActiveView('editChallengeDataView');
        }
        $scope.viewProfile = function(){
          MainService.setActiveView('userProfileView');
        }
        $scope.editProfile = function(){
          MainService.setActiveView('editProfileView');
        }
        $scope.challengeViews = function() {
          MainService.setActiveView('challengeView');
        }
        var setLoading = function (id) {
            if(id=="resource"){
                var search = $('#resource');
            } else if(id=="proposal"){
                var search = $('#proposal');
            }
            if (!search.data('normal-text')) {
              search.data('normal-text', search.html());
            }
            search.html(search.data('loading-text'));
        };
        var clearLoading = function (id) {
            if(id=="resource"){
                var search = $('#resource');
            } else if(id=="proposal"){
                var search = $('#proposal');
            }
            search.html(search.data('normal-text'));
            init();
        };  
      
    }])