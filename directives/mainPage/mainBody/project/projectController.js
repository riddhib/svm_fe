angular.module('SVM')
    .directive('project', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/project/project.html',
            controller: 'projectController'
        }
    })
    .controller('projectController', ['$scope', 'MainService', 'ProjectService', function($scope, MainService, ProjectService) {
        $scope.token = localStorage.getItem("token");
        setTimeout(function(){
          $('.select2').select2({
            minimumResultsForSearch: Infinity
          });
        }, 100);
        $scope.folderList = {};
        $scope.project = {};
        $scope.folder = [];
        $scope.type = '';
        $scope.projectList = function(folder) {
            ProjectService.setProjectFolderId(folder.projectFolderId);
            ProjectService.setProjectFolderName(folder.name);
            MainService.setActiveView('projectListView');
        }
        function init() {
            $scope.requiredProjectFolderData = [];
            ProjectService.getFolderList($scope.token).then(function(response) {
                response.result.forEach(function(projectFolder){
                    $scope.allowedAccess = projectFolder.users.filter(folder=> folder.userId == $scope.userDetails.userId);
                    if($scope.allowedAccess.length > 0) {
                        projectFolder.isAccessible = 1;
                    } else {
                        projectFolder.isAccessible = 0;
                    }
                    $scope.requiredProjectFolderData.push(projectFolder);
                  });
                $scope.folderList = $scope.requiredProjectFolderData;
            }, function(error) {

            });
        }
        init();
        $scope.saveFolder = function() {
            $scope.project.token = $scope.token;
            ProjectService.addFolder($scope.project).then(function(response) {
                $scope.project = {};
                $scope.validate = {};
                jQuery("#modaldemo6").modal("hide");
                init();
            }, function(validate) {
              $scope.validate = validate;
            })
        }
        $scope.clear = function(){
            $scope.projectData = {};
            $scope.validate = {};
        }  
        $scope.getFolderData = function (folderId) {
            ProjectService.getFolderDetail({"folderId": folderId, "token": $scope.token}).then(function(response) {
                $scope.folder = response.result[0];
            }, function() {
            })
        }
        $scope.editFolderData = function (data) {
            data.token = $scope.token;
            ProjectService.editFolder(data).then(function(response) {
                $scope.closeDialogEditFolder();
                init();
            }, function(validate) {
                $scope.validate = validate;
            })
        }
        $scope.closeDialog = function(){
            $scope.projectData = {};
            $scope.validate = {};
            jQuery("#modaldemo6").modal("hide");
        } 
        $scope.closeDialogEditFolder = function(){
            $scope.validate = {};
            jQuery("#modaldemo7").modal("hide");
        } 
        $scope.clearEditFolder = function(){
            $scope.folder = {};
            $scope.validate = {};
        }
    }])