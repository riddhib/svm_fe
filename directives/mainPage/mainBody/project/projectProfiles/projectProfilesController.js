angular.module('SVM')
    .directive('projectProfiles', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/project/projectProfiles/projectProfiles.html',
            controller: 'projectProfilesController'
        }
    })
    .controller('projectProfilesController', ['$scope', 'MainService', 'ProjectService','$timeout', function($scope, MainService, ProjectService, $timeout) {
    	$scope.projectId = ProjectService.getProjectId();
    }])