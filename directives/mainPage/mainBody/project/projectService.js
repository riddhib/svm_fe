angular.module('SVM')
    .factory('ProjectService', ['$http', '$q', 'ValidateService', function ($http, $q, ValidateService) {
        var folderData = null;
        var projectFolderName = null;
        var projectName = null;
        var projectUsersFilter = 'All';
        return {
            getProjectDetail: function(project) {
                var defered = $q.defer();
                $http.get('/ui/getProject/'+ project.projectId, {
                    headers: {'token': project.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            }, 
            addProject: function(obj) {
                var defered = $q.defer();
                ValidateService.validateProject(obj)
                    .then(function () {
                        $http.post('/ui/project', obj, {
                            headers: {'token': obj.token}
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function (validate) {
                        defered.reject(validate);
                    });
                return defered.promise;
            }, 
            addRequest: function(obj) {
                var defered = $q.defer();
                ValidateService.validateReqRes(obj)
                    .then(function () {
                        $http.put('/ui/project/requests', obj, {
                            headers: {token: obj.token}
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function (validate) {
                        defered.reject(validate);
                    });
                return defered.promise;
            },
            deleteRequest: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/project/deleteRequest', obj, {
                    headers: {token: obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            addResponse: function(data) {
                var defered = $q.defer();
                ValidateService.validateReqRes(data)
                    .then(function () {
                        var fd = new FormData();
                        if(data.file) {
                            fd.append("upload", data.file);
                            var details = JSON.stringify({"message": data.message, "fileName": data.name,"userId": data.userId,"projectId":data.projectId,"requestId":data.requestId, "uploadBy": data.uploadBy,"category":"resources"});
                            fd.append("advanceDetails", details);
                            $http.put('/ui/project/requestsResponse', fd, {
                                transformRequest: angular.identity,
                                headers: { 'Content-Type': undefined, "token": data.token }
                            }).then(function(response) {
                                defered.resolve(response.data);
                            }, function(error) {
                                defered.reject(response.data);
                            });
                            return defered.promise;
                        } else {
                            $http.put('/ui/project/requestsResponse', data, {
                                headers: {"token": data.token}
                            }).then(function(response) {
                                defered.resolve(response.data);
                            }, function(error) {
                                defered.reject(response.data);
                            });
                            return defered.promise;
                        }
                    }, function(validate){
                        defered.reject(validate);
                    });
                return defered.promise;
            }, 
            projectFollowing: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/following/', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            updateProject: function(obj) {
                var defered = $q.defer();
                ValidateService.validateProjectUpdate(obj)
                    .then(function () {
                        $http.put('/ui/project', obj, {
                            headers: {'token': obj.token}
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function (validate) {
                        defered.reject(validate);
                    });
                return defered.promise;
            },
            updateProjectFiles: function(obj) {
                var defered = $q.defer();                
                $http.put('/ui/project', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;                    
            },
            updateProjectDetails: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/project', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            getProjectList: function(token) {
                var defered = $q.defer();
                $http.get('/ui/query/project', {
                    headers: {'token': token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            },
            getFolderList: function(token) {
                var defered = $q.defer();
                $http.get('/ui/query/projectFolder', {
                    headers: {'token': token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            },
            addFolder: function(obj) {
                var defered = $q.defer();
                ValidateService.validateProjectFolder(obj)
                    .then(function () {
                        $http.post('/ui/projectFolder', obj, {
                            headers: {'token': obj.token}
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function (validate) {
                        defered.reject(validate);
                    });
                return defered.promise;
            },
            getFolderDetail: function(folder) {
                var defered = $q.defer();
                $http.get('/ui/projectFolder/'+ folder.folderId, {
                    headers: {'token': folder.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            }, 
            editFolder: function(obj) {
                var defered = $q.defer();
                ValidateService. validateEditProjectFolder(obj)
                    .then(function () {
                        $http.put('/ui/projectFolder', obj, {
                            headers: {'token': obj.token}
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function (validate) {
                        defered.reject(validate);
                    });
                return defered.promise;
            },
            addFolderUsers: function(obj) {
                var defered = $q.defer();
                ValidateService.validateFolderUpdate(obj)
                    .then(function () {
                        $http.put('/ui/projectFolder/addUsers',obj, {
                            headers: {'token': obj.token}
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(error);
                        });
                        return defered.promise;
                    }, function (validate) {
                        defered.reject(validate);
                    });
                return defered.promise;
            },
            getFolderProjects: function(projects) {
                var defered = $q.defer();
                $http.get('/ui/query/project/?projectFolderId='+projects.projectFolderId, {
                    headers: {'token': projects.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            },
            getFolderDetails: function(folder) {
                var defered = $q.defer();
                $http.get('/ui/projectFolder/'+folder.folderId, {
                    headers: {'token': folder.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            },
            uploadProjects: function (data) {
                var defered = $q.defer();
                ValidateService.validateProjectUploadProjects(data).then(function () {
                    var fd = new FormData();
                    fd.append("upload", data.file);
                    var details = JSON.stringify({
                        "description": data.description,
                        "projectId": data.projectId,
                        "fileName": data.fileName1,
                        "userId": data.userId,
                        "uploadBy": data.uploadBy,
                        "category": data.type,
                        "author": data.author || '',
                        "url": data.url || '',
                        "keyWords": data.keyWords || ''
                    });
                    fd.append("advanceDetails", details);
                    $http.post('/ui/project/uploads/'+data.type, fd, {
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined, 'token': data.token }
                    }).then(function(response) {
                        defered.resolve(response.data);
                    }, function(error) {
                        defered.reject(response.data);
                    });
                    return defered.promise;
                }, function(validate){
                    defered.reject(validate);
                });
                return defered.promise;
            },
            projectRating: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/userRating/', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },     
            setProjectId: function(id){
                projectId = id;
            },
            getProjectId: function(){
                return projectId;
            },
            setProjectFolderId: function(id){
                projectFolderId = id;
            },
            getProjectFolderId: function(){
                return projectFolderId;
            },
            setProjectFolderName: function(name){
                projectFolderName = name;
            },
            getProjectFolderName: function(){
                return projectFolderName;
            },
            setProjectName: function(name){
                projectName = name;
            },
            getProjectName: function(){
                return projectName;
            },
            setProjectUsersFilter: function(filter){
                projectUsersFilter = filter;
            },
            getProjectUsersFilter: function(){
                return projectUsersFilter;
            },
            setProjectFolderProfilePath: function(folderInfo){
                folderData = folderInfo;
            },
            getProjectFolderProfilePath: function(){
                return folderData;
            }  
        }
    }]);