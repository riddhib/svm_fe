angular.module('SVM')
    .directive('projectFolderUsers', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/project/projectFolderUsers/projectFolderUsers.html',
            controller: 'projectFolderUsersController'
        }
    })
    .controller('projectFolderUsersController', ['$scope', 'MainService', 'ProjectService','UserService','$timeout', function($scope, MainService, ProjectService, UserService, $timeout) {
    	$scope.token = localStorage.getItem("token");
        $scope.projectFolderId = ProjectService.getProjectFolderId();
        $scope.projectFolderName = ProjectService.getProjectFolderName();
        $scope.usersList = {};
        $scope.projectFolderProfile = {};
        $scope.userIds = [];
        $scope.successMessage = null;
        $scope.errorMessage = null;
        $scope.projectName = ProjectService.getProjectName();
        $scope.projectUsersFilter =  ProjectService.getProjectUsersFilter();
        MainService.setActiveView('projectFolderUsersView');

        function init() {
            UserService.getUsers($scope.token).then(function(response) {
                $scope.usersList = response.result;
                ProjectService.getFolderDetails({"folderId": $scope.projectFolderId, "token": $scope.token}).then(function(response){
                    let folderUsers = response.result[0].users;
                    $scope.usersList = $scope.usersList.map(user => {
                        user.isSelected = folderUsers.length ? (folderUsers.some(folderUser => folderUser.userId == user.userId) || false) : false;
                        return user;
                    });
                    $scope.selectedUsersList = $scope.usersList.filter(user=> user.isSelected == true);
                    if($scope.projectUsersFilter != 'All') {
                        $scope.selectedUsersList = $scope.selectedUsersList.filter(user=> user.type == $scope.projectUsersFilter);
                    }
                });
            }, function(error) {
                console.log("Error", error);
            });
        }

        $scope.addFolderUsers = function() {
            setLoading();
            $scope.successMessage = null;
            $scope.errorMessage = null;
            ProjectService.addFolderUsers({"userIds": $scope.userIds, "projectFolderId": $scope.projectFolderId, "token": $scope.token}).then(function(response) {
                if (response.status == "Success") {
                    $scope.successMessage = "Users saved uccessfully";
                    $scope.userIds = [];
                    clearLoading();
                    init();
                    setTimeout(function() {
                        $('#successMessage').fadeOut('fast');
                    }, 2500);
                } else if (response.error) {
                    $scope.errorMessage = response.error;
                    $scope.userIds = [];
                    clearLoading();
                    $scope.upload = {};
                    setTimeout(function() {
                      $('#errorMessage').fadeOut('fast');
                    }, 3000);
                }
            })
        }

        $scope.projectViews = function(view) {
          MainService.setActiveView(view); 
        }

        $scope.deselectUsers = function(){
            $scope.userIds = [];
            $('[name=deselectUser]:checked').each(function () {
                $scope.userIds.push($(this).val());
            });
        }
        $scope.viewProfile =function(id) {
            UserService.setUserId(id);
            MainService.setActiveView('userProfileView');
        }

        $scope.selectUsers = function(){
            $scope.userIds = [];
            $('[name=selectUser]:checked').each(function () {
                $scope.userIds.push($(this).val());
            });
        }
        
        var setLoading = function () {
            var search = $('#addUsers');
            if (!search.data('normal-text')) {
              search.data('normal-text', search.html());
            }
            search.html(search.data('loading-text'));
        };

        $scope.viewProfile =function(id) {
            $scope.projectFolderProfile.folderName = $scope.projectFolderName;
            $scope.projectFolderProfile.projectName = $scope.projectName;
            $scope.projectFolderProfile.projectPersona = $scope.projectUsersFilter;
            ProjectService.setProjectFolderProfilePath($scope.projectFolderProfile);
            UserService.setUserId(id);
            MainService.setActiveView('userProfileView');
        }
        
        var clearLoading = function () {
            var search = $('#addUsers');
            search.html(search.data('normal-text'));
        };   
        
        init();
    }])