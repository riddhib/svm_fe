angular.module('SVM')
    .directive('projectFolderUsers', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/project/projectFolderUsers/projectFolderUsers.html',
            controller: 'projectFolderUsersController'
        }
    })
    .controller('projectFolderUsersController', ['$scope', 'MainService', 'ProjectService','UserService','$timeout', function($scope, MainService, ProjectService, UserService, $timeout) {
    	$scope.projectFolderId = ProjectService.getProjectFolderId();
        $scope.projectFolderName = ProjectService.getProjectFolderName();
        $scope.usersList = {};
        var table = null;
        var table1 = null;
        MainService.setActiveView('projectFolderUsersView');

        function init() {
            $scope.successMessage = null;
            UserService.getUsers({}).then(function(response) {
                $scope.usersList = response.result;
                ProjectService.getFolderDetails($scope.projectFolderId).then(function(response){
                    let folderUsers = response.result[0].users;
                    $scope.usersList = $scope.usersList.map(user => {
                        user.isSelected = folderUsers.length ? (folderUsers.some(folderUser => folderUser.userId == user.userId) || false) : false;
                        return user;
                    });
                    $scope.selectedUsersList = $scope.usersList.filter(user=> user.isSelected == true);
                    setTimeout(function(){
                        table = $('#datatable1').DataTable({
                            responsive: true,
                            columnDefs: [
                                {
                                    orderable: false,
                                    className: 'select-checkbox',
                                    targets: 0
                                },
                                {
                                    targets: 1,
                                    visible: false,
                                    searchable: false
                                }
                            ],
                            select: {
                                style: 'multi',
                                selector: 'td:first-child'
                            },
                            language: {
                                searchPlaceholder: 'Search...',
                                sSearch: '',
                                lengthMenu: '_MENU_ items/page',
                            }
                        });
                        table1 = $('#datatable2').DataTable({
                            responsive: true,
                            columnDefs: [
                                {
                                    orderable: false,
                                    className: 'select-checkbox',
                                    targets: 0
                                },
                                {
                                    targets: 1,
                                    visible: false,
                                    searchable: false
                                }
                            ],
                            select: {
                                style: 'multi',
                                selector: 'td:first-child'
                            },
                            language: {
                                searchPlaceholder: 'Search...',
                                sSearch: '',
                                lengthMenu: '_MENU_ items/page',
                            }
                        });
                    }, 100);
                });
            }, function(error) {
                console.log("Error", error);
            });
        }
        $scope.viewProfile =function(id) {
            UserService.setUserId(id);
            MainService.setActiveView('userProfileView');
        } 
        
        $scope.addFolderUsers = function() {
            setLoading();
            $scope.successMessage = null;
            $scope.errorMessage = null;
            var userIds = $scope.usersList.filter(user => user.isSelected).map(user => user.userId);
            ProjectService.addFolderUsers({"userIds": userIds, "projectFolderId": $scope.projectFolderId}).then(function(response) {
                if (response.status == "Success") {
                    $scope.successMessage = "Users saved uccessfully";
                    clearLoading();
                    setTimeout(function() {
                        $('#successMessage').fadeOut('fast');
                    }, 2500);
                } else if (response.error) {
                    $scope.errorMessage = response.error;
                    clearLoading();
                    $scope.upload = {};
                    setTimeout(function() {
                      $('#errorMessage').fadeOut('fast');
                    }, 3000);
                }
            })
        }

        $scope.projectViews = function(view) {
          MainService.setActiveView(view); 
        }

        $scope.selectAllUsers = function() {
            if ($scope.select) {
                table.rows().select();
            } else {
                table.rows().deselect();
            }
        }

        $scope.getChecked = function(id, validate) {
            $timeout(function() {
                if (validate) {
                    angular.element(document.getElementById(id)).addClass("selected");
                } else {
                    angular.element(document.getElementById(id)).removeClass("selected");
                }
            }, 100);
        }
        
        var setLoading = function () {
            var search = $('#addUsers');
            if (!search.data('normal-text')) {
              search.data('normal-text', search.html());
            }
            search.html(search.data('loading-text'));
        };
        
        var clearLoading = function () {
            var search = $('#addUsers');
            search.html(search.data('normal-text'));
        };   
        
        init();
    }])