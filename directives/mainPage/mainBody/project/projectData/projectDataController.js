angular.module('SVM')
    .directive('projectData', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/project/projectData/projectData.html',
            controller: 'projectDataController'
        }
    })
    .controller('projectDataController', ['$scope', 'MainService', 'ProjectService', 'ResearchService', '$sce','DiscussionService','UserService', function($scope, MainService, ProjectService, ResearchService, $sce, DiscussionService,UserService) {
        $scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
        $scope.token = localStorage.getItem("token");
        $scope.projectFolderName = ProjectService.getProjectFolderName();
        $scope.singleProjectData = {};
        $scope.ratingDetails = {};
        $scope.projectInfo = {};
        $scope.response = {};
        $scope.followDetails = {};
        $scope.request = {};
        $scope.tabs = [
          {l: 'Resources', t: 'resources'},
          {l: 'Request For Information', t: 'requestInformation'},
          {l: 'Project Submissions', t: 'submission'},
          {l: 'On-Ground Data', t: 'onGroundData'},
          {l: 'Academic Research', t: 'academicResearch'},
          {l: 'Use Cases', t: 'useCases'},
          {l: 'Presentations', t: 'presentation'}
        ];
        $scope.files = {};
        var discussionData = {};
        $scope.path = {};
        $scope.projectId = ProjectService.getProjectId();
        $scope.changeLoaderStatus = MainService.setLoaderStatus;
        $scope.changeLoaderStatus(true);
        setTimeout(function(){
          $('.select2').select2({
              minimumResultsForSearch: Infinity
          });
          $('.select2-show-search').select2({
              minimumResultsForSearch: ''
          });
        }, 100);
        $scope.loadSelect = function(){
          setTimeout(function(){
            $('#sector').select2({
              dropdownParent: $('#modaldemo6'),
              width : '100%'
            });
          }, 100)
        }

        function init(){
          ProjectService.getProjectDetail({"projectId": $scope.projectId, "token": $scope.token}).then(function(response) { 
              if (response.status == 'Success') {
                $scope.singleProjectData = response.result.project;
                $scope.followData = response.result.project.following.filter(uid => uid.userId == $scope.userDetails.userId);
                $scope.people = $scope.singleProjectData.people.filter(user=> user.type == 'People');
                $scope.government = $scope.singleProjectData.people.filter(user=> user.type == 'Government');
                $scope.startup = $scope.singleProjectData.people.filter(user=> user.type == 'Startup');
                $scope.university = $scope.singleProjectData.people.filter(user=> user.type == 'University');
                $scope.corporation = $scope.singleProjectData.people.filter(user=> user.type == 'Corporation');
                if($scope.singleProjectData.phone == 0) {
                  $scope.singleProjectData.phone = '';
                }
                else {
                  var USNumber = $scope.singleProjectData.phone.toString().match(/(\d{3})(\d{3})(\d{4})/);
                  USNumber = "(" + USNumber[1] + ")-" + USNumber[2] + "-" + USNumber[3];
                  $scope.singleProjectData.phone = USNumber;
                }
                $scope.files = _.groupBy(response.result.files || [], 'category');
                $scope.number = 5;
                $scope.changeLoaderStatus(false);
              }
          });
        }

        init();

        $scope.editProject = function(type) {
            ProjectService.type = type;
            MainService.setActiveView('editProjectDataView');   
        }
        
        $scope.projectProfiles = function(){
            MainService.setActiveView('projectProfilesView');   
        }
        
        $scope.rateProject =  function(projectId, ratingNo) {
          $scope.ratingDetails.collectionName = 'project';
          $scope.ratingDetails.collectionId = projectId;
          $scope.ratingDetails.ratingById =  $scope.userDetails.userId;
          $scope.ratingDetails.rating = ratingNo;
          $scope.ratingDetails.ratingBy =  $scope.userDetails.userName;
          $scope.ratingDetails.token = $scope.token;
          ProjectService.projectRating($scope.ratingDetails).then(function(response) {  
            if(response.status == 'Success') {
              init();
            } else {
            }
            }, function(error) {

          });

        }
        $scope.saveRequest = function() {
          $scope.request.projectId = $scope.projectId;
          $scope.request.token = $scope.token;                  
          ProjectService.addRequest($scope.request).then(function(response) {
            if(response.status == 'Success') {
              $scope.changeLoaderStatus(true);
              $scope.validate = {};
              $scope.request = {};
              init();
              $scope.closeDialog();
            }
          }, function(validate) {
            $scope.validate = validate;
          });
        }
        $scope.requestInfo = function(requestId) {
          $scope.response.requestId = requestId;
        }
        $scope.deleteRequest = function(requestId) {
          $scope.changeLoaderStatus(true);
          $scope.response.requestId = requestId;
          $scope.response.projectId = $scope.projectId;
          $scope.response.token = $scope.token; 
          ProjectService.deleteRequest($scope.response).then(function(response) {
            if(response.status == 'Success') {
              $scope.validate = {};
              init();
              $scope.closeDialog();
            }
          }, function(validate) {
            $scope.validate = validate;
          });
        }
        $scope.saveResponse = function() { 
          $scope.changeLoaderStatus(true);
          $scope.response.projectId = $scope.projectId;
          $scope.response.token = $scope.token; 
          if ($scope.document) {
            $scope.response.file = $scope.document;
            $scope.response.name = $scope.name;
            $scope.response.uploadedBy = $scope.userDetails.userId;  
          }
          ProjectService.addResponse($scope.response).then(function(response) {
            if(response.status == 'Success') {
              init();
              $scope.closeDialog();
            }
          }, function(validate) {
            $scope.validate = validate;
          });

        }
        $scope.responsesData = function(reqId) {
          $scope.responseData = [];
          $scope.responseData = $scope.singleProjectData.requests.filter(resp => resp.requestId == reqId);
        }
        $scope.fileNameChanged = function (file) {
          var selectedFile = file.files[0];
          var reader = new FileReader();
          reader.onload = function (e) {
              $scope.$apply(function () {
                  $scope.src = e.target.result;
                  $scope.name = selectedFile.name;
              });
          };
          reader.readAsDataURL(selectedFile);
          $scope.$apply(function () {
              if (selectedFile != undefined) {
                  $scope.document = selectedFile;
              }
          });
        }
        $scope.clear = function(){
          $scope.name = '';
          $scope.document = null;
          $('#customFile2').val('');
          $scope.request = {};
          $scope.response = {};
          $scope.validate = {};
        }
        $scope.closeDialog = function(){
          $scope.clear();
          jQuery("#modaldemo6").modal("hide");
          jQuery("#modaldemo7").modal("hide");
          jQuery("#modaldemo8").modal("hide");
        }
        $scope.viewProfile = function(id) {
          $scope.user ={};
          $scope.profile = {};
          UserService.getEditUser({'userId':id, 'token': $scope.token}).then(function(response) {            
            if (response.status == 'Success') {
                $scope.user=response.result.user;
                $scope.profile = response.result.profile;
                if($scope.user.phone == 0) {
                    $scope.user.phone = '';
                }
                else {
                    var USNumber = $scope.user.phone.toString().match(/(\d{3})(\d{3})(\d{4})/);
                    USNumber = "(" + USNumber[1] + ")-" + USNumber[2] + "-" + USNumber[3];
                    $scope.user.phone = USNumber;
                }
                $scope.profiles = response.result.profile;
                $scope.files = response.result.files; 
                $scope.changeLoaderStatus(false);
            }
          }, function(error) {
          });
        }
        $scope.pageRefresh = function() {
          $scope.changeLoaderStatus(true);
          init();
        }
        $scope.follow = function(projectId, isFollowing){
          $scope.changeLoaderStatus(true);
          $scope.followDetails.collectionName = 'project';
          $scope.followDetails.collectionId = projectId;
          $scope.followDetails.userId =  $scope.userDetails.userId;
          $scope.followDetails.userName =  $scope.userDetails.userName ; 
          if(isFollowing == 1) {
            $scope.followDetails.follow = true;
          } else {
            $scope.followDetails.follow = false;
          }
          $scope.followDetails.token = $scope.token;
          ProjectService.projectFollowing($scope.followDetails).then(function(response) {  
            if(response.status == 'Success') {
              init();
              $scope.changeLoaderStatus(false);
            } else {
            }
          }, function(error) {
            $scope.changeLoaderStatus(false);
          });
        }
        $scope.getNumber = function(num) {
          return new Array(num);   
        }
        $scope.toTrustedHTML = function( html ){
          return $sce.trustAsHtml( html );
        }
        
        $scope.projectViews = function(view) {
          MainService.setActiveView(view); 
        }
        
        $scope.addUsers = function(projectName,data) {
          ProjectService.setProjectUsersFilter(data);
          ProjectService.setProjectName(projectName);
          MainService.setActiveView('projectFolderUsersView');
        }

        $scope.discussion = function (x) {
          discussionData.body  = x.description;
          discussionData.discussionName = x.name;
          $scope.path.pathName ='Projects';
          $scope.path.folderName =  $scope.projectFolderName;
          $scope.path.projectName = x.name;
          DiscussionService.setPath($scope.path);
          discussionData.discussionId  = x.discussions[0];
          MainService.setDiscussionThread(discussionData);
          MainService.setActiveView('discussionsView');
        }

        $scope.deleteUploadedRecord = function(project, key, file){
          if(key != 'academicResearch'){
            var updatedProjectFiles = project.resources.filter(resource=> resource != file.fileId);
            ProjectService.updateProjectFiles({"projectId": project.projectId, "resources": updatedProjectFiles, "token": $scope.token})
              .then(function(response){
                if(response.status == 'Success'){
                  init();
                } else if(response.error){
                  console.log(response.error)
                }
              }, function(error){

              });
          } else {
            var updatedProjectFiles = project.resources.filter(resource=> resource != file.fileId);
            ProjectService.updateProjectFiles({"projectId": project.projectId, "resources": updatedProjectFiles, "token": $scope.token})
              .then(function(response){
                if(!response.error){
                  ResearchService.updateAcademicResearchFiles({"fileId": file.fileId, "token": $scope.token}).then(function(response){
                    if(!response.error){
                      init();
                    }
                  })
                }
              }, function(error){

              });
          }
        }
    }])