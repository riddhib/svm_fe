angular.module('SVM')
    .directive('requestInformation', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/project/requestInformation/requestInformation.html',
            controller: 'RequestInformationController'
        }
    })
    .controller('RequestInformationController', ['$scope', 'MainService', 'ProjectService','$timeout', function($scope, MainService, ProjectService, $timeout) {
        setTimeout(function(){
            $('.select2').select2({
                minimumResultsForSearch: Infinity
            });
            $('.select2-show-search').select2({
                minimumResultsForSearch: ''
            });
        }, 100);  
        $scope.loadSelect = function(){
            setTimeout(function(){
              $('#sector').select2({
                dropdownParent: $('#modaldemo6'),
                width : '100%'
              });
            }, 100)
        }
    }])