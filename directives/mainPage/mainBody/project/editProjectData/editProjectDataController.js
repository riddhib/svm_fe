angular.module('SVM')
    .directive('editProjectData', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/project/editProjectData/editProjectData.html',
            controller: 'editProjectDataController'
        }
    })
    .controller('editProjectDataController', ['$scope', 'MainService', 'ProjectService', function($scope, MainService, ProjectService) {
        $scope.projectFolderName = ProjectService.getProjectFolderName();
        $scope.projectUpdateData = {};
        $scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
        $scope.token = localStorage.getItem("token");
        $scope.projectId = ProjectService.getProjectId();
        $scope.type = ProjectService.type;
        $scope.allFiles = [];
        $scope.successMessage = null;
        $scope.errorMessage = null;
        
        function init() {
            ProjectService.getProjectDetail({"projectId": $scope.projectId, "token": $scope.token}).then(function(response) { 
                if (response.status == 'Success') {
                    $scope.allFiles = response.result.files || []; 
                    $scope.files = (response.result.files || []).filter(file => file.category == $scope.type);
                    $scope.singleProjectData = response.result.project;
                    var markupStr1 = $scope.singleProjectData.details;
                    setTimeout(function(){
                        $('.select2-show-search').select2({
                            minimumResultsForSearch: ''
                        });
                        $('.select2').select2({
                            minimumResultsForSearch: Infinity
                        });
                        $('#phoneMask').mask('(999)-999-9999');
                        // Inline editor
                        var editor = new MediumEditor('.editable');

                        // Summernote editor
                        $('#details').summernote({
                          placeholder: 'General Details..',
                          height: 150,
                          tooltip: false
                        });
                    }, 100);
                    $('#details').summernote('code', markupStr1);
                }
            });
        }
        init();
        
        $scope.projectPrimaryDetails = function() {
            setLoading("primaryDetails");
            $scope.projectUpdateData.projectId = $scope.projectId;
            $scope.projectUpdateData.name = $scope.singleProjectData.name;
            $scope.projectUpdateData.sector = $scope.singleProjectData.sector;
            $scope.projectUpdateData.email = $scope.singleProjectData.email;
            $scope.projectUpdateData.contactPerson = $scope.singleProjectData.contactPerson;
            $scope.projectUpdateData.status = $scope.singleProjectData.status;
            $scope.projectUpdateData.description =$scope.singleProjectData.description;
            $scope.projectUpdateData.logo =$scope.singleProjectData.logo;
            $scope.projectUpdateData.token = $scope.token;
            if($scope.singleProjectData.phone && typeof $scope.singleProjectData.phone == 'string'){
                $scope.projectUpdateData.phone = $scope.singleProjectData.phone.replace(/[()-]/g,'');
            }
            ProjectService.updateProject($scope.projectUpdateData).then(function(response) {  
                if(response.status == 'Success') {
                    $scope.validate = {};
                    $scope.image1 = ''; 
                    ProjectService.getProjectDetail({"projectId": $scope.projectId, "token": $scope.token}).then(function(response) { 
                        if (response.status == 'Success') {
                            $scope.singleProjectData=response.result.project;
                            var markupStr1 = $scope.singleProjectData.details;
                            $('#details').summernote('code', markupStr1);
                        }
                    }, function(error) {

                    });
                    $scope.successMessage1 = "Project details updated successfully";
                    clearLoading("primaryDetails");
                    setTimeout(function() {
                        $('#successMessage1').fadeOut('fast');
                    }, 2500);               
                } else {
                    $scope.validate = {};
                    $scope.errorMessage1 = response.error;
                    clearLoading("primaryDetails");
                    setTimeout(function() {
                      $('#errorMessage1').fadeOut('fast');
                    }, 2500);
                }
            }, function(validate) {
                $scope.validate = validate;
                clearLoading("primaryDetails");
            });
        }

        $scope.projectDetails = function() {
            setLoading("projectDetails");
            $scope.projectUpdateData.projectId = $scope.projectId;
            $scope.projectUpdateData.details = $('#details').summernote('code');
            $scope.projectUpdateData.token = $scope.token;
            ProjectService.updateProjectDetails($scope.projectUpdateData).then(function(response) {  
                if(response.status == 'Success') {
                    $scope.validate = {};
                    ProjectService.getProjectDetail({"projectId": $scope.projectId, "token": $scope.token}).then(function(response) { 
                        if (response.status == 'Success') {
                            $scope.singleProjectData=response.result.project;
                            var markupStr1 = $scope.singleProjectData.details;
                            $('#details').summernote('code', markupStr1);
                        }
                    }, function(error) {

                    });
                    $scope.successMessage2 = "Project details updated successfully";
                    clearLoading("projectDetails");
                    setTimeout(function() {
                        $('#successMessage2').fadeOut('fast');
                    }, 2500);
                } else {
                    $scope.validate = {};
                    $scope.errorMessage2 = response.error;
                    clearLoading("projectDetails");
                    setTimeout(function() {
                      $('#errorMessage2').fadeOut('fast');
                    }, 2500);
                }
            }, function(validate) {
                $scope.validate = validate;
                clearLoading("projectDetails");
            });
        }
        
        $scope.fileNameChanged = function (file) {
            var selectedFile = file.files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
                $scope.$apply(function () {
                    $scope.src = e.target.result;
                    $scope.selectedResource = selectedFile.name;
                });
            };
            reader.readAsDataURL(selectedFile);
            $scope.$apply(function () {
                if (selectedFile != undefined) {
                    $scope.document = selectedFile;
                }
            });
        }
        
        $scope.imageUpload = function(event) {
            var files = event.target.files; //FileList object
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                $scope.image1 = file.name;
                var reader = new FileReader();
                reader.onload = $scope.imageIsLoaded; 
                reader.readAsDataURL(file);
            }
        }
   
        $scope.imageIsLoaded = function(e) {
            $scope.$apply(function() {
                $scope.singleProjectData.logo = e.target.result;
            });
        }

        $scope.upload = {};
        $scope.save = function(type) {
            setLoading(type);
            if ($scope.upload) {
                $scope.upload.file = $scope.document;
                $scope.upload.userId =$scope.userDetails.userId;
                $scope.upload.projectId = $scope.projectId;
                $scope.upload.uploadBy = $scope.userDetails.userName;
                $scope.upload.token = $scope.token;
            }
            if($scope.upload.url) {
                let url = $scope.upload.url.replace(/^\/\/|^.*?:(\/\/)?/, '');
                $scope.upload.url = url;
            }
            $scope.upload.type = $scope.type;
            ProjectService.uploadProjects($scope.upload).then(function (response) {
                if (response.status == 'Success') {
                    $scope.validate = {};
                    $scope.resource ='';
                    ProjectService.getProjectDetail({"projectId": $scope.projectId, "token": $scope.token}).then(function(response) { 
                        if (response.status == 'Success') {
                            $scope.files = (response.result.files || []).filter(file => file.category == $scope.type);
                            $scope.singleProjectData = response.result.project;
                            var markupStr1 = $scope.singleProjectData.details;
                            $('#details').summernote('code', markupStr1);
                        }
                    });
                    $scope.successMessage3 = "File added successfully";
                    clearLoading(type);
                    $scope.upload = {};
                    $scope.document = '';
                    $scope.selectedResource = '';
                    setTimeout(function() {
                        $('#successMessage3').fadeOut('fast');
                    }, 2500);
                } else {
                    $scope.validate = {};
                    $scope.errorMessage3 = response.error;
                    clearLoading(type);
                    setTimeout(function() {
                      $('#errorMessage3').fadeOut('fast');
                    }, 2500);
                }
            }, function (validate) {
                $scope.validate = validate;
                clearLoading(type);
            });
        }

        $scope.projectViews = function(view) {
            MainService.setActiveView(view); 
        }

        $scope.changeSectionView = function(type) {
            $scope.files = ($scope.allFiles || []).filter(file => file.category == type);
            $scope.type = type;
        }

        function setLoading(id) {
            let search = $('#'+id);
            if (!search.data('normal-text')) {
              search.data('normal-text', search.html());
            }
            search.html(search.data('loading-text'));
        };

        function clearLoading(id) {
            let search = $('#'+id);
            search.html(search.data('normal-text'));
        };

    }])