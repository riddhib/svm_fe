angular.module('SVM')
    .directive('projectList', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/project/projectList/projectList.html',
            controller: 'projectListController'
        }
    })
    .controller('projectListController', ['$scope', 'MainService', 'ProjectService', 'UserService', '$timeout', function($scope, MainService, ProjectService, UserService, $timeout) {
         $scope.token = localStorage.getItem("token");
         setTimeout(function(){
          $('.select2').select2({
            minimumResultsForSearch: Infinity
          });
          $('.select2-show-search').select2({
            minimumResultsForSearch: ''
          });
        }, 100);
        $scope.projectFolderId = ProjectService.getProjectFolderId();
        $scope.projectFolderName = ProjectService.getProjectFolderName();
        $scope.projectData = {};
        $scope.ratingDetails = {};
        $scope.usersList = {};
        $scope.requiredProjectData = [];
        $scope.projectCategories =[];
        $scope.followDetails = {};
        $scope.changeLoaderStatus = MainService.setLoaderStatus;
        $scope.changeLoaderStatus(true);
        function init() {
            ProjectService.getFolderProjects({"projectFolderId": $scope.projectFolderId, "token": $scope.token}).then(function(response) {
                response.result.forEach(function(project){
                    $scope.following = project.following.filter(follow=> follow.userId == $scope.userDetails.userId);
                    if($scope.following.length > 0) {
                        project.isFollowing = 1;
                    } else {
                        project.isFollowing = 0;
                    }
                    $scope.requiredProjectData.push(project);
                  });
                $scope.projectList = $scope.requiredProjectData;
                $scope.projectCategories = response;
                $scope.requiredProjectData = [];
                $scope.number = 5;
                $scope.changeLoaderStatus(false);
            }, function(error) {
              $scope.changeLoaderStatus(true);
            })
        }

        $scope.saveProject = function() {
          $scope.changeLoaderStatus(true);
          if($scope.image1) {
            $scope.projectData.logoName = $scope.image1;
          }
          if($scope.image2) {
            $scope.projectData.logoName = $scope.image2;
          }
          $scope.projectData.startDate = new Date($scope.projectData.startDate).getTime();
          $scope.projectData.endDate = new Date($scope.projectData.endDate).getTime();
          $scope.projectData.projectFolderId = $scope.projectFolderId;
          $scope.projectData.token = $scope.token
          ProjectService.addProject($scope.projectData).then(function(response) {
            $scope.projectData = {};
            $scope.validate = {};
            jQuery("#modaldemo6").modal("hide");
            init();
            $scope.changeLoaderStatus(false);
          }, function(validate) {
            $scope.validate = validate;
            $scope.changeLoaderStatus(false);
          })
        }
        $scope.editProject = function(id){
            ProjectService.setProjectId(id);
            MainService.setActiveView('projectDataView');
        }
        $scope.follow = function(projectId, isFollowing){
          $scope.changeLoaderStatus(true);
          $scope.followDetails.collectionName = 'project';
          $scope.followDetails.collectionId = projectId;
          $scope.followDetails.userId =  $scope.userDetails.userId;
          $scope.followDetails.userName =  $scope.userDetails.userName ; 
          if(isFollowing) {
            $scope.followDetails.follow = false;
          } else {
            $scope.followDetails.follow = true;
          }
          $scope.followDetails.token = $scope.token;
          ProjectService.projectFollowing($scope.followDetails).then(function(response) {  
            if(response.status == 'Success') {
              $scope.projectList.forEach(function(project){
                if(project.projectId == $scope.followDetails.collectionId ){
                  project.isFollowing =  $scope.followDetails.follow ;
                }
              });
              $scope.changeLoaderStatus(false);
            } else {
            }
          }, function(error) {
            $scope.changeLoaderStatus(false);
          });
        }

        $scope.addUsers = function() {
          MainService.setActiveView('projectFolderUsersView');
        }
        $scope.clear = function(){
          $scope.projectData = {};
          $scope.peopleData = {};
          $scope.validate = {};
        }  
        $scope.imageUpload = function(event){
          var files = event.target.files; //FileList object
          for (var i = 0; i < files.length; i++) {
              var file = files[i];
              $scope.image1 = file.name;
              var reader = new FileReader();
              reader.onload = $scope.imageIsLoaded; 
              reader.readAsDataURL(file);
          }
        }
        $scope.imageIsLoaded = function(e){        
          $scope.$apply(function() {
              $scope.projectData.logo = e.target.result;
          });
        }
        $scope.closeAddProject = function(){
          $scope.projectData = {};
          $scope.validate = {};
          jQuery("#modaldemo6").modal("hide");
        } 
        $scope.closeAddUsers = function(){
          $scope.peopleData = {};
          $scope.validate = {};
          jQuery("#modaldemo7").modal("hide");
        } 
        $scope.loadSelect = function(){
          setTimeout(function(){
            $('#type').select2({
              dropdownParent: $('#modaldemo6'),
              width : '100%'
            });
            $('#sector').select2({
              dropdownParent: $('#modaldemo6'),
              width : '100%'
            });
            $('#projectType').select2({
              dropdownParent: $('#modaldemo6'),
              width : '100%'
            });
            $('#status').select2({
              dropdownParent: $('#modaldemo6'),
              width : '100%'
            });
          }, 100)
        } 
        function formatDropdown(dropdownValue) {
          if (!dropdownValue.id) {
            return dropdownValue.text;
          }
          var $dropdownValue = $(
            '<span><img src="'+dropdownValue.logo+'" class="img-flag" /> ' + dropdownValue.text + '</span>'
          );
          return $dropdownValue;
        };
        $scope.loadUsers = function(){
          $scope.successMessage = null;
          UserService.getUsers({}).then(function(response){
            $scope.usersList = response.result;
          });
          setTimeout(function(){
            $('#users').select2({
              tags: true,
              allowClear: true,
              dropdownParent: $('#modaldemo7'),
              width : '100%'
            });
          }, 100);
        }
        $scope.searchData = function () {
          $scope.searchString = '';
          $timeout(function () {
            if ($scope.category.length == 0 || $scope.category == "All") {
              $scope.projectList = $scope.projectCategories.result;
            } 
            else {
              var matchedProject = [];
                $scope.projectCategories.result.forEach(function (project) {
                  if (project.categories.includes( $scope.category) && !matchedProject.find(x => x.projectId === project.projectId)) {
                    matchedProject.push(project);
                  }
                });
              $scope.projectList = matchedProject;
              matchedProject = [];
            }
          }, 100);
      }
        $scope.rateProject =  function(projectId, ratingNo) {
          $scope.changeLoaderStatus(true);
          $scope.ratingDetails.collectionName = 'project';
          $scope.ratingDetails.collectionId = projectId;
          $scope.ratingDetails.ratingById =  $scope.userDetails.userId;
          $scope.ratingDetails.rating = ratingNo;
          $scope.ratingDetails.ratingBy =  $scope.userDetails.userName ;
          $scope.ratingDetails.token = $scope.token;
          ProjectService.projectRating($scope.ratingDetails).then(function(response) {  
            if(response.status == 'Success') {
              $scope.projectList.forEach(function(project){
                if(project.projectId == $scope.ratingDetails.collectionId ){
                    project.avgRating =  response.result.avgRating ;
                    project.totalRating =  response.result.totalRating ;
                }
              });
              $scope.changeLoaderStatus(false);
            } else {
            }
          }, function(error) {
            $scope.changeLoaderStatus(false);
          });

        }
        $scope.projectViews = function(view) {
          MainService.setActiveView(view); 
        }
        $scope.getNumber = function(num) {
          return new Array(num);   
        }
        init();
    }])