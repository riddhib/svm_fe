angular.module('SVM')
    .factory('SettingsService', ['$http', '$q', 'ValidateService', function ($http, $q, ValidateService) {
        return { 
        	updateUser: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/user/', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            updateSolution: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/solution', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            updateProjectDetails: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/project', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            updateCollaborate: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/collaborate', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            updateChallenge: function(obj) {
                var defered = $q.defer();                
                $http.put('/ui/challenge/', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;                   
            },
            updateProjectFolderDetails: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/projectFolder', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            updateDiscussions: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/discussion/discussionStatus', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            updateResearch: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/research', obj, {
                    headers: {'token': obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
        }
    }]);