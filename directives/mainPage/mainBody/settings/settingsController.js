angular.module('SVM')
    .directive('settings', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/settings/settings.html',
            controller: 'settingsController'
        }
    })
    .controller('settingsController', ['$scope','MainService', 'SettingsService', 
        'ChallengeService', 'ProjectService', 'SolutionService', 'ResearchService', 'DiscussionService', 'CollaborateService', 'UserService', 
        function($scope, MainService, SettingsService, ChallengeService, ProjectService, SolutionService, ResearchService, DiscussionService, CollaborateService, UserService) {
        $scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
        $scope.token = localStorage.getItem("token");
        $scope.changeLoaderStatus = MainService.setLoaderStatus;
        $scope.changeLoaderStatus(true);
        $scope.view = 'users';
        $scope.selectedUsers = [];
        $scope.deselectedUsers = [];
        $scope.selectedChallenges = [];
        $scope.deselectedChallenges = [];
        $scope.selectedCollaborates = [];
        $scope.deselectedCollaborates = [];
        $scope.selectedProjects = [];
        $scope.deselectedProjects = [];
        $scope.selectedSolutions = [];
        $scope.deselectedSolutions = [];
        $scope.discussionData = {};

        function init() {
            UserService.getUsers($scope.token).then(function(response) {
                $scope.adminsList = response.result.filter(user=> user.isAdmin == true);
                $scope.usersList = response.result.filter(user=> user.isAdmin != true);
                $scope.number = 5;
                $scope.changeLoaderStatus(false);
            }, function(error) {
                console.log("Error", error);
                $scope.changeLoaderStatus(false);
            });
        }
        init();

        function getChallenges() {
            $scope.changeLoaderStatus(true);
            ChallengeService.getchallenge($scope.token).then(function(response){
                $scope.challengesList = response.result;
                $scope.number = 5;
                $scope.changeLoaderStatus(false);
            },function(error){
                console.log("Error", error);
                $scope.changeLoaderStatus(false);
            });
        }

        function getSolutions() {
            $scope.changeLoaderStatus(true);
            SolutionService.getSolutionList($scope.token).then(function(response){
                $scope.solutionsList = response.result;
                $scope.number = 5;
                $scope.changeLoaderStatus(false);
            },function(error){
                console.log("Error", error);
                $scope.changeLoaderStatus(false);
            });
        }

        function getResearches() {
            $scope.changeLoaderStatus(true);
            ResearchService.getReaseachList($scope.token).then(function(response){
                $scope.researchList = response.result;
                $scope.changeLoaderStatus(false);
            },function(error){
                console.log("Error", error);
                $scope.changeLoaderStatus(false);
            });
        }

        function getDiscussions() {
            $scope.changeLoaderStatus(true);
            DiscussionService.getDiscussionList($scope.token).then(function(response){
                $scope.discussionsList = response.result;
                $scope.changeLoaderStatus(false);
            },function(error){
                console.log("Error", error);
                $scope.changeLoaderStatus(false);
            });
        }

        function getCollaborators() {
            $scope.changeLoaderStatus(true);
            CollaborateService.getCollaborateList($scope.token).then(function(response){
                $scope.collaboratorsList = response.result;
                $scope.number = 5;
                $scope.changeLoaderStatus(false);
            },function(error){
                console.log("Error", error);
                $scope.changeLoaderStatus(false);
            });
        }

        function getProjectFolders() {
            $scope.changeLoaderStatus(true);
            ProjectService.getFolderList($scope.token).then(function(response) {
                $scope.projectFoldersList = response.result;
                $scope.changeLoaderStatus(false);
            }, function(error) {
                console.log("Error", error);
                $scope.changeLoaderStatus(false);
            });
        }

        function getProjects() {
            $scope.changeLoaderStatus(true);
            ProjectService.getProjectList($scope.token).then(function(response){
                $scope.projectsList = response.result;
                $scope.number = 5;
                $scope.changeLoaderStatus(false);
            },function(error){
                console.log("Error", error);
                $scope.changeLoaderStatus(false);
            });
        }

        $scope.makeAdmin = function(userId){
            $scope.changeLoaderStatus(true);
            SettingsService.updateUser({"userId": userId, "isAdmin": true, "token": $scope.token}).then(function(response) {  
                if(response.status == 'Success') {
                    init();
                    $scope.changeLoaderStatus(false);
                } else if(response.error){
                    $scope.changeLoaderStatus(false);
                }
            }, function(error) {
               $scope.changeLoaderStatus(false);
            });
        }

        $scope.removeAdmin = function(userId){
            $scope.changeLoaderStatus(true);
            SettingsService.updateUser({"userId": userId, "isAdmin": false, "token": $scope.token}).then(function(response) {  
                if(response.status == 'Success') {
                    init();
                    $scope.changeLoaderStatus(false);
                } else if(response.error){
                    $scope.changeLoaderStatus(false);
                }
            }, function(error) {
               $scope.changeLoaderStatus(false);
            });
        }

        function deleteDiscussions(discussionIds) {
            $scope.changeLoaderStatus(true);
            SettingsService.updateDiscussions({"discussionIds": discussionIds, "isActive": false, "token": $scope.token}).then(function(response) {  
                if(response.status == 'Success') {
                    getDiscussions();
                    $scope.changeLoaderStatus(false);
                } else if(response.error){
                    $scope.changeLoaderStatus(false);
                }
            }, function(error) {
               $scope.changeLoaderStatus(false);
            });
        }

        function addDiscussions(discussionIds) {
            $scope.changeLoaderStatus(true);
            SettingsService.updateDiscussions({"discussionIds": discussionIds, "isActive": true, "token": $scope.token}).then(function(response) {  
                if(response.status == 'Success') {
                    getDiscussions();
                    $scope.changeLoaderStatus(false);
                } else if(response.error){
                    $scope.changeLoaderStatus(false);
                }
            }, function(error) {
               $scope.changeLoaderStatus(false);
            });
        }

        $scope.addChallenge = function(challenge){
            $scope.changeLoaderStatus(true);
            SettingsService.updateChallenge({"challengeId": challenge.challengeId, "isActive": true, "token": $scope.token}).then(function(response) {  
                if(response.status == 'Success') {
                    getChallenges();
                    addDiscussions(challenge.discussions);
                    $scope.changeLoaderStatus(false);
                } else if(response.error){
                    $scope.changeLoaderStatus(false);
                }
            }, function(error) {
               $scope.changeLoaderStatus(false);
            });
        }

        $scope.addSolution = function(solution){
            $scope.changeLoaderStatus(true);
            SettingsService.updateSolution({"solutionId": solution.solutionId, "isActive": true, "token": $scope.token}).then(function(response) {  
                if(response.status == 'Success') {
                    getSolutions();
                    addDiscussions(solution.discussions);
                    $scope.changeLoaderStatus(false);
                } else if(response.error){
                    $scope.changeLoaderStatus(false);
                }
            }, function(error) {
               $scope.changeLoaderStatus(false);
            });
        }

        $scope.addResearch = function(research){
            $scope.changeLoaderStatus(true);
            SettingsService.updateResearch({"researchId": research.researchId, "isActive": true, "token": $scope.token}).then(function(response) {  
                if(response.status == 'Success') {
                    getResearches();
                    deleteDiscussions(research.discussions);
                    $scope.changeLoaderStatus(false);
                } else if(response.error){
                    $scope.changeLoaderStatus(false);
                }
            }, function(error) {
               $scope.changeLoaderStatus(false);
            });
        }

        $scope.addDiscussion = function(discussion){
            $scope.changeLoaderStatus(true);
            addDiscussions([discussion.discussionId]);
        }

        $scope.addCollaborator = function(collaborateId){
            $scope.changeLoaderStatus(true);
            SettingsService.updateCollaborate({"collaborateId": collaborateId, "isActive": true, "token": $scope.token}).then(function(response) {  
                if(response.status == 'Success') {
                    getCollaborators();
                    $scope.changeLoaderStatus(false);
                } else if(response.error){
                    $scope.changeLoaderStatus(false);
                }
            }, function(error) {
               $scope.changeLoaderStatus(false);
            });
        }

        $scope.addUser = function(userId){
            $scope.changeLoaderStatus(true);
            SettingsService.updateUser({"userId": userId, "status": "Active", "token": $scope.token}).then(function(response) {  
                if(response.status == 'Success') {
                    init();
                    $scope.changeLoaderStatus(false);
                } else if(response.error){
                    $scope.changeLoaderStatus(false);
                }
            }, function(error) {
               $scope.changeLoaderStatus(false);
            });
        }

        $scope.addProjectFolders = function(projectFolderId){
            $scope.changeLoaderStatus(true);
            SettingsService.updateProjectFolderDetails({"projectFolderId": projectFolderId, "isActive": true, "token": $scope.token}).then(function(response) {  
                if(response.status == 'Success') {
                    getProjectFolders();
                    $scope.changeLoaderStatus(false);
                } else if(response.error){
                    $scope.changeLoaderStatus(false);
                }
            }, function(error) {
               $scope.changeLoaderStatus(false);
            });
        }

        $scope.addProject = function(project){
            $scope.changeLoaderStatus(true);
            SettingsService.updateProjectDetails({"projectId": project.projectId, "isActive": true, "token": $scope.token}).then(function(response) {  
                if(response.status == 'Success') {
                    getProjects();
                    addDiscussions(project.discussions);
                    $scope.changeLoaderStatus(false);
                } else if(response.error){
                    $scope.changeLoaderStatus(false);
                }
            }, function(error) {
               $scope.changeLoaderStatus(false);
            });
        }

        $scope.deleteChallenge = function(challenge){
            $scope.changeLoaderStatus(true);
            SettingsService.updateChallenge({"challengeId": challenge.challengeId, "isActive": false, "token": $scope.token}).then(function(response) {  
                if(response.status == 'Success') {
                    getChallenges();
                    deleteDiscussions(challenge.discussions);
                    $scope.changeLoaderStatus(false);
                } else if(response.error){
                    $scope.changeLoaderStatus(false);
                }
            }, function(error) {
               $scope.changeLoaderStatus(false);
            });
        }

        $scope.deleteResearch = function(research){
            $scope.changeLoaderStatus(true);
            SettingsService.updateResearch({"researchId": research.researchId, "isActive": false, "token": $scope.token}).then(function(response) {  
                if(response.status == 'Success') {
                    getResearches();
                    deleteDiscussions(research.discussions);
                    $scope.changeLoaderStatus(false);
                } else if(response.error){
                    $scope.changeLoaderStatus(false);
                }
            }, function(error) {
               $scope.changeLoaderStatus(false);
            });
        }

        $scope.deleteDiscussion = function(discussion){
            deleteDiscussions([discussion.discussionId]);
        }

        $scope.deleteSolution = function(solution){
            $scope.changeLoaderStatus(true);
            SettingsService.updateSolution({"solutionId": solution.solutionId, "isActive": false, "token": $scope.token}).then(function(response) {  
                if(response.status == 'Success') {
                    getSolutions();
                    deleteDiscussions(solution.discussions);
                    $scope.changeLoaderStatus(false);
                } else if(response.error){
                    $scope.changeLoaderStatus(false);
                }
            }, function(error) {
               $scope.changeLoaderStatus(false);
            });
        }

        $scope.deleteCollaborator = function(collaborateId){
            $scope.changeLoaderStatus(true);
            SettingsService.updateCollaborate({"collaborateId": collaborateId, "isActive": false, "token": $scope.token}).then(function(response) {  
                if(response.status == 'Success') {
                    getCollaborators();
                    $scope.changeLoaderStatus(false);
                } else if(response.error){
                    $scope.changeLoaderStatus(false);
                }
            }, function(error) {
               $scope.changeLoaderStatus(false);
            });
        }

        $scope.deleteUser = function(userId){
            $scope.changeLoaderStatus(true);
            SettingsService.updateUser({"userId": userId, "status": "Inactive", "token": $scope.token}).then(function(response) {  
                if(response.status == 'Success') {
                    init();
                    $scope.changeLoaderStatus(false);
                } else if(response.error){
                    $scope.changeLoaderStatus(false);
                }
            }, function(error) {
               $scope.changeLoaderStatus(false);
            });
        }

        $scope.deleteProjectFolders = function(projectFolderId){
            $scope.changeLoaderStatus(true);
            SettingsService.updateProjectFolderDetails({"projectFolderId": projectFolderId, "isActive": false, "token": $scope.token}).then(function(response) {  
                if(response.status == 'Success') {
                    getProjectFolders();
                    $scope.changeLoaderStatus(false);
                } else if(response.error){
                    $scope.changeLoaderStatus(false);
                }
            }, function(error) {
               $scope.changeLoaderStatus(false);
            });
        }

        $scope.deleteProject = function(project){
            $scope.changeLoaderStatus(true);
            SettingsService.updateProjectDetails({"projectId": project.projectId, "isActive": false, "token": $scope.token}).then(function(response) {  
                if(response.status == 'Success') {
                    getProjects();
                    deleteDiscussions(project.discussions);
                    $scope.changeLoaderStatus(false);
                } else if(response.error){
                    $scope.changeLoaderStatus(false);
                }
            }, function(error) {
               $scope.changeLoaderStatus(false);
            });
        }

        $scope.selectUser = function(userId){

        }
        $scope.deselectUser = function(userId){
            
        }

        $scope.selectChallenge = function(challengeId){

        }
        $scope.deselectChallenge = function(challengeId){
            
        }

        $scope.selectCollaborate = function(collaboratorId){

        }
        $scope.deselectCollaborate = function(collaboratorId){
            
        }

        $scope.selectSolution = function(solutionId){

        }
        $scope.deselectSolution = function(solutionId){
            
        }

        $scope.selectProject = function(projectId){

        }
        $scope.deselectProject = function(projectId){
            
        }

        $scope.viewChallenge = function (id) {
            ChallengeService.setChallengeId(id);
            ChallengeService.setPath('Settings');
            MainService.setActiveView('editChallengeView');
        }

        $scope.viewCollaborate = function(id){
            CollaborateService.setCollaborateId(id);
            CollaborateService.setPath('Settings');
            MainService.setActiveView('editCollaborateView');
        }

        $scope.viewProject = function(id){
            ProjectService.setProjectId(id);
            ProjectService.setProjectFolderName('Settings');
            MainService.setActiveView('projectDataView');
        }

        $scope.viewSolution =function(id){
          SolutionService.setSolutionId(id);
          SolutionService.setPath('Settings');
          MainService.setActiveView('editSolutionView')
        }

        $scope.viewDiscussion = function (x) {
          $scope.discussionData.body  = x.description;
          $scope.discussionData.discussionName = x.title;
          $scope.discussionData.discussionId  = x.discussionId;
          DiscussionService.setPath('Settings');
          MainService.setDiscussionThread($scope.discussionData);
          MainService.setActiveView('discussionsView');
        }

        $scope.viewProfile =function(id) {
            UserService.setUserId(id);
            UserService.setPath('Settings');
            MainService.setActiveView('userProfileView')
        }

        $scope.settingsView = function(view){
            $scope.changeLoaderStatus(true);
            $scope.view = view;
            if($scope.view == 'users'){
                init();
            } else if($scope.view == 'challenges'){
                getChallenges();
            } else if($scope.view == 'solutions'){
                getSolutions();
            } else if($scope.view == 'collaborators'){
                getCollaborators();
            } else if($scope.view == 'research'){
                getResearches();
            } else if($scope.view == 'discussions'){
                getDiscussions();
            } else if($scope.view == 'projectFolders'){
                getProjectFolders();
            } else if($scope.view == 'projects'){
                getProjects();
            } 
        }

        $scope.getNumber = function(num) {
          return new Array(num);   
        }

    }]);