angular.module('SVM')
    .directive('mainBody', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/mainBody.html',
            controller: 'mainBodyController'
        }
    })
    .controller('mainBodyController', ['$scope','MainService','LoginService', function($scope,MainService,LoginService) {
        if (!JSON.parse(localStorage.getItem("currentView"))) {
            MainService.setActiveView('challengeView');
        }
    }])
