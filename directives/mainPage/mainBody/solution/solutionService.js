angular.module('SVM')
    .factory('SolutionService', ['$http', '$q', 'ValidateService', function ($http, $q, ValidateService) {
        var path = null;
        return {
            uploadResource: function (data) {
                var defered = $q.defer();
                ValidateService.validateUploadResource(data)
                    .then(function () {
                        var fd = new FormData();
                        fd.append("upload", data.file);
                        var details = JSON.stringify({"description": data.description,"solutionId": data.solutionId,"fileName": data.fileName1,"userId": data.userId, "uploadBy": data.uploadBy});
                        fd.append("advanceDetails", details);
                        $http.post('/ui/solution/uploadResource', fd, {
                            transformRequest: angular.identity,
                            headers: { 'Content-Type': undefined, token: data.token }
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function(validate){
                        defered.reject(validate);
                });
            return defered.promise;
            },
            getSolutionDetail: function(solution) {
                var defered = $q.defer();
                $http.get('/ui/solution/'+ solution.solutionId, {
                    headers: {token: solution.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            }, 
            getArticleData: function(solution) {
                var defered = $q.defer();
                $http.get('/ui/getArticle/'+ solution.solutionId, {
                    headers: {token: solution.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            },
            addSolution: function(obj) {
                var defered = $q.defer();
                ValidateService.validateSolution(obj)
                    .then(function () {
                        $http.post('/ui/solution', obj, {
                            headers: {token: obj.token}
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function(validate){
                        defered.reject(validate);
                    });
                return defered.promise;
            }, 
            updateSolution: function(obj) {
                var defered = $q.defer();
                ValidateService.validateEditSolution(obj)
                    .then(function () {
                        $http.put('/ui/solution', obj, {
                            headers: {token: obj.token}
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function(validate){
                        defered.reject(validate);
                    });
                return defered.promise;
            },
            deleteResource: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/solution', obj, {
                    headers: {token: obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            solutionUpdateDescription: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/solution', obj, {
                    headers: {token: obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;

            },
            solutionUpdateWebArticles: function(obj) {
                var defered = $q.defer();
                ValidateService.validateWebArticles(obj.addArticle)
                    .then(function () {
                        $http.put('/ui/solution', obj, {
                            headers: {token: obj.token}
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise; 
                    }, function(validate){
                        defered.reject(validate);
                    });
                return defered.promise;
            },
            solutionUpdateImpact: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/solution', obj, {
                    headers: {token: obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            getSolutionList: function(token) {
                var defered = $q.defer();
                $http.get('/ui/query/solution', {
                    headers: {token: token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            },
            setSolutionId: function(id){
                solutionId = id;
            },
            getSolutionId: function(){
                return solutionId;
            },      
            solutionFollowing: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/following/', obj, {
                    headers: {token: obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            }, 
            solutionRating: function(obj) {
                var defered = $q.defer();
                $http.put('/ui/userRating/', obj, {
                    headers: {token: obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            },
            setPath: function(x){
                path = x;
            },
            getPath: function(){
                return path;
            }
        }
    }]);