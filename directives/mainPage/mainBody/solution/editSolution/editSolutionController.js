angular.module('SVM')
    .directive('editSolution', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/solution/editSolution/editSolution.html',
            controller: 'editSolutionController'
        }
    })
    .controller('editSolutionController', ['$scope', 'MainService','SolutionService', 'ChallengeService', '$sce','DiscussionService', function($scope,MainService,SolutionService, ChallengeService, $sce, DiscussionService) {
        $scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
        $scope.token = localStorage.getItem("token");
        $scope.descriptionOfSolutionValue = true;
        $scope.impactValue = false;
        $scope.addAnArticleValue = false;
        $scope.webArticleValue = false;
        $scope.ratingDetails = {};
        var discussionData = {};
        $scope.discussionPath = {};
        $scope.followDetails ={};
        $scope.webArticle = [];
        $scope.solutionId = SolutionService.getSolutionId();
        $scope.changeLoaderStatus = MainService.setLoaderStatus;
        $scope.changeLoaderStatus(true);
        $scope.path = SolutionService.getPath();
        function init() {
          SolutionService.getSolutionDetail({'solutionId': $scope.solutionId, 'token': $scope.token}).then(function(response) { 
            if (response.status == 'Success') {
                $scope.singleSolutionData=response.result[0];
                $scope.webArticle = response.result[0].addArticle;
                $scope.followData = response.result[0].following.filter(uid => uid.userId == $scope.userDetails.userId);
                $scope.number = 5;
                $scope.changeLoaderStatus(false);
            }
          }, function(error) {

          });
        }
        init();
        $scope.description = function(){
            $scope.descriptionOfSolutionValue = true;
            $scope.impactValue = false;
            $scope.addAnArticleValue = false;
            $scope.webArticleValue = false;
        }
        $scope.impact = function(){
            $scope.descriptionOfSolutionValue = false;
            $scope.impactValue = true;
            $scope.addAnArticleValue = false;
            $scope.webArticleValue = false;
        }
        
        $scope.article = function(){
            $scope.descriptionOfSolutionValue = false;
            $scope.impactValue = false;
            $scope.webArticleValue = false;
            $scope.addAnArticleValue = true;
            $scope.getDocument($scope.solutionId);
        }
        $scope.webArticles = function(){
          $scope.descriptionOfSolutionValue = false;
          $scope.impactValue = false;
          $scope.addAnArticleValue = false;
          $scope.webArticleValue = true;
          
        }
        $scope.getDocument = function(solutionId){
          $scope.changeLoaderStatus(true);
            SolutionService.getArticleData({'solutionId': solutionId, 'token': $scope.token}).then(function(response) { 
              if (response.status == 'Success') {
                  $scope.getDocumentData=response.result; 
                  $scope.changeLoaderStatus(false);  
              }
          }, function(error) {
  
          });
        }
        $scope.toTrustedHTML = function( html ){
          return $sce.trustAsHtml( html );
        }
        $scope.rateSolution =  function(solutionId, ratingNo) {
          $scope.ratingDetails.collectionName = 'solution';
          $scope.ratingDetails.collectionId = solutionId;
          $scope.ratingDetails.ratingById =  $scope.userDetails.userId;
          $scope.ratingDetails.rating = ratingNo;
          $scope.ratingDetails.ratingBy =  $scope.userDetails.userName;
          $scope.ratingDetails.token = $scope.token;
          SolutionService.solutionRating($scope.ratingDetails).then(function(response) {  
            if(response.status == 'Success') {
              SolutionService.getSolutionDetail({'solutionId': solutionId, 'token': $scope.token}).then(function(response) { 
                  if (response.status == 'Success') {
                      $scope.singleSolutionData=response.result[0];
                      $scope.webArticle = response.result[0].addArticle;
                      $scope.number = 5;
                      $scope.changeLoaderStatus(false);
                  }
              }, function(error) {

              });
            } else {
            }
            }, function(error) {

          });

        }
        $scope.follow = function(solutionId, isFollowing){
          $scope.followDetails.collectionName = 'solution';
          $scope.followDetails.collectionId = solutionId;
          $scope.followDetails.userId =  $scope.userDetails.userId;
          $scope.followDetails.userName =  $scope.userDetails.userName;
          $scope.followDetails.token = $scope.token; 
          if(isFollowing == 1) {
            $scope.followDetails.follow = true;
          } else {
            $scope.followDetails.follow = false;
          }
          $scope.followDetails.token = $scope.token;
          SolutionService.solutionFollowing($scope.followDetails).then(function(response) {  
            if(response.status == 'Success') {
              init();
              $scope.changeLoaderStatus(false);
            } else {
            }
          }, function(error) {
            $scope.changeLoaderStatus(false);
          });
        } 
        $scope.editSolution =function(){
            MainService.setActiveView('editSolutionDataView');
        }
        $scope.getNumber = function(num) {
          return new Array(num);   
        }
        $scope.discussion = function (x) {
          discussionData.body  = x.descriptionOfSolution;
          discussionData.discussionName = x.name;
          discussionData.discussionId  = x.discussions[0];
          $scope.discussionPath.pathName ='Solutions';
          $scope.discussionPath.solutionName = x.name;
          DiscussionService.setPath($scope.discussionPath);
          MainService.setDiscussionThread(discussionData);
          MainService.setActiveView('discussionsView');
        }
    }])