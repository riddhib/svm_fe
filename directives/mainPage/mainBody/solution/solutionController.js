angular.module('SVM')
    .directive('solution', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/solution/solution.html',
            controller: 'solutionController'
        }
    })
    .controller('solutionController', ['$scope', 'MainService', 'SolutionService','$timeout','DiscussionService', function($scope, MainService, SolutionService, $timeout, DiscussionService) {
        setTimeout(function(){
          $('.select2-show-search').select2({
            minimumResultsForSearch: ''
          });
        }, 100);
        $scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
        $scope.token = localStorage.getItem("token");
        var discussionData ={};
        $scope.solutionDetail = {};
        $scope.requiredSolutionData = [];
        $scope.followDetails = {};
        $scope.ratingDetails = {};
        $scope.solutionpersona = [];
        $scope.solutionCategories = [];
        $scope.path = {};
        $scope.sectorView = true;
        $scope.solCardViewValue = false;
        $scope.changeLoaderStatus = MainService.setLoaderStatus;
        $scope.solutionsDetail =function(id){
          SolutionService.setSolutionId(id);
          MainService.setActiveView('editSolutionView')
        }
        $scope.sectorNames = [
          {
            name:'All'
          },
          {
            name:'Agriculture'
          },
          {
            name:'Healthcare'
          },
          {
            name:'Transportation'
          },
          {
            name:'Water & Sanitation'
          },
          {
            name:'Connectivity'
          },
          {
            name:'Education'
          },
          {
            name:'Energy'
          },
          {
            name:'Livelihood'
          }
        ]
        function init() {
            SolutionService.getSolutionList($scope.token).then(function(response) {
                response.result.forEach(function(solution){
                  $scope.following = solution.following.filter(follow=> follow.userId == $scope.userDetails.userId);
                  if($scope.following.length > 0) {
                    solution.isFollowing = 1;
                  } else {
                    solution.isFollowing = 0;
                  }
                  $scope.requiredSolutionData.push(solution);
                });
                $scope.solutionList = $scope.requiredSolutionData;
                $scope.solutionCategories = response;
                $scope.number = 5;
                $scope.requiredSolutionData = [];
                //$scope.changeLoaderStatus(false);
            }, function(error) {

            })
        }
        
        $scope.rateSolution =  function(solutionId, ratingNo) {
          $scope.ratingDetails.collectionName = 'solution';
          $scope.ratingDetails.collectionId = solutionId;
          $scope.ratingDetails.ratingById =  $scope.userDetails.userId;
          $scope.ratingDetails.rating = ratingNo;
          $scope.ratingDetails.ratingBy =  $scope.userDetails.userName;
          $scope.ratingDetails.token = $scope.token;
          SolutionService.solutionRating($scope.ratingDetails).then(function(response) {  
            if(response.status == 'Success') {
              $scope.solutionList.forEach(function(solution){
                if(solution.solutionId == $scope.ratingDetails.collectionId ){
                  solution.avgRating =  response.result.avgRating ;
                  solution.totalRating =  response.result.totalRating ;
                }
              });
            } else {
            }
            }, function(error) {

          });
        }
        $scope.getNumber = function(num) {
          return new Array(num);   
        }
        $scope.follow = function(solutionId, isFollowing){
          $scope.followDetails.collectionName = 'solution';
          $scope.followDetails.collectionId = solutionId;
          $scope.followDetails.userId =  $scope.userDetails.userId;
          $scope.followDetails.userName =  $scope.userDetails.userName;
          $scope.followDetails.token = $scope.token;
          if(isFollowing) {
            $scope.followDetails.follow = false;
          } else {
            $scope.followDetails.follow = true;
          }
          SolutionService.solutionFollowing($scope.followDetails).then(function(response) {  
            if(response.status == 'Success') {
              $scope.solutionList.forEach(function(solutions){
                if(solutions.solutionId == $scope.followDetails.collectionId ){
                  solutions.isFollowing =  $scope.followDetails.follow ;
                }
              });
            
            } else {
            }
            }, function(error) {
    
          });
        }
        $scope.clear = function(){
          $scope.solutionDetail = {};
          $scope.validate = {};
        }  
        $scope.closeDialog = function(){
          $scope.solutionDetail = {};
          $scope.validate = {};
          jQuery("#modaldemo7").modal("hide");
        } 
        $scope.searchData = function (sector) {
          $scope.changeLoaderStatus(true);
          SolutionService.getSolutionList($scope.token).then(function(response) {
              response.result.forEach(function(solution){
                $scope.following = solution.following.filter(follow=> follow.userId == $scope.userDetails.userId);
                if($scope.following.length > 0) {
                  solution.isFollowing = 1;
                } else {
                  solution.isFollowing = 0;
                }
                $scope.requiredSolutionData.push(solution);
              });
              $scope.solutionList = $scope.requiredSolutionData;
              $scope.solutionCategories = response;
              $scope.number = 5;
              $scope.requiredSolutionData = [];
              $scope.sector = sector;
              $scope.sectorView = false;
              $timeout(function () {
                $('.select2-show-search').select2({
                  minimumResultsForSearch: ''
                });
                if (sector.length == 0 || sector == "All") {
                  $scope.solutionList = $scope.solutionCategories.result;
                  $scope.solutionpersona = $scope.solutionList;
                  $scope.changeLoaderStatus(false);
                } 
                else {
                  var matchedSolution = [];
                    $scope.solutionCategories.result.forEach(function (solution) {
                      if (solution.sector.includes(sector) && !matchedSolution.find(x => x.solutionId === solution.solutionId)) {
                        matchedSolution.push(solution);
                      }
                    });
                  $scope.solutionList = matchedSolution;
                  $scope.solutionpersona = $scope.solutionList;
                  matchedSolution = [];
                  $scope.changeLoaderStatus(false);
                }
              }, 100);
          }, function(error) {

          });          
        }
        $scope.searchByPersona = function (persona) {
            $scope.searchString = '';
            $timeout(function () {
              if (persona == "All") {
                $scope.solutionList = $scope.solutionpersona;
              }
              else {
                var matchedSolution = [];
                  $scope.solutionpersona.forEach(function (solution) {
                    if (solution.affiliated.includes(persona) && !matchedSolution.find(x => x.solutionId === solution.solutionId)) {
                      matchedSolution.push(solution);
                    }
                  });
                $scope.solutionList = matchedSolution;
                matchedSolution = [];
              }
            }, 100);
        }
        $scope.solutionHome = function() {
          $scope.sector = null;
          $scope.sectorView =  true;
        }

        $scope.imageUpload1 = function(event){
            var files = event.target.files; //FileList object            
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                $scope.image1 = file.name;
                var reader = new FileReader();
                reader.onload = $scope.imageIsLoaded; 
                reader.readAsDataURL(file);
            }
        }

        $scope.imageUpload2 = function(event){
            var files = event.target.files; //FileList object            
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                $scope.image2 = file.name;
                var reader = new FileReader();
                reader.onload = $scope.imageIsLoaded; 
                reader.readAsDataURL(file);
            }
        }
   
        $scope.imageIsLoaded = function(e){        
          $scope.$apply(function() {
              $scope.solutionDetail.logo = e.target.result;
          });
        }
        $scope.solChangeView = function () {
          if($scope.solCardViewValue){
            $scope.solCardViewValue = false;
          } else {
            $scope.solCardViewValue = true;
          }
        }
        $scope.loadSelect = function(){
          setTimeout(function(){
            $('#sector').select2({
              dropdownParent: $('#modaldemo7'),
              width : '100%'
            });
            $('#affiliated').select2({
              dropdownParent: $('#modaldemo7'),
              width : '100%'
            });
            $('#productCycle').select2({
              dropdownParent: $('#modaldemo7'),
              width : '100%'
            });
          }, 100)
        }
        $scope.discussion = function (x) {
          discussionData.body  = x.description;
          discussionData.discussionName = x.title;
          discussionData.discussionId  = x.discussions[0];
          $scope.path.pathName ='Solutions';
          $scope.path.solutionName = x.name;
          DiscussionService.setPath($scope.path);
          SolutionService.setSolutionId(x.solutionId);
          MainService.setDiscussionThread(discussionData);
          MainService.setActiveView('discussionsView');
        }
        $scope.saveSolution = function() {
          $scope.changeLoaderStatus(true);
          if($scope.image1) {$scope.solutionDetail.logoName = $scope.image1;}
          if($scope.image2) {$scope.solutionDetail.logoName = $scope.image2;}
          $scope.solutionDetail.dueDate = new Date($scope.solutionDetail.dueDate).getTime();
          $scope.solutionDetail.token = $scope.token;
          SolutionService.addSolution($scope.solutionDetail).then(function(response) {
            if (response.status == 'Success') {
              $scope.solutionDetail = {};
              $scope.validate = {};
              jQuery("#modaldemo7").modal("hide");
              $scope.changeLoaderStatus(false);
              init();
            }
            
          }, function (validate) {
            $scope.validate = validate;
            $scope.changeLoaderStatus(false);
          });
        }       
    }])