angular.module('SVM')
    .directive('editSolutionData', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/mainBody/solution/editSolutionData/editSolutionData.html',
            controller: 'editSolutionDataController'
        }
    })
    .controller('editSolutionDataController', ['$scope', 'MainService','LoginService', 'SolutionService', function($scope,MainService,LoginService, SolutionService) {
        $scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
        $scope.token = localStorage.getItem("token");
        $scope.newchallengeData={};
        $scope.solutionUpdateDetails ={};
        $scope.solutionUpdateImpact={};
        $scope.solutionUpdateDescription= {};
        $scope.addArticle= {};
        $scope.articleData= {}; 
        $scope.deleteResource = {};
        $scope.solutionId = SolutionService.getSolutionId();
        $scope.changeLoaderStatus = MainService.setLoaderStatus;
        $scope.changeLoaderStatus(true);
        function init(){
            SolutionService.getSolutionDetail({'solutionId': $scope.solutionId, 'token': $scope.token}).then(function(response) { 
                if (response.status == 'Success') {
                    $scope.singleSolutionData=response.result[0];
                    $scope.webArticle = response.result[0].addArticle;
                    $scope.singleSolutionData.dueDate = moment($scope.singleSolutionData.dueDate).format("MM-DD-YYYY") || '';
                    var markupStr1 = $scope.singleSolutionData.description;
                    var markupStr2 = $scope.singleSolutionData.impact;
                    setTimeout(function(){
                      $('.select2').select2({
                        minimumResultsForSearch: Infinity
                      });
                      $('.select2-show-search').select2({
                        minimumResultsForSearch: ''
                      });
                      $('#impact').summernote({
                          placeholder: 'Impact..',
                          height: 150,
                          tooltip: false
                        });
                      $('#description').summernote({
                          placeholder: 'Description..',
                          height: 150,
                          tooltip: false
                        });
                    }, 100);
                    $('#description').summernote('code', markupStr1);
                    $('#impact').summernote('code', markupStr2);
                    $scope.changeLoaderStatus(false);
                }
            }, function(error) {
    
            });
            SolutionService.getArticleData({'solutionId': $scope.solutionId, 'token': $scope.token}).then(function(response) { 
                if (response.status == 'Success') {
                    $scope.getDocumentData=response.result; 
                    $scope.changeLoaderStatus(false);  
                }
            }, function(error) {
    
            });
        }
        init();
        $scope.solutionPrimaryDetails = function(){
            setLoading("primaryDetails");
            $scope.successMessage1 = null;
            $scope.errorMessage1 = null;
            $scope.solutionUpdateDetails.solutionId = $scope.solutionId;
            $scope.solutionUpdateDetails.name = $scope.singleSolutionData.name;
            $scope.solutionUpdateDetails.sector = $scope.singleSolutionData.sector;
            $scope.solutionUpdateDetails.location = $scope.singleSolutionData.location;
            $scope.solutionUpdateDetails.type = $scope.singleSolutionData.type;
            $scope.solutionUpdateDetails.descriptionOfSolution = $scope.singleSolutionData.descriptionOfSolution;
            $scope.solutionUpdateDetails.logo = $scope.singleSolutionData.logo;
            $scope.solutionUpdateDetails.logoName = $scope.image;
            if($scope.singleSolutionData.website) {
                $scope.solutionUpdateDetails.website = $scope.singleSolutionData.website.replace(/^\/\/|^.*?:(\/\/)?/, '');
            }
            $scope.solutionUpdateDetails.affiliated = $scope.singleSolutionData.affiliated;
            $scope.solutionUpdateDetails.dueDate = new Date($scope.singleSolutionData.dueDate).getTime();
            $scope.solutionUpdateDetails.token = $scope.token;
            SolutionService.updateSolution($scope.solutionUpdateDetails).then(function(response) {  
                if(response.status == 'Success') {
                    $scope.validate = {}
                    SolutionService.getSolutionDetail({'solutionId': $scope.solutionId, 'token': $scope.token}).then(function(response) { 
                        if (response.status == 'Success') {
                            $scope.singleSolutionData=response.result[0];
                            $scope.singleSolutionData.dueDate = moment($scope.singleSolutionData.dueDate).format("MM-DD-YYYY") || '';
                            var markupStr1 = $scope.singleSolutionData.description;
                            var markupStr2 = $scope.singleSolutionData.impact;
                            $('#description').summernote('code', markupStr1);
                            $('#impact').summernote('code', markupStr2);
                        }
                    }, function(error) {
            
                    });
                    clearLoading("primaryDetails");
                    $scope.successMessage1 = "Technology details updated successfully";
                    setTimeout(function() {
                        $('#successMessage1').fadeOut('fast');
                    }, 2500);           
                } else if(response.error){
                    $scope.validate = {};
                    clearLoading("primaryDetails");
                    $scope.errorMessage1 = response.error;
                    setTimeout(function() {
                      $('#errorMessage1').fadeOut('fast');
                    }, 2500);
                }
               
            }, function(validate) {
                $scope.validate = validate;
                clearLoading("primaryDetails");
            });
        }
        $scope.imageUpload = function(event){
            var files = event.target.files; //FileList object
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                $scope.image = file.name;
                var reader = new FileReader();
                reader.onload = $scope.imageIsLoaded; 
                reader.readAsDataURL(file);
            }
        }
   
       $scope.imageIsLoaded = function(e){        
            $scope.$apply(function() {
                $scope.singleSolutionData.logo = e.target.result;
            });
        }
        $scope.description = function() {
            setLoading("description");
            $scope.successMessage2 = null;
            $scope.errorMessage2 = null;
            $scope.solutionUpdateDescription.solutionId = $scope.solutionId;
            $scope.solutionUpdateDescription.description = $('#description').summernote('code');
            $scope.solutionUpdateDescription.token = $scope.token;
            SolutionService.solutionUpdateDescription($scope.solutionUpdateDescription).then(function(response) {  
                if(response.status == 'Success') {
                    $scope.validate = {};
                    SolutionService.getSolutionDetail({'solutionId': $scope.solutionId, 'token': $scope.token}).then(function(response) { 
                        if (response.status == 'Success') {
                            $scope.singleSolutionData=response.result[0];
                            $scope.singleSolutionData.dueDate = moment($scope.singleSolutionData.dueDate).format("MM-DD-YYYY") || '';
                            var markupStr1 = $scope.singleSolutionData.description;
                            var markupStr2 = $scope.singleSolutionData.impact;
                            $('#description').summernote('code', markupStr1);
                            $('#impact').summernote('code', markupStr2);
                        }
                    }, function(error) {
            
                    }); 
                    clearLoading("description");
                    $scope.successMessage2 = "Technology details updated successfully";
                    setTimeout(function() {
                        $('#successMessage2').fadeOut('fast');
                    }, 2500);                
                } else if(response.error){
                    $scope.validate = {};
                    clearLoading("description");
                    $scope.errorMessage2 = response.error;
                    setTimeout(function() {
                      $('#errorMessage2').fadeOut('fast');
                    }, 2500);
                }
            }, function(validate) {
                $scope.validate = validate;
                clearLoading("description");
            });
        }
        $scope.impact = function() {
            setLoading("impact");
            $scope.successMessage3 = null;
            $scope.errorMessage3 = null;
            $scope.solutionUpdateImpact.solutionId = $scope.solutionId;
            $scope.solutionUpdateImpact.impact = $('#impact').summernote('code');
            $scope.solutionUpdateImpact.token = $scope.token;
            SolutionService.solutionUpdateImpact($scope.solutionUpdateImpact).then(function(response) {  
                if(response.status == 'Success') {
                    $scope.validate = {};
                    SolutionService.getSolutionDetail({'solutionId': $scope.solutionId, 'token': $scope.token}).then(function(response) { 
                        if (response.status == 'Success') {
                            $scope.singleSolutionData=response.result[0];
                            $scope.singleSolutionData.dueDate = moment($scope.singleSolutionData.dueDate).format("MM-DD-YYYY") || '';
                            var markupStr1 = $scope.singleSolutionData.description;
                            var markupStr2 = $scope.singleSolutionData.impact;
                            $('#description').summernote('code', markupStr1);
                            $('#impact').summernote('code', markupStr2);
                        }
                    }, function(error) {
            
                    }); 
                    clearLoading("impact"); 
                    $scope.successMessage3 = "Technology details updated successfully";
                    setTimeout(function() {
                        $('#successMessage3').fadeOut('fast');
                    }, 2500);            
                } else if(response.error){
                    $scope.validate = {};
                    clearLoading("impact");
                    $scope.errorMessage3 = response.error;
                    setTimeout(function() {
                      $('#errorMessage3').fadeOut('fast');
                    }, 2500);
                }
            }, function(validate) {
                $scope.validate = validate
                clearLoading("impact");
            });
        }
        $scope.addArticles = function() {
            setLoading("webArticles");
            $scope.successMessage5 = null;
            $scope.errorMessage5 = null;
            $scope.articleData.solutionId = $scope.solutionId;
            $scope.articleData.addArticle = [];
            if($scope.addArticle.link) {
                let url = $scope.addArticle.link.replace(/^\/\/|^.*?:(\/\/)?/, '');
                $scope.addArticle.link = url;
            }
            let addArticle = $scope.singleSolutionData.addArticle;
            addArticle.push($scope.addArticle);
            $scope.articleData.addArticle = addArticle;
            $scope.articleData.token = $scope.token;
            SolutionService.solutionUpdateWebArticles($scope.articleData).then(function(response) {  
                if(response.status == 'Success') {
                    $scope.validate = {};
                    SolutionService.getSolutionDetail({'solutionId': $scope.solutionId, 'token': $scope.token}).then(function(response) { 
                        if (response.status == 'Success') {
                            $scope.singleSolutionData=response.result[0];
                            $scope.webArticle = response.result[0].addArticle;
                            var markupStr1 = $scope.singleSolutionData.description;
                            var markupStr2 = $scope.singleSolutionData.impact;
                            $('#description').summernote('code', markupStr1);
                            $('#impact').summernote('code', markupStr2);
                            $scope.addArticle = {};
                        }
                    }, function(error) {
            
                    }); 
                    clearLoading("webArticles");
                    $scope.successMessage5 = "Technology details updated successfully";
                    setTimeout(function() {
                        $('#successMessage5').fadeOut('fast');
                    }, 2500);               
                } else if(response.error){
                    $scope.validate = {};
                    clearLoading("webArticles");
                    $scope.errorMessage5 = response.error;
                    setTimeout(function() {
                      $('#errorMessage5').fadeOut('fast');
                    }, 2500);
                }
            }, function(validate) {
                $scope.validate = validate;
                clearLoading("webArticles");
            });
        }
        $scope.deleteResources = function(fileId) {
            $scope.deleteResource.solutionId = $scope.singleSolutionData.solutionId;
            $scope.deleteResource.article = $scope.singleSolutionData.article.filter(art=> art != fileId);
            $scope.deleteResource.token = $scope.token;
            SolutionService.deleteResource($scope.deleteResource).then(function(response){
                if(!response.error){
                    $scope.deleteResource = {};
                    init();
                }
            }, function(error){
                console.log(error);
            })
        }
        $scope.deleteWebArticle = function(index) {
            $scope.articleData.solutionId = $scope.solutionId;
            $scope.articleData.addArticle = [];
            $scope.singleSolutionData.addArticle.splice(index,1);
            let addArticle = $scope.singleSolutionData.addArticle;
            $scope.articleData.addArticle = addArticle;
            $scope.articleData.token = $scope.token;
            SolutionService.solutionUpdateWebArticles($scope.articleData).then(function(response) {  
                if(response.status == 'Success') {
                    $scope.validate = {};
                    init();              
                }
            }, function() {
            });
        }
        $scope.fileNameChanged = function (file) {
            var selectedFile = file.files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
                $scope.$apply(function () {
                    $scope.src = e.target.result;
                    $scope.name = selectedFile.name;
                });
            };
            reader.readAsDataURL(selectedFile);
            $scope.$apply(function () {
                if (selectedFile != undefined) {
                    $scope.document = selectedFile;
                }
            });
        }
        $scope.upload = {};
        $scope.resource = function() {
            setLoading("resources");
            $scope.successMessage4 = null;
            $scope.errorMessage4 = null;
            if ($scope.upload) {            
                $scope.upload.file = $scope.document;
                $scope.upload.userId =$scope.userDetails.userId;
                $scope.upload.solutionId = $scope.solutionId;
                $scope.upload.uploadBy = $scope.userDetails.userName;
                $scope.upload.token = $scope.token;
            }
            SolutionService.uploadResource($scope.upload).then(function (response) {
                if (response.status == 'Success') {
                    $scope.validate = {};
                    $scope.upload = {};
                    $scope.name = '';
                    init();
                    clearLoading("resources");
                    $scope.successMessage4 = "Technology details updated successfully";
                    setTimeout(function() {
                        $('#successMessage4').fadeOut('fast');
                    }, 2500);
                } else if(response.error){
                    $scope.validate = {};
                    clearLoading("resources");
                    $scope.errorMessage4 = response.error;
                    setTimeout(function() {
                      $('#errorMessage4').fadeOut('fast');
                    }, 2500);
                }
                $scope.validate = {};
            }, function (validate) {
                $scope.validate = validate;
                clearLoading("resources");
                // var firstValidatedElement = Object.keys($scope.validate)[0];
                //document.getElementById(firstValidatedElement).scrollIntoView(true);
            });
        
        }
        $scope.editSolution =function(view){
            MainService.setActiveView(view);
        }
        var setLoading = function (id) {
            if(id=="primaryDetails"){
                var search = $('#primaryDetails');
            } else if(id=="description"){
                var search = $('#description');
            } else if(id=="impact"){
                var search = $('#impact');
            } else if(id=="resources"){
                var search = $('#resources');
            } else if(id=="webArticles"){
                var search = $('#webArticles');
            } 
            if (!search.data('normal-text')) {
              search.data('normal-text', search.html());
            }
            search.html(search.data('loading-text'));
        };
        var clearLoading = function (id) {
            if(id=="primaryDetails"){
                var search = $('#primaryDetails');
            } else if(id=="description"){
                var search = $('#description');
            } else if(id=="impact"){
                var search = $('#impact');
            } else if(id=="resources"){
                var search = $('#resources');
            } else if(id=="webArticles"){
                var search = $('#webArticles');
            } 
            search.html(search.data('normal-text'));
        };
    }])