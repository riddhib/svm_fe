angular.module('SVM')
    .directive('userMessages', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/userMessages/userMessages.html',
            controller: 'UserMessagesController'
        }
    })
    .controller('UserMessagesController', ['$scope', 'MainService', 'LoginService', 'ChallengeService', 'MessagesService', 
        function ($scope, MainService, LoginService, ChallengeService, MessagesService) {
            $scope.view = MessagesService.getActiveView;
    }]);