angular.module('SVM')
    .directive('messageList', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/userMessages/messageList/messageList.html',
            controller: 'MessageListController'
        }
    })
    .controller('MessageListController', ['$scope', 'MainService', 'LoginService', 'ChallengeService', 
    'MessagesService', '$filter', function ($scope, MainService, LoginService, ChallengeService, MessagesService, $filter) {
        let userDetails = JSON.parse(localStorage.getItem('userDetails'));
        $scope.token = localStorage.getItem("token");
        $scope.activeUserMsg = $scope.activeUserMsg || {};
        $scope.list = [];
        
        $scope.getMessageList = function() {
            MessagesService.getMessageList({"userId": userDetails.userId, "token": $scope.token}).then(function (response) {
                if (!response.error) {
                    $scope.list = response.result;
                    getActiveUser();
                } else {
                    console.log(response);
                }
            });
        }

        $scope.getMessageList();
        $scope.$parent.getMessageList = $scope.getMessageList;

        // setInterval(function(){
        //     $scope.getMessageList();
        // }, 10 * 1000);

        $scope.openSelectedChat = function(item, type) {
            $scope.list = $scope.list.map(element => {
                element.isActive = element.userId[0] == item.userId[0] ? true : false;
                return element;
            });
            if (type != 'new') {
                var data = {
                    userId: userDetails.userId,
                    recipientId: item.userId[0],
                    token: $scope.token
                }
                MessagesService.getuserMessages(data).then(function (response) {
                    if (!response.error) {
                        MessagesService.setData({list: response.result, recepientDetails: item});
                        MessagesService.setActiveView('messages');
                    } else {
                        console.log(response);
                    }
                });
            } else {
                MessagesService.setData({list: [], recepientDetails: item});
                MessagesService.setActiveView('messages');
            }
        }

        function getActiveUser() {
            let item = $scope.list.find(listItem => listItem.userId[0] == $scope.activeUserMsg.userId);
            if (item) {
                $scope.openSelectedChat(item);
            } else if ($scope.activeUserMsg && $scope.activeUserMsg.userId > 0) {
                item = {
                    createdDate: new Date().getTime(),
                    isOnline: $scope.activeUserMsg.isOnline,
                    logo: $scope.activeUserMsg.logo,
                    messages: [{message: '', date: new Date().getTime()}],
                    userId: [$scope.activeUserMsg.userId],
                    userName: $scope.activeUserMsg.userName,
                    isActive: true
                };
                $scope.list = [item, ...$scope.list];
                $scope.openSelectedChat(item, 'new');
            } else if ($scope.list.length && $scope.list[0]) {
                $scope.openSelectedChat($scope.list[0]);
            }
        }
    }]);