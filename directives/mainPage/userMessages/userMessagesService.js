angular.module('SVM')
    .factory('MessagesService', ['$http', '$q', function ($http, $q) {
        var view = {
            messages: false
        }, data = {};
        return {
            getActiveView: function(){
                return view;
            },
            getData: function(){
                return data;
            },
            getMessageList: function (sender) {
                var defered = $q.defer();
                $http.get('ui/query/userMessagingList/'+ sender.userId, {
                    headers: {'token': sender.token}
                }).then(function (response) {
                        defered.resolve(response.data);
                    }, function (error) {
                        defered.reject(error);
                    });
                return defered.promise;
            },
            setActiveView: function(item){
                view = {
                    messages: false
                };
                view[item] = true;
            },
            setData: function(obj){
                data = obj;
            },
            getuserMessages: function(data) {
                var defered = $q.defer();
                $http.get('ui/query/userMessages/'+ data.userId +'/'+ data.recipientId, {
                    headers: {'token': data.token}
                }).then(function (response) {
                        defered.resolve(response.data);
                    }, function (error) {
                        defered.reject(error);
                    });
                return defered.promise;
            },
            sendUserMessages: function(data) {
                var defered = $q.defer();
                $http.post('/ui/user/messages', data, {
                    headers: {'token': data.token}
                }).then(function (response) {
                        defered.resolve(response.data);
                    }, function (error) {
                        defered.reject(error);
                    });
                return defered.promise;
            }
        }
    }]);