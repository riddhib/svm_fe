angular.module('SVM')
    .directive('messenger', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/userMessages/messenger/messenger.html',
            controller: 'MessengerController'
        }
    })
    .controller('MessengerController', ['$scope', 'MainService', 'LoginService', 'ChallengeService', 'MessagesService',
        function ($scope, MainService, LoginService, ChallengeService, MessagesService) {
            $scope.userDetails = JSON.parse(localStorage.getItem('userDetails'));
            $scope.token = localStorage.getItem("token");
            $scope.currentList = MessagesService.getData;
            $scope.msgOnKeyUp = function (msg, event) {
                if(event.keyCode === 13){
                    $scope.sendMessage(msg)
                }
            }
            $scope.sendMessage = function (msg) {
                var data = {
                    "senderId": $scope.userDetails.userId,
                    "recipientId": $scope.currentList().recepientDetails.userId[0],
                    "messages": [{
                        "date": new Date().getTime(),
                        "message": msg
                    }],
                    "token": $scope.token
                };
                MessagesService.sendUserMessages(data)
                .then(function (response) {
                    if (!response.error) {
                        MessagesService.setData({list: response.result, 
                            recepientDetails: $scope.currentList().recepientDetails});
                        $scope.msg = '';
                        $scope.$parent.getMessageList();
                    } else {
                        console.log(response);
                    }
                });
            }
            $scope.$watch('currentList().list', function (newValue, oldValue) {
                setTimeout(function () {
                    $scope.currentList().list = newValue;
                    var objDiv = document.getElementById("messenger");
                    objDiv.scrollTop = objDiv.scrollHeight;
                }, 100);
            })

        }]);