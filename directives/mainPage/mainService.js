angular.module('SVM')
    .factory('MainService', ['$http', '$q', function($http, $q) { 
        var view = {
            landingView:true,
            challengeOpenView:false,
            signUpView:false,
            challengeView:false,
            userMessageView: false,
            editChallengeView:false,
            userProfileView:false,
            editProfileView:false,
            editChallengeDataView:false,
            discussionsView:false,
            solutionView:false,
            mainBodyView:true,
            editSolutionView:false,
            collaborateView:false,
            editCollaborateView:false,
            editCollaborateDataView:false,
            projectView:false,
            projectListView:false,
            researchView:false,
            projectDataView:false,
            editProjectDataView:false,
            userFollowView :false,
            resetPasswordView:false,
            projectProfilesView: false,
            projectFolderUsersView: false,
            settingsView: false,
            researchProjectsView: false,
            villageBulletinView: false,
            villageBulletinDataView:false,
            villageBulletinInformationView:false,
            activateUserView:false,
            errorPageView:false,
            requestInformationView:false
        };
        var disccussionData = {};
        var loaderStatusValue = false;
        var value = "";
        return {
            getActiveView: function() {
                return view;
            },
            setActiveView: function(viewName) {
                 view = {
                    landingView:false,
                    challengeOpenView:false,
                    signUpView:false,
                    challengeView:false,
                    editChallengeView:false,
                    userProfileView:false,
                    editProfileView:false,
                    editChallengeDataView:false,
                    userMessageView: false,
                    discussionsView:false,
                    solutionView:false,
                    mainBodyView:true,
                    editSolutionView:false,
                    editSolutionDataView:false,
                    collaborateView:false,
                    editCollaborateView:false,
                    editCollaborateDataView:false,
                    projectView:false,
                    projectListView:false,
                    researchView:false,
                    projectDataView:false,
                    editProjectDataView:false,
                    userFollowView :false,
                    resetPasswordView:false,
                    projectProfilesView: false,
                    projectFolderUsersView: false,
                    settingsView: false,
                    researchProjectsView: false,
                    villageBulletinView: false,
                    villageBulletinDataView:false,
                    villageBulletinInformationView:false,
                    activateUserView:false,
                    errorPageView:false,
                    requestInformationView:false
                };
                view[viewName] = true;
            },
            setDiscussionThread: function(discussion) {
                disccussionData = discussion || {};
            },
            getDiscussionThread: function(){
                return disccussionData;
            },
            setLoaderStatus: function(boolValue) {
                loaderStatusValue = boolValue;
            },
            getLoaderStatus: function() {
                return loaderStatusValue;  
            },
            setErrorPageValue: function(val) {
                value = val;
            },
            getErrorPageValue: function() {
                return value;
            }
        }
    }]);