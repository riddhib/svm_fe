angular.module('SVM')
    .directive('editProfile', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: '/directives/mainPage/userProfile/editProfile/editProfile.html',
            controller: 'editProfileController'
        }
    })
    .controller('editProfileController', ['$scope','MainService','LoginService', 'UserService', function($scope,MainService,LoginService, UserService) {
        $scope.upload = {};
        
        $scope.viewProfile = function(){
            MainService.setActiveView('userProfileView');
        }

        $scope.signOut = function(){
            MainService.setActiveView('signUpView');
            localStorage.clear();
        }
        $scope.token = localStorage.getItem("token");
        $scope.changeLoaderStatus = MainService.setLoaderStatus;
        $scope.changeLoaderStatus(true);
        $scope.userprofileDetails ={};
        $scope.userPassword ={};
        $scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
        
        UserService.getEditUser({'userId': $scope.userDetails.userId, 'token': $scope.token}).then(function(response) {
            setTimeout(function(){
                $('#dateMask').mask('99/99/9999');
                $('#phoneMask').mask('(999)-999-9999');
                $('#ssnMask').mask('999-99-9999');

                // Time Picker
                $('#tpBasic').timepicker();
                $('#tp2').timepicker({'scrollDefault': 'now'});
                $('#tp3').timepicker();
                $('.select2').select2({
                    minimumResultsForSearch: Infinity
                });
                $('.select2-show-search').select2({
                    minimumResultsForSearch: ''
                });
            }, 100);  
            if (response.status == 'Success') {
                $scope.user = response.result.user;
                $scope.files= response.result.files;
                $scope.profile = response.result.profile;
                $scope.user.dob = moment($scope.user.dob).format("MM-DD-YYYY") || '';
                $scope.changeLoaderStatus(false);
            }
        });

        $scope.updateProfile = function(){
            $scope.successMessage41 = null;
            $scope.errorMessage41 = null;
            $scope.userprofileDetails.userId = $scope.userDetails.userId;
            $scope.userprofileDetails.firstName = $scope.user.firstName;
            $scope.userprofileDetails.lastName = $scope.user.lastName;
            $scope.userprofileDetails.type = $scope.user.type;
            $scope.userprofileDetails.areaOfInterest = $scope.user.areaOfInterest;
            $scope.userprofileDetails.logo = $scope.user.logo;
            $scope.userprofileDetails.logoName = $scope.image;
            $scope.userprofileDetails.token = $scope.token;
            localStorage.setItem('userDetailsLogo', JSON.stringify($scope.userprofileDetails.logo));
            UserService.updateUser($scope.userprofileDetails).then(function(response) {  
                if(response.status == 'Success') {
                    $scope.$parent.userDetailsLogo = $scope.user.logo;
                    $scope.validate = {};
                    $scope.successMessage1 = "User details updated successfully";
                    setTimeout(function() {
                        $('#successMessage1').fadeOut('fast');
                    }, 2500);
                    UserService.getUser({'userId': $scope.userDetails.userId, 'token': $scope.token}).then(function(response) {
                        if (response.status == 'Success') {
                            $scope.user=response.result[0]; 
                        }
                    }, function(error) {
            
                    });                
                } else if(response.error){
                    $scope.validate = {};
                    $scope.errorMessage1 = response.error;
                    setTimeout(function() {
                      $('#errorMessage1').fadeOut('fast');
                    }, 3000);
                }
               // <top-nav></top-nav>
                MainService.setActiveView('editProfileView');
            }, function(validate) {
                $scope.validate = validate;
            });
        }
        
        $scope.personalDetails = function(){
            $scope.successMessage2 = null;
            $scope.errorMessage2 = null;
            $scope.userprofileDetails.userId = $scope.userDetails.userId;
            $scope.userprofileDetails.companyName = $scope.user.companyName;
            $scope.userprofileDetails.gender = $scope.user.gender;
            $scope.userprofileDetails.designation = $scope.user.designation;
            $scope.userprofileDetails.linkedin = $scope.user.linkedin;
            $scope.userprofileDetails.location = $scope.user.location;
            $scope.userprofileDetails.dob = new Date($scope.user.dob).getTime();
            $scope.userprofileDetails.website = $scope.user.website;
            $scope.userprofileDetails.about = $scope.user.about;
            $scope.userprofileDetails.token = $scope.token;
            if ($scope.user.phone && typeof $scope.user.phone == 'string' ) {
                $scope.userprofileDetails.phone = $scope.user.phone.replace(/[()-]/g,'');
            }
            UserService.updatePersonalDetails($scope.userprofileDetails).then(function(response) {  
                if(response.status == 'Success') {
                    $scope.validate = {};
                    $scope.successMessage2 = "Personal details updated successfully";
                    setTimeout(function() {
                        $('#successMessage2').fadeOut('fast');
                    }, 2500);
                } else if(response.error){
                    $scope.validate = {};
                    $scope.errorMessage2 = response.error;
                    setTimeout(function() {
                      $('#errorMessage2').fadeOut('fast');
                    }, 3000);
                }
            }, function(validate) {
                $scope.validate = validate;
            });
        }
        
        $scope.challenge = function(){
            MainService.setActiveView('challengeView');
        }
        
        $scope.changePassword = function() {
            $scope.successMessage3 = null;
            $scope.errorMessage3 = null;
            $scope.currentPasswordError = false;
            $scope.userPassword.userId = $scope.userDetails.userId;
            $scope.userPassword.password = $scope.password;
            $scope.userPassword.newPassword = $scope.newPassword;
            $scope.userPassword.confirmPassword = $scope.confirmPassword;
            $scope.userPassword.token = $scope.token;
            UserService.changePass($scope.userPassword).then(function(response) {  
                if(response.status == 'Success') {
                    $scope.validate = {};
                    $scope.password = '';
                    $scope.newPassword = '';
                    $scope.confirmPassword ='';
                    $scope.successMessage3 = "Password details updated successfully";
                    setTimeout(function() {
                        $('#successMessage3').fadeOut('fast');
                    }, 2500);
                } else if(response.error){
                    $scope.validate = {};
                    $scope.errorMessage3 = response.error;
                    setTimeout(function() {
                      $('#errorMessage3').fadeOut('fast');
                    }, 3000);
                }
            }, function(validate) {
                $scope.validate = validate;
            });
        }

        $scope.imageUpload = function(event){
            var files = event.target.files; //FileList object
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                $scope.image = file.name;
                var reader = new FileReader();
                reader.onload = $scope.imageIsLoaded; 
                reader.readAsDataURL(file);
            }
        }
   
       $scope.imageIsLoaded = function(e){        
            $scope.$apply(function() {
                $scope.user.logo = e.target.result;
            });
       }

        $scope.categoryChanged = function (category) {
            $scope.category = category;
        }

        $scope.fileNameChanged1 = function (file) {
            var selectedFile = file.files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
                $scope.$apply(function () {
                    $scope.src = e.target.result;
                    $scope.name = selectedFile.name;
                    $scope.fileName1 = selectedFile.name;
                });
            };
            reader.readAsDataURL(selectedFile);
            $scope.$apply(function () {
                if (selectedFile != undefined) {
                    $scope.document = selectedFile;
                }
            });
        }

        $scope.fileNameChanged2 = function (file) {
            var selectedFile = file.files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
                $scope.$apply(function () {
                    $scope.src = e.target.result;
                    $scope.name = selectedFile.name;
                    $scope.fileName2 = selectedFile.name;
                });
            };
            reader.readAsDataURL(selectedFile);
            $scope.$apply(function () {
                if (selectedFile != undefined) {
                    $scope.document = selectedFile;
                }
            });
        }

        $scope.saveProfile = function() {
            $scope.successMessage4 = null;
            $scope.errorMessage4 = null;
            if ($scope.upload) {
                $scope.upload.file = $scope.document;
                $scope.upload.userId =$scope.userDetails.userId;
                $scope.upload.token = $scope.token;
            }
            UserService.uploadProfile($scope.upload).then(function (response) {
                if (response.status == 'Success') {
                    $scope.successMessage4 = "Profile uploaded successfully";
                    $scope.upload = {};
                    setTimeout(function() {
                        $('#successMessage4').fadeOut('fast');
                    }, 2500);
                } else if(response.error){
                    $scope.errorMessage4 = response.error;
                    setTimeout(function() {
                      $('#errorMessage4').fadeOut('fast');
                    }, 3000);
                }
                $scope.validate = {};
            }, function (validate) {
                $scope.validate = validate;
            });
        
        }

        $scope.saveDoc = function() {
            $scope.successMessage5 = null;
            $scope.errorMessage5 = null;
            if ($scope.upload) {
                $scope.upload.file = $scope.document;
                $scope.upload.userId =$scope.userDetails.userId;
                $scope.upload.category = $scope.category;
                $scope.upload.token = $scope.token;
            }
            UserService.uploadDocuments($scope.upload).then(function (response) {
                if (response.status == 'Success') {
                    $scope.successMessage5 = "Document details uploaded successfully";
                    $scope.upload = {};
                    setTimeout(function() {
                        $('#successMessage5').fadeOut('fast');
                    }, 2500);
                } else if(response.error){
                    $scope.errorMessage5 = response.error;
                    setTimeout(function() {
                      $('#errorMessage5').fadeOut('fast');
                    }, 3000);
                }
                $scope.validate = {};
            }, function (validate) {
                $scope.validate = validate;
                var firstValidatedElement = Object.keys($scope.validate)[0];
                document.getElementById(firstValidatedElement).scrollIntoView(true);
            });
        
        }
      
    }])