angular.module('SVM')
    .directive('userProfile', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/userProfile/userProfile.html',
            controller: 'userProfileController'
        }
    })
    .controller('userProfileController', ['$scope','MainService','LoginService', 'ChallengeService', 'UserService','$timeout','ProjectService','DiscussionService', function($scope,MainService,LoginService, ChallengeService, UserService, $timeout, ProjectService,DiscussionService) {
        $scope.projectInfo = {};
        $scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
        $scope.token = localStorage.getItem("token");
        $scope.userDetailsLogo = JSON.parse(localStorage.getItem("userDetailsLogo"));
        if(UserService.getUserId()) {
            $scope.userId = UserService.getUserId();
        } else {
            $scope.userId = $scope.userDetails.userId;
        }
        $scope.path = UserService.getPath() || ProjectService.getProjectFolderProfilePath() || DiscussionService.getDiscussionProfilePath();
        $scope.changeLoaderStatus = MainService.setLoaderStatus;
        $scope.changeLoaderStatus(true);
        UserService.getEditUser({'userId': $scope.userId, 'token': $scope.token}).then(function(response) {            
            if (response.status == 'Success') {
                $scope.user=response.result.user;
                if($scope.user.phone == 0) {
                    $scope.user.phone = '';
                }
                else {
                    var USNumber = $scope.user.phone.toString().match(/(\d{3})(\d{3})(\d{4})/);
                    USNumber = "(" + USNumber[1] + ")-" + USNumber[2] + "-" + USNumber[3];
                    $scope.user.phone = USNumber;
                }
                $scope.profiles = response.result.profile;
                $scope.files = response.result.files; 
                $scope.changeLoaderStatus(false);
            }
        }, function(error) {

        });
        UserService.setPath();
        ProjectService.setProjectFolderProfilePath();
        DiscussionService.setDiscussionProfilePath();
        $scope.projectViews = function(view) {
            if(view == 'discussionsView') {
                MainService.setDiscussionThread();
            }
          MainService.setActiveView(view); 
        }

        $scope.discussionThreadView = function() {
            var discussionData ={};
            discussionData.discussionName = $scope.path.discussionName;
            discussionData.discussionId  = $scope.path.discussionId;
            MainService.setDiscussionThread(discussionData);
            MainService.setActiveView('discussionsView');
        }
        $scope.addUsers = function(projectName,data) {
          ProjectService.setProjectUsersFilter(data);
          ProjectService.setProjectName( projectName );
          MainService.setActiveView('projectFolderUsersView');
        }
        
    }])
