angular.module('SVM')
    .factory('UserService', ['$http', '$q', 'ValidateService', function ($http, $q, ValidateService) {
        var userId = null;
        var path = null;
        return {
            logoutUser: function(token) {
                var defered = $q.defer();
                $http.get('/ui/logout/'+token, {
                    headers: {token: token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            }, 
        	updateUser: function(obj) {
                var defered = $q.defer();
                ValidateService.validateEditUser(obj)
                    .then(function () {
                        $http.put('/ui/user/', obj, {
                            headers: {token: obj.token}
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function (validate) {
                        defered.reject(validate);
                    });
                return defered.promise;
            },
            updatePersonalDetails: function(obj) {
                var defered = $q.defer();
                ValidateService.validateUserPersonalDetails(obj)
                    .then(function () {
                        $http.put('/ui/user/', obj, {
                            headers: {token: obj.token}
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function (validate) {
                        defered.reject(validate);
                    });
                return defered.promise;
            },
            changePass: function(obj) {
                var defered = $q.defer();
                ValidateService.validateChangePassword(obj)
                    .then(function () {
                        $http.put('/ui/changepassword/', obj, {
                            headers: {token: obj.token}
                        }).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function (validate) {
                        defered.reject(validate);
                    });
                return defered.promise;
            },
            getUser: function(user) {
                var defered = $q.defer();
                $http.get('/ui/user/'+user.userId, {
                    headers: {token: user.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            }, 
            getUsers: function(token) {
                var defered = $q.defer();
                $http.get('/ui/query/user', {
                    headers: {token: token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            }, 
            getEditUser: function(user) {
                var defered = $q.defer();
                $http.get('/ui/editProfile/'+user.userId, {
                    headers: {token: user.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            }, 
            getFollowing: function(user) {
                var defered = $q.defer();
                $http.get('/ui/iamFollowing/'+user.userId, {
                    headers: {token: user.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(error);
                });
                return defered.promise;
            }, 
        	uploadProfile: function (data) {
                var defered = $q.defer();
	            var fd = new FormData();
	            fd.append("upload", data.file);
	            var details = JSON.stringify({"fileName": data.filename1,"userId": data.userId});
                fd.append("advanceDetails", details);
	            $http.post('/ui/profileUpload', fd, {
	            	transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined, 'token': data.token }
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
	            return defered.promise;
            },
            uploadDocuments: function (data) {
                var defered = $q.defer();
	            var fd = new FormData();
	            fd.append("upload", data.file);
	            var details = JSON.stringify({"subCategory":data.category, "fileName": data.filename2,"userId": data.userId});
                fd.append("advanceDetails", details);
	            $http.post('/ui/uploadResearchDocuments', fd, {
	            	transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined, 'token': data.token }
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
	            return defered.promise;
            },
            following: function(obj,moduleName) {
                var defered = $q.defer();
                $http.put('/ui/following/', obj, {
                    headers: {token: obj.token}
                }).then(function(response) {
                    defered.resolve(response.data);
                }, function(error) {
                    defered.reject(response.data);
                });
                return defered.promise;
            }, 
            setUserId: function(id){
                userId = id;
            },
            getUserId: function(){
                return userId;
            },
            setPath: function(x){
                path = x;
            },
            getPath: function(){
                return path;
            }
        }
    }]);