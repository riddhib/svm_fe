angular.module('SVM')
    .directive('userFollow', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/userProfile/userFollow/userFollow.html',
            controller: 'UserFollowController'
        }
    })
    .controller('UserFollowController', ['$scope','MainService','LoginService', 
        'ChallengeService', 'ProjectService', 'SolutionService', 'ResearchService', 'DiscussionService', 'CollaborateService', 'UserService', 
        function($scope,MainService,LoginService, ChallengeService, ProjectService, SolutionService, ResearchService, DiscussionService, CollaborateService, UserService) {
        $scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
        $scope.token = localStorage.getItem("token");
        $scope.challengeFollow = [];
        $scope.solutionFollow = [];
        $scope.projectFollow = [];
        $scope.researchFollow = [];
        $scope.collaborateFollow = [];
        $scope.discussionsFollow = [];
        $scope.followDetails = {};
        var discussionData = {};
        $scope.number = 5;
        $scope.changeLoaderStatus = MainService.setLoaderStatus;
        $scope.changeLoaderStatus(true);
       // Get Follow Data
       function init(){
        UserService.getFollowing({'userId': $scope.userDetails.userId, 'token': $scope.token}).then(function(response) {
            if (response.status == 'Success') {
                $scope.userFollowing = response.result;
                $scope.challengeFollow = $scope.userFollowing.challenges;
                $scope.solutionFollow = $scope.userFollowing.solutions;
                $scope.projectFollow = $scope.userFollowing.projects;
                $scope.researchFollow = $scope.userFollowing.researchs;
                $scope.collaborateFollow = $scope.userFollowing.collaborates;
                $scope.discussionsFollow = $scope.userFollowing.discussions;
                $scope.changeLoaderStatus(false);
            }
        }, function(error) {

        });
       }
       init();
        
        $scope.getNumber = function(num) {
            return new Array(num);   
        }
        $scope.unFollow = function(id,moduleName,index){
            if(moduleName == 'challenge') {
                $scope.challengeFollow.splice(index,1);
            }else if(moduleName == 'project' ) {
                $scope.projectFollow.splice(index,1);
            }else if(moduleName == 'research' ) {
                $scope.researchFollow.splice(index,1);
            }else if(moduleName == 'collaborate' ) {
                $scope.collaborateFollow.splice(index,1);
            }else if(moduleName == 'solution' ) {
                $scope.solutionFollow.splice(index,1);
            }
            else if(moduleName == 'discussionss' ) {
                $scope.discussionsFollow.splice(index,1);
            }
            $scope.followDetails.collectionName = moduleName;
            $scope.followDetails.collectionId = id;
            $scope.followDetails.userId =  $scope.userDetails.userId;
            $scope.followDetails.userName =  $scope.userDetails.userName ; 
            $scope.followDetails.follow = false;
            $scope.followDetails.token = $scope.token;
            UserService.following($scope.followDetails).then(function(response) {  
                if(response.status == 'Success') {

                } else {

                }
            }, function(error) {

            });
        }

        $scope.viewChallenge = function (id) {
            MainService.setActiveView('editChallengeView');
            $scope.id = id;
            ChallengeService.setChallengeId($scope.id);
        }

        $scope.viewCollaborate = function(id){
            CollaborateService.setCollaborateId(id);
            MainService.setActiveView('editCollaborateView');
        }

        $scope.viewProject = function(id){
            ProjectService.setProjectId(id);
            ProjectService.setProjectFolderName('Following');
            MainService.setActiveView('projectDataView');
        }

        $scope.viewSolution =function(id){
          SolutionService.setSolutionId(id);
          SolutionService.setPath('Following');
          MainService.setActiveView('editSolutionView')
        }
        $scope.discussion = function (x) {
            discussionData.discussionName = x.discussionName;
            discussionData.discussionId  = x.discussionId;
            MainService.setDiscussionThread(discussionData);
            MainService.setActiveView('discussionsView');
        }
        
    }])
