angular.module('SVM', ['720kb.datepicker'])
    .directive('mainPage', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: 'directives/mainPage/mainPage.html',
            controller: 'mainPageController'
        }
    }) 
    .controller('mainPageController', ['$scope','MainService', function($scope,MainService) {
        if (JSON.parse(localStorage.getItem("userDetails")) && JSON.parse(localStorage.getItem("currentView"))) {
            MainService.setActiveView(JSON.parse(localStorage.getItem("currentView")));
        } else if (JSON.parse(localStorage.getItem("userDetails"))) {
            MainService.setActiveView('mainBodyView');
        } else {
            
        }
        $scope.view = MainService.getActiveView;
        $scope.changePassword = false;
        $scope.activateUser = false;
        $scope.errorPage = false;
        $scope.loader = MainService.getLoaderStatus;
        var reg = new RegExp('/changepassword',"g");
        var reg1 = new RegExp('/activateduser');
        var reg2 = new RegExp('/errorpage');
        var reg3 = new RegExp('/linkexpired');
        var currentUrl = window.location.href.toLowerCase();   
        if(currentUrl.match(reg)) {
            $scope.changePassword = true;
            MainService.setActiveView("resetPasswordView");
        } else if(currentUrl.match(reg1)) {      
            $scope.activateUser = true;
            MainService.setActiveView("activateUserView");
        } else if(currentUrl.match(reg2)) {  
            MainService.setErrorPageValue('error');     
            $scope.errorPage = true;
            MainService.setActiveView("errorPageView");
        }else if(currentUrl.match(reg3)) {  
            MainService.setErrorPageValue('expiredlink'); 
            $scope.errorPage = true;
            MainService.setActiveView("errorPageView");
        }else {
            $scope.activateUser = false;
            $scope.changePassword = false;  
            $scope.errorPage = false;            
        }
    }])
