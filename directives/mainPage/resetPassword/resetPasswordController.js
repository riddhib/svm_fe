angular.module('SVM')
  .directive('resetPassword', function () {
    return {
      restrict: 'E',
      scope: false,
      templateUrl: './directives/mainPage/resetPassword/resetPassword.html',
      controller: 'ResetPasswordController'
    }
  })
  .controller('ResetPasswordController', ['$scope', 'MainService','LoginService','$location','$timeout', function ($scope, MainService, LoginService, $location, $timeout ) {
    //$scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
    var url = $location.absUrl();
    var id = url.split("::")[1];
    $scope.resetPassword = {};
    $scope.resetData = {};
    $scope.resetData.userId=id;
    $scope.savePassword = function(){
      $scope.resetData.password = $scope.resetPassword.newPassword;
      $scope.resetData.confirmPassword = $scope.resetPassword.confirmPassword;
      LoginService.reset($scope.resetData).then(function(response) {
        if(!response.error){
          $scope.successMessage='Password changed Successfully';
          $timeout(function() { 
            $scope.successMessage='';
            $scope.errorMessage='';
            MainService.setActiveView('signUpView');
          }, 2000);
        } else {
          $scope.errorMessage=response.error;
        }
      }, function (validate) {
        $scope.validate = validate;
      });
    }
        

  }])
