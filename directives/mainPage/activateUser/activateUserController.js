angular.module('SVM')
  .directive('activateUser', function () {
    return {
      restrict: 'E',
      scope: false,
      templateUrl: './directives/mainPage/activateUser/activateUser.html',
      controller: 'activateUserController'
    }
  })
  .controller('activateUserController', ['$scope', 'MainService','LoginService','$location','$timeout', function ($scope, MainService, LoginService, $location, $timeout ) {
      
    $timeout(function() { 
        $scope.successMessage='';
        $scope.errorMessage='';
        window.location.href = '/';
        MainService.setActiveView('signUpView');
    }, 4000);
    

  }])