angular.module('SVM')
    .factory('LandingPageService', ['$http', '$q', function($http, $q) { 
        var view = {
            landingPageHomeView:true,
            trackerView:false,
            challengeOpenView :false,
        };
        var userFields = {};
        var trackerValue ='';
        return {
            setUserData: function(userData){
                userFields = userData || {};
            },
            getUserData: function(){
                return userFields;
            }, 
            getActiveView: function() {
                return view;
            },
            setActiveView: function(viewName) {
                 view = {
                    landingPageHomeView:false,
                    trackerView:false,
                    challengeOpenView :false,
                };
                view[viewName] = true;
            },
            setValue: function(val) {
                valueName = val || {};
            },
            getValue: function(){
                return valueName;
            },
            setTrakerSignUpValue: function(value) {
                trackerValue = value;
            },
            getTrakerSignUpValue : function() {
                return trackerValue;
            }
        }
    }]);