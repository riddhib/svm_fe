angular.module('SVM')
    .directive('tracker', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/landingPage/tracker/tracker.html',
            controller: 'TrackerController'
        }
    })
    .controller('TrackerController', ['$scope','LandingPageService','MainService', function($scope, LandingPageService,MainService) {
        $scope.userData = {};
        $scope.sector = [];
        $scope.currentState = 'whoAreYou';
        $scope.firstStage = function(value) {
            $scope.firstValue =value;
            $scope.currentState = 'chooseProblem';

        }
        $scope.landingViews = function(value){
            LandingPageService.setActiveView('challengeOpenView'); 
            LandingPageService.setValue(value);
        }
        $scope.home = function() {
            LandingPageService.setActiveView('landingPageHomeView');
        }
        // $scope.secondStage = function(value) {
        //     $scope.secondValue =value;
        //     $scope.currentState = 'results';

        //     $scope.userData.type =  $scope.firstValue;
        //     $scope.userData.areaOfInterest =  $scope.secondValue;
        //     LandingPageService.setUserData($scope.userData);
        // }
        $scope.secondStage = function(value){
            if($scope.sector.includes(value)){
                $scope.sector = $scope.sector.filter(x=> x!= value);
            } else {
                $scope.sector.push(value);
            }
            $scope.secondValue = $scope.sector;
            $scope.userData.type =  $scope.firstValue;
            $scope.userData.areaOfInterest = $scope.secondValue;
            LandingPageService.setUserData($scope.userData);

        }
        $scope.next = function(){
            $scope.sector = [];
            $scope.currentState = 'results';
        }
        $scope.signUp = function() {
            MainService.setActiveView('signUpView');
        } 

        $scope.reverseClick = function(value) {
            if (value == 1){
                $scope.currentState = 'whoAreYou';
                $scope.firstValue = '';
                $scope.secondValue ='';
            }
            else if (value == 2){
                $scope.currentState = 'chooseProblem';
                $scope.secondValue ='';
            }

        }
    }])