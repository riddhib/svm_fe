angular.module('SVM')
    .directive('landingPageHome', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/landingPage/landingPageHome/landingPageHome.html',
            controller: 'LandPageHomeController'
        }
    })
    .controller('LandPageHomeController', ['$scope','MainService', 'LandingPageService', function($scope,MainService, LandingPageService) { 
        $scope.trackerValue = false;
        $scope.trackerValue = LandingPageService.getTrakerSignUpValue();
        if ($scope.trackerValue) {
            LandingPageService.setActiveView('trackerView');
        }
        $scope.signUp = function() {
            MainService.setActiveView('signUpView');
        }   
        $scope.landingViews = function(value){
            LandingPageService.setValue(value);
            LandingPageService.setActiveView('challengeOpenView');  
        }
        $scope.tracker = function (view) {
            LandingPageService.setActiveView(view); 
        }
    }])
   
