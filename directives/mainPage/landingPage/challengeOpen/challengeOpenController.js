angular.module('SVM')
    .directive('challengeOpen', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/landingPage/challengeOpen/challengeOpen.html',
            controller: 'ChallengeOpenController'
        }
    })
    .controller('ChallengeOpenController', ['$scope','MainService', 'LandingPageService', function($scope,MainService,LandingPageService) {
      $scope.openView = LandingPageService.getValue();
        $scope.signUp = function() {
            MainService.setActiveView('signUpView');
        }   
        $scope.landingViews = function(value){
            LandingPageService.setActiveView('challengeOpenView'); 
            LandingPageService.setValue(value);
            $scope.openView = value;
        }
        $scope.home = function() {
            LandingPageService.setActiveView('landingPageHomeView');
        }
        $scope.traker = function(view) {
            LandingPageService.setActiveView(view);
        }
    }])
   