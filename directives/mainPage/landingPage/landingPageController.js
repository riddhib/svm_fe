angular.module('SVM')
    .directive('landingPage', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/landingPage/landingPage.html',
            controller: 'LandPageController'
        }
    })
    .controller('LandPageController', ['$scope','LandingPageService', function($scope, LandingPageService) {
        $scope.view = LandingPageService.getActiveView;
    }])
   
