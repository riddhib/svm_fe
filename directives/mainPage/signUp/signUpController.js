angular.module('SVM')
    .directive('signUp', function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: './directives/mainPage/signUp/signUp.html',
            controller: 'signUpController'
        }
    })
    .controller('signUpController', ['$scope','LoginService','$timeout','MainService','LandingPageService', function($scope, LoginService, $timeout, MainService, LandingPageService) {
      $scope.logInValue=true;
      $scope.signUpValue=false;
      var trakerSignUp = false;
      $scope.logIn = {};
      $scope.signUp ={};
      $scope.userData = {};
      $scope.validate = {};
      $scope.userData = LandingPageService.getUserData();
      $scope.changeLoaderStatus = MainService.setLoaderStatus;
      if ($scope.userData.areaOfInterest && $scope.userData.type ) {
        $scope.logInValue=false;
        $scope.signUpValue=true;
      }
      $scope.cardView = function(value) {
        if (value =='signUp') {
          if( !$scope.userData.areaOfInterest && !$scope.userData.type ) {
            trakerSignUp = true;
            LandingPageService.setTrakerSignUpValue(trakerSignUp)
            MainService.setActiveView('landingView');
          } else {
            $scope.logInValue=false;
            $scope.signUpValue=true;
            $scope.forgotPasswordValue=false;
            $scope.logIn = {};
            $scope.validate = {};
            $scope.successMessage='';
            $scope.errorMessage='';
          }
         
        } else if(value =='logIn'){
          $scope.logInValue=true;
          $scope.signUpValue=false;
          $scope.forgotPasswordValue=false;
          $scope.signUp ={};
          $scope.validate = {};
          $scope.successMessage='';
          $scope.errorMessage='';
        } else if(value =='forgotPassword'){
          $scope.logInValue=false;
          $scope.signUpValue=false;
          $scope.forgotPasswordValue=true;
          $scope.forgotPassword ={};
          $scope.successMessage='';
          $scope.errorMessage='';
        }
        else {
          console.log('Login error, Please try after Sometime');
        }    
      }

      //user login 
      $scope.userLogin = function() {
        $scope.changeLoaderStatus(true);
        $scope.successMessage='';
        $scope.errorMessage='';
        $scope.logIn.type = $scope.userData.type;
        $scope.logIn.areaOfInterest = $scope.userData.areaOfInterest;
        LoginService.login($scope.logIn)
          .then(function(response) {
            if (response.status == 'Success'){
              localStorage.setItem('token', response.result.token);
              localStorage.setItem('userDetails', JSON.stringify(response.result.user));
              MainService.setActiveView('mainBodyView');
              $scope.logIn = {};
              $scope.changeLoaderStatus(false);
            } else if(response.error){
              $scope.errorMessage = response.error;
              $scope.changeLoaderStatus(false);
            }
          }, function(validate) {
            $scope.validate = validate;
            $scope.changeLoaderStatus(false);
          });
      }

      // user signUp
      $scope.userSignUp = function() {
        $scope.successMessage='';
        $scope.errorMessage='';
        $scope.signUp.type = $scope.userData.type;
        $scope.signUp.areaOfInterest = $scope.userData.areaOfInterest;
        LoginService.userSignUp($scope.signUp)
          .then(function(response) {  
            if(response.status == 'Success') {
              $scope.successMessage = 'Link has been sent to your registered email. Please activate your account.'; 
              $timeout(function() {
                $scope.cardView('logIn');
                $scope.successMessage='';
                $scope.errorMessage='';
              }, 3500);
              $scope.signUp ={};  
              LandingPageService.setUserData({});
            } else if(response.error) {
              if(response.error) {
                $scope.errorMessage = response.error;
                $scope.eid= true;
              }       
            }
          }, function(validate) {
            $scope.validate = validate;
          });
      }

      $scope.userForgotPassword = function() {
        $scope.successMessage='';
        $scope.errorMessage='';
        LoginService.userForgotPassword($scope.forgotPassword)
          .then(function(response) { 
            if(response.status == 'Success') {
              $scope.successMessage = 'Password reset link has been sent to your registered email.'; 
              $timeout(function() {
                $scope.cardView('logIn');
                $scope.successMessage='';
                $scope.errorMessage='';
              }, 3500);
              $scope.forgotPassword ={};  
            }  else if(response.error) {
              if(response.error) {
                  $scope.errorMessage = response.error;
                  $scope.eid= true;
              }       
            }
          }, function(validate) {
            $scope.validate = validate;
          });
      }
      $scope.home = function() {
        trakerSignUp = false;
        LandingPageService.setTrakerSignUpValue(false);
        MainService.setActiveView('landingView');
      }
      $scope.googleAuth = function(){
        window.location.href="/auth/google"
      }
    }])
   
