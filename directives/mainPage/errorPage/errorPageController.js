angular.module('SVM')
  .directive('errorPage', function () {
    return {
      restrict: 'E',
      scope: false,
      templateUrl: './directives/mainPage/errorPage/errorpage.html',
      controller: 'ErrorPageController'
    }
  })
  .controller('ErrorPageController', ['$scope', 'MainService','LoginService','$location','$timeout', function ($scope, MainService, LoginService, $location, $timeout ) {
    $scope.value = MainService.getErrorPageValue();
    var url = $location.absUrl();
    $scope.home = function() {
        window.location.href="/"
        MainService.setActiveView('landingView');
    }
    $scope.login = function() {
      window.location.href="/"
      MainService.setActiveView('signUpView');
    }

  }])