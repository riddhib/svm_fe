angular.module('SVM').
    factory('LoginService', ['$http', '$q', 'ValidateService', function ($http, $q, ValidateService) {
        return {
            login: function (obj) {
                var defered = $q.defer();
                ValidateService.validateSignIn(obj)
                    .then(function () {
                        $http.post('ui/auth', obj).then(function (response) {
                            defered.resolve(response.data);
                        },function (error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function (validate) {
                        defered.reject(validate);
                    });
                return defered.promise;
            },
            userSignUp: function(obj) {
                var defered = $q.defer();
                ValidateService.validateSignUp(obj)
                    .then(function () {
                        $http.post('/ui/user', obj).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function (validate) {
                        defered.reject(validate);
                    });
                return defered.promise;
            },
            userForgotPassword: function(obj) {
                var defered = $q.defer();
                ValidateService.validateUserForgotPassword(obj)
                    .then(function () {
                        $http.post('/ui/user/forgotPassword', obj).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function (validate) {
                        defered.reject(validate);
                    });
                return defered.promise;
            },
            userResetPassword: function(obj) {
                var defered = $q.defer();
                ValidateService.validateUserResetPassword(obj)
                    .then(function () {
                        $http.post('/ui/user/resetPassword', obj).then(function(response) {
                            defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(response.data);
                        });
                        return defered.promise;
                    }, function (validate) {
                        defered.reject(validate);
                    });
                return defered.promise;
            },
            reset: function(obj) {
                var defered = $q.defer();
                ValidateService.validateUserResetPassword(obj)
                    .then(function () {
                        $http.post('/ui/resetPassword',obj).then(function(response){
                         defered.resolve(response.data);
                        }, function(error) {
                            defered.reject(error);
                        });
                         return defered.promise;
                    }, function (validate) {
                        defered.reject(validate);
                    });
                return defered.promise;
            }
        }
    }]);