angular.module('SVM')
  .directive('topNav', function () {
      return {
          restrict: 'E',
          scope: false,
          templateUrl: './directives/mainPage/topNav/topnav.html',
          controller: 'topNavController'
      }
  })
  .controller('topNavController', ['$scope', 'MainService','$timeout','UserService', function ($scope, MainService, $timeout, UserService) {
      var discussionData = {};
      $scope.userDetails = JSON.parse(localStorage.getItem("userDetails"));
      $scope.token = localStorage.getItem("token");
      $scope.userDetailsLogo = JSON.parse(localStorage.getItem("userDetailsLogo")) || $scope.userDetails.logo;
      if(JSON.parse(localStorage.getItem("currentView"))){
        $scope.activeTab = JSON.parse(localStorage.getItem("currentView"));
      } else {
        $scope.activeTab = 'challengeView';
      }

      $scope.changeView = function (view, user = {}) {
          localStorage.setItem("currentView", JSON.stringify(view));
          if (view == 'discussionsView') {
              MainService.setDiscussionThread(discussionData);
          }
          if (view == 'userProfileView') {
              UserService.setUserId($scope.userDetails.userId);
          }
          MainService.setActiveView(view);
          $scope.activeTab = view;
          $scope.activeUserMsg = user || {};
      }

      $scope.activeInternalView = function (view) {
          MainService.setActiveView(view);
      }
      
      $scope.signOut = function () {
        UserService.logoutUser($scope.token).then(function(response){
          if (!response.error) {
            MainService.setActiveView('landingView');
            localStorage.clear();
          }
        });
      }
  }])